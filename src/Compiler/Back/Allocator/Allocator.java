package Compiler.Back.Allocator;

import Compiler.Back.MidCode.Intruction.Operand.VirtualRegister.VirtualRegister;
import Compiler.Front.Ast.Function;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by crystal on 5/31/17.
 */
public abstract class Allocator {
    public Function function;
    public Map<VirtualRegister, PhysicalRegister> registerMapping;

    public Allocator(Function function) {
        this.function = function;
        this.registerMapping = new HashMap<>();
    }

    public Set<PhysicalRegister> getUsedPhysicalRegisters() {
        return new HashSet<PhysicalRegister>() {{
            for (VirtualRegister virtualRegister : registerMapping.keySet()) {
                PhysicalRegister physicalRegister = registerMapping.get(virtualRegister);
                if (physicalRegister != null) {
                    add(physicalRegister);
                }
            }
        }};
    }
}
