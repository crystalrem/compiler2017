package Compiler.Back.Allocator.GlobalRegisterAllocator;

import Compiler.Back.Allocator.PhysicalRegister;
import Compiler.Back.MidCode.Intruction.Operand.VirtualRegister.VariableRegister.TemporaryRegister;
import Compiler.Back.MidCode.Intruction.Operand.VirtualRegister.VirtualRegister;
import Compiler.Back.Translator.Register.Register;

import java.util.*;

/**
 * Created by crystal on 5/31/17.
 */
public class InterferenceGraph {
    public static List<PhysicalRegister> color = new ArrayList<PhysicalRegister>() {{
        add(Register.r12);
        add(Register.r13);
        add(Register.r14);
        add(Register.r15);
        add(Register.rbx);
        add(Register.rsi);
        add(Register.rdi);
        add(Register.r8);
        add(Register.r9);
    }};

    public Set<VirtualRegister> vertices;
    public Map<VirtualRegister, Set<VirtualRegister>> forbids;
    public Map<VirtualRegister, Set<VirtualRegister>> recommends;

    InterferenceGraph() {
        vertices = new HashSet<>();
        forbids = new HashMap<>();
        recommends = new HashMap<>();
    }

    void add(VirtualRegister x) {
        vertices.add(x);
        Set<VirtualRegister>forbidsSet = new HashSet<>();
        Set<VirtualRegister>recommendsSet = new HashSet<>();
        forbids.put(x, forbidsSet);
        recommends.put(x, recommendsSet);
    }

    void forbid(VirtualRegister x, VirtualRegister y) {
        if (x == y) {
            return;
        }
        if (x instanceof TemporaryRegister && y instanceof TemporaryRegister) {
            forbids.get(x).add(y);
            forbids.get(y).add(x);
        }
    }

    void recommend(VirtualRegister x, VirtualRegister y) {
        if (x == y) {
            return;
        }
        if (x instanceof TemporaryRegister && y instanceof TemporaryRegister) {
            recommends.get(x).add(y);
            recommends.get(y).add(x);
        }
    }
}
