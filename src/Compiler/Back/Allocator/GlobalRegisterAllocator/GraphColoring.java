package Compiler.Back.Allocator.GlobalRegisterAllocator;

import Compiler.Back.Allocator.PhysicalRegister;
import Compiler.Back.MidCode.Intruction.Operand.VirtualRegister.VirtualRegister;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by crystal on 5/31/17.
 */
public class GraphColoring {
    public InterferenceGraph graph;
    Map<VirtualRegister, PhysicalRegister> mapping;

    GraphColoring(InterferenceGraph graph) {
        this.graph = graph;
        this.mapping = new HashMap<>();
    }

    void color(final VirtualRegister vertex) {
        Set<PhysicalRegister> used = new HashSet<PhysicalRegister>() {{
            for (VirtualRegister neighbor : graph.forbids.get(vertex)) {
                if (mapping.containsKey(neighbor)) {
                    add(mapping.get(neighbor));
                }
            }
            add(null);
        }};
        for (VirtualRegister neighbor : graph.recommends.get(vertex)) {
            if (!mapping.containsKey(neighbor)) {
                continue;
            }
            PhysicalRegister color = mapping.get(neighbor);
            if (!mapping.containsKey(vertex) && !used.contains(color)) {
                mapping.put(vertex, color);
                break;
            }
        }
        for (PhysicalRegister color : InterferenceGraph.color) {
            if (!mapping.containsKey(vertex) && !used.contains(color)) {
                mapping.put(vertex, color);
                break;
            }
        }
        if (!mapping.containsKey(vertex)) {
            mapping.put(vertex, null);
        }
    }
}
