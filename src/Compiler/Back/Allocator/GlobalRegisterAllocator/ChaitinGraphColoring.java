package Compiler.Back.Allocator.GlobalRegisterAllocator;

import Compiler.Back.Allocator.Allocator;
import Compiler.Back.Allocator.PhysicalRegister;
import Compiler.Back.MidCode.Intruction.Operand.VirtualRegister.VariableRegister.TemporaryRegister;
import Compiler.Back.MidCode.Intruction.Operand.VirtualRegister.VirtualRegister;

import java.util.*;

/**
 * Created by crystal on 5/31/17.
 */
public class ChaitinGraphColoring{
    public InterferenceGraph interferenceGraph;
    public Map<VirtualRegister, PhysicalRegister> mapping;
    public Map<VirtualRegister, Integer> degree;
    public Set<VirtualRegister> leftVertices;

    public ChaitinGraphColoring(InterferenceGraph interferenceGraph) {
        this.interferenceGraph = interferenceGraph;
        mapping = new HashMap<>();
        degree = new HashMap<>();
        leftVertices = new HashSet<>();
    }

    public Map<VirtualRegister, PhysicalRegister> analysis() throws Exception {
        for (VirtualRegister register : interferenceGraph.vertices) {
            leftVertices.add(register);
            degree.put(register, interferenceGraph.forbids.get(register).size());
        }
        Stack<VirtualRegister> stack = new Stack<>();
        while (stack.size() < interferenceGraph.vertices.size()) {
            boolean modified = false;
            for (VirtualRegister vertice : leftVertices) {
                if (degree.get(vertice) < InterferenceGraph.color.size()) {
                    stack.add(vertice);
                    remove(vertice);
                    modified = true;
                    break;
                }
            }
            if (!modified) {
                int maxDegree = -1;
                VirtualRegister uu = null;
                for (VirtualRegister vertice : leftVertices) {
                    if (degree.get(vertice) > maxDegree) {
                        maxDegree = degree.get(vertice);
                        uu = vertice;
                    }
                }
                if (uu != null) {
                    stack.add(uu);
                    remove(uu);
                } else {
                    throw new InternalError("Internal Error!!");
                }
            }
        }
        while (!stack.empty()) {
            putColor(stack.pop());
        }

        Map<VirtualRegister, PhysicalRegister> old = mapping;
        mapping = new HashMap<>();
        for (VirtualRegister register : old.keySet()) {
            if (register instanceof TemporaryRegister) {
                mapping.put(register, old.get(register));
            }
        }
        return mapping;
    }

    public void remove(VirtualRegister vertice) {
        leftVertices.remove(vertice);
        for (VirtualRegister v : interferenceGraph.forbids.get(vertice)) {
            degree.put(v, degree.get(v) - 1);
        }
    }

    public void putColor(VirtualRegister vertice) {
        Set<PhysicalRegister> used = new HashSet<>();
        for (VirtualRegister v : interferenceGraph.forbids.get(vertice)) {
            if (mapping.containsKey(v)) {
                used.add(mapping.get(v));
            }
        }
        for (VirtualRegister v : interferenceGraph.recommends.get(vertice)) {
            if (mapping.containsKey(v)) {
                PhysicalRegister color = mapping.get(v);
                if (!mapping.containsKey(vertice) && !used.contains(color)) {
                    mapping.put(vertice, color);
                    break;
                }
            }
        }
        for (PhysicalRegister color : InterferenceGraph.color) {
            if (!mapping.containsKey(vertice) && !used.contains(color)) {
                mapping.put(vertice, color);
                break;
            }
        }
    }
}
