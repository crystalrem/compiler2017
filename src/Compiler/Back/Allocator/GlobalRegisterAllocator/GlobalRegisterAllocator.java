package Compiler.Back.Allocator.GlobalRegisterAllocator;

import Compiler.Back.Allocator.Allocator;
import Compiler.Back.MidCode.Block;
import Compiler.Back.MidCode.Intruction.ArithmeticInstruction.DualInstruction.DualInstruction;
import Compiler.Back.MidCode.Intruction.ArithmeticInstruction.MemoryInstruction.MoveInstruction;
import Compiler.Back.MidCode.Intruction.Instruction;
import Compiler.Back.MidCode.Intruction.Operand.VirtualRegister.VirtualRegister;
import Compiler.Front.Ast.Function;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by crystal on 5/31/17.
 */
public class GlobalRegisterAllocator extends Allocator{

    public GlobalRegisterAllocator(Function function) throws Exception {
        super(function);
        //function.midCode.refresh();
        InterferenceGraph graph = new InterferenceGraph();
        for (Block block : function.midCode.blocks) {
            for (Instruction instruction : block.instructions) {
                for (VirtualRegister register : instruction.getUsedRegisters()) {
                    graph.add(register);
                }
                for (VirtualRegister register : instruction.getDefinedRegisters()) {
                    graph.add(register);
                }
            }
        }
        for (Block block : function.midCode.blocks) {

            Set<VirtualRegister> live = new HashSet<>();
            for (VirtualRegister virtualRegister : block.liveliness.liveOut) {
                live.add(virtualRegister);
            }
            for (int i = block.instructions.size() - 1; i >= 0; --i) {
                Instruction instruction = block.instructions.get(i);
                if (instruction instanceof DualInstruction) {
                    for (VirtualRegister livingRegister : live) {
                        graph.forbid(((DualInstruction) instruction).aim, livingRegister);
                    }
                    live.remove(((DualInstruction) instruction).aim);
                    if (((DualInstruction) instruction).source2 instanceof VirtualRegister) {
                        live.add((VirtualRegister) ((DualInstruction) instruction).source2);
                    }

                    for (VirtualRegister livingRegister : live) {
                        graph.forbid(((DualInstruction) instruction).aim, livingRegister);
                    }
                    live.remove(((DualInstruction) instruction).aim);
                    if (((DualInstruction) instruction).source instanceof VirtualRegister) {
                        live.add((VirtualRegister) ((DualInstruction) instruction).source);
                    }
                } else {
                    for (VirtualRegister register : instruction.getDefinedRegisters()) {
                        for (VirtualRegister livingRegister : live) {
                            graph.forbid(register, livingRegister);
                        }
                    }
                    for (VirtualRegister register : instruction.getDefinedRegisters()) {
                        live.remove(register);
                    }
                    for (VirtualRegister register : instruction.getUsedRegisters()) {
                        live.add(register);
                    }
                }

            }
        }
        for (Block block : function.midCode.blocks) {
            for (Instruction instruction : block.instructions) {
                if (instruction instanceof MoveInstruction) {
                    MoveInstruction i = (MoveInstruction)instruction;
                    if (i.source instanceof VirtualRegister) {
                        graph.recommend(i.aim, (VirtualRegister)i.source);
                    }
                }
            }
        }
        registerMapping = new ChaitinGraphColoring(graph).analysis();
    }
}
