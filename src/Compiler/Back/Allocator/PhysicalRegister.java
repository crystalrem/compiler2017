package Compiler.Back.Allocator;

/**
 * Created by crystal on 5/27/17.
 */
public abstract class PhysicalRegister {
    public int identity;
    public String name;
    public boolean calleeSave;

    public PhysicalRegister(int identity, String name, boolean calleeSave) {
        this.identity = identity;
        this.name = name;
        this.calleeSave = calleeSave;
    }

    @Override
    public String toString() {
            return name;
        }

}
