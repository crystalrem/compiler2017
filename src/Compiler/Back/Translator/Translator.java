package Compiler.Back.Translator;

import Compiler.Back.MidCode.MidCode;

import java.io.IOException;
import java.io.PrintStream;

/**
 * Created by crystal on 5/27/17.
 */
public abstract class Translator {
    public PrintStream output;

    public Translator(PrintStream output) {
        this.output = output;
    }

    public abstract void translate(MidCode midCode);
    public abstract void translate() throws Exception;
}
