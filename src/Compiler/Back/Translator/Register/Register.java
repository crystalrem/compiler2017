package Compiler.Back.Translator.Register;

import Compiler.Back.Allocator.PhysicalRegister;

/**
 * Created by crystal on 5/25/17.
 */
public class Register extends PhysicalRegister{

    //public abstract String toString();
    //forNaive
    /*
    public static PhysicalRegister rax = new Register(6, "rax");
    public static PhysicalRegister rbx = new Register(2, "rbx");
    public static PhysicalRegister rcx = new Register(3, "rcx");
    public static PhysicalRegister rdx = new Register(4, "rdx");
    public static PhysicalRegister rsp = new Register(5, "rsp");
    public static PhysicalRegister rbp = new Register(1, "rbp");
    public static PhysicalRegister rsi = new Register(7, "rsi");
    public static PhysicalRegister rdi = new Register(8, "rdi");
    public static PhysicalRegister r8 = new Register(9, "r8");
    public static PhysicalRegister r9 = new Register(10, "r9");
    public static PhysicalRegister r10 = new Register(11, "r10");
    public static PhysicalRegister r11 = new Register(12, "r11");
    public static PhysicalRegister r12 = new Register(13, "r12");
    public static PhysicalRegister r13 = new Register(14, "r13");
    public static PhysicalRegister r14 = new Register(15, "r14");
    public static PhysicalRegister r15 = new Register(16, "r15");
    **/
    //forUpdate
    public static PhysicalRegister rax = new Register(1, "rax", false);
    public static PhysicalRegister rcx = new Register(2, "rcx", false);
    public static PhysicalRegister rdx = new Register(3, "rdx", false);
    public static PhysicalRegister rbx = new Register(4, "rbx", true);
    public static PhysicalRegister rsp = new Register(5, "rsp", true);
    public static PhysicalRegister rbp = new Register(6, "rbp", true);
    public static PhysicalRegister rsi = new Register(7, "rsi", false);
    public static PhysicalRegister rdi = new Register(8, "rdi", false);
    public static PhysicalRegister r8 = new Register(9, "r8", false);
    public static PhysicalRegister r9 = new Register(10, "r9", false);
    public static PhysicalRegister r10 = new Register(11, "r10", false);
    public static PhysicalRegister r11 = new Register(12, "r11", false);
    public static PhysicalRegister r12 = new Register(13, "r12", true);
    public static PhysicalRegister r13 = new Register(14, "r13", true);
    public static PhysicalRegister r14 = new Register(15, "r14", true);
    public static PhysicalRegister r15 = new Register(16, "r15", true);


    private Register(int identity, String name, boolean calleeSave) {
        super(identity, name, calleeSave);
    }

    public static int size() {
        return 8;
    }
    public static int count() {
        return 16;
    }
}
