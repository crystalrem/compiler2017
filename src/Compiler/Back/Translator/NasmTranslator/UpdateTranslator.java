package Compiler.Back.Translator.NasmTranslator;

import Compiler.Back.Allocator.Allocator;
import Compiler.Back.Allocator.PhysicalRegister;
import Compiler.Back.MidCode.Block;
import Compiler.Back.MidCode.Intruction.ArithmeticInstruction.ArithmeticInstruction;
import Compiler.Back.MidCode.Intruction.ArithmeticInstruction.DualInstruction.*;
import Compiler.Back.MidCode.Intruction.ArithmeticInstruction.MemoryInstruction.*;
import Compiler.Back.MidCode.Intruction.ArithmeticInstruction.UnitaryInsurction.MinusInstruction;
import Compiler.Back.MidCode.Intruction.ArithmeticInstruction.UnitaryInsurction.NotInstruction;
import Compiler.Back.MidCode.Intruction.ArithmeticInstruction.UnitaryInsurction.UnitaryInstruction;
import Compiler.Back.MidCode.Intruction.ControlFlowInstruction.BranchInstruction;
import Compiler.Back.MidCode.Intruction.ControlFlowInstruction.ControlFlowInstruction;
import Compiler.Back.MidCode.Intruction.ControlFlowInstruction.JumpInstruction;
import Compiler.Back.MidCode.Intruction.FunctionInstruction.CallInstruction;
import Compiler.Back.MidCode.Intruction.FunctionInstruction.FunctionInstruction;
import Compiler.Back.MidCode.Intruction.FunctionInstruction.ReturnInstruction;
import Compiler.Back.MidCode.Intruction.Instruction;
import Compiler.Back.MidCode.Intruction.Operand.ImmediateValue;
import Compiler.Back.MidCode.Intruction.Operand.Operand;
import Compiler.Back.MidCode.Intruction.Operand.VirtualRegister.StringRegister;
import Compiler.Back.MidCode.Intruction.Operand.VirtualRegister.VariableRegister.GlobalRegister;
import Compiler.Back.MidCode.Intruction.Operand.VirtualRegister.VariableRegister.ParameterRegister;
import Compiler.Back.MidCode.Intruction.Operand.VirtualRegister.VariableRegister.TemporaryRegister;
import Compiler.Back.MidCode.Intruction.Operand.VirtualRegister.VirtualRegister;
import Compiler.Back.MidCode.MidCode;
import Compiler.Back.Translator.Register.Register;
import Compiler.Front.Ast.Function;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by crystal on 5/31/17.
 */
public class UpdateTranslator extends NasmTranslator{

    public Allocator allocator;
    public MidCode midCode;
    public UpdateTranslator(PrintStream output) {
        super(output);
    }

    public String getMemoryPlace(Operand register) {
        if (register instanceof GlobalRegister) {
            return "qword [rel " + getGlobalVariableName(register) + "]";
        } else if (register instanceof TemporaryRegister) {
            int offset = midCode.frame.temporary.get(register);
            return String.format("qword [rbp + (%d)]", offset);
        } else if (register instanceof ParameterRegister) {
            int offset = midCode.frame.parameter.get(register);
            return String.format("qword [rbp + (%d)]", offset);
        } else if (register instanceof ImmediateValue) {
            return String.valueOf(((ImmediateValue) register).value);
        } else if (register instanceof StringRegister) {
            return getStringConstantName(register);
        }
        return "FUCK";
    }

    public void protectRegisters() {
        for (PhysicalRegister physicalRegister : allocator.getUsedPhysicalRegisters()) {
            if(!physicalRegister.calleeSave) {
                output.printf("\tmov qword[rbp + (%d)], %s\n",
                        -midCode.frame.size - Register.size() * (physicalRegister.identity + 1),
                        physicalRegister.name);
            }
        }
    }

    public void restoreRegisters() {
        for (PhysicalRegister physicalRegister : allocator.getUsedPhysicalRegisters()) {
            if(!physicalRegister.calleeSave) {
                output.printf("\tmov %s, qword[rbp + (%d)]\n",
                        physicalRegister.name,
                        -midCode.frame.size - Register.size() * (physicalRegister.identity + 1)
                        );
            }
        }
    }

    public PhysicalRegister loadToRead(Operand source, PhysicalRegister aim) {
        if((source instanceof TemporaryRegister) && (allocator.registerMapping.containsKey(source))) {
            return allocator.registerMapping.get(source);
        }
        else {
            //getPhisicalMemoryName
            output.printf("\tmov %s, %s\n", aim, getMemoryPlace(source));
            return aim;
        }
    }

    public PhysicalRegister loadToWrite(Operand source, PhysicalRegister aim) {
        if((source instanceof TemporaryRegister) && (allocator.registerMapping.containsKey(source))) {
            return allocator.registerMapping.get(source);
        }
        return aim;
    }

    public void store(PhysicalRegister source, VirtualRegister aim) {
        if(allocator.registerMapping.containsKey(aim)) {
            return;
        }
        else {
            output.printf("\tmov %s, %s\n", getMemoryPlace(aim), source);
        }
    }

    public void move(PhysicalRegister source, VirtualRegister aim) {

        if (allocator.registerMapping.containsKey(aim)) {
            output.printf("\tmov %s, %s\n", allocator.registerMapping.get(aim), source);
        } else {
            output.printf("\tmov %s, %s\n", getMemoryPlace(aim), source);
        }
    }

    public void moveFilter(PhysicalRegister aim, PhysicalRegister source) {
        if(source == aim) {
            return;
        }
        else {
            output.printf("\tmov %s, %s\n", aim, source);
        }
    }

    @Override
    public void translate(MidCode midCode) {
        this.midCode = midCode;
        this.allocator = midCode.function.allocator;
        output.printf("%s:\n", getFunctionName(midCode.function));
        output.printf("\tpush rbp\n");
        output.printf("\tmov rbp, rsp\n");
        output.printf("\tsub rsp, %d\n", midCode.frame.size + Register.size() * 20);
        if (midCode.function.parameters.size() >= 1) {
            output.printf("\tmov qword [rbp-8], rdi\n");
        }
        if (midCode.function.parameters.size() >= 2) {
            output.printf("\tmov qword [rbp-16], rsi\n");
        }
        if (midCode.function.parameters.size() >= 3) {
            output.printf("\tmov qword [rbp-24], rdx\n");
        }
        if (midCode.function.parameters.size() >= 4) {
            output.printf("\tmov qword [rbp-32], rcx\n");
        }
        if (midCode.function.parameters.size() >= 5) {
            output.printf("\tmov qword [rbp-40], r8\n");
        }
        if (midCode.function.parameters.size() >= 6) {
            output.printf("\tmov qword [rbp-48], r9\n");
        }
        //protectScene();
        for (PhysicalRegister physicalRegistor : allocator.getUsedPhysicalRegisters()) {
            if (physicalRegistor.calleeSave) {
                output.printf("\tmov    qword [rbp + (%d)], %s\n",
                        -midCode.frame.size - (physicalRegistor.identity + 1) * Register.size(),
                        physicalRegistor);
            }
        }

        for (Block block : midCode.blocks) {
            output.printf("%s:\n", getBlockName(block));
            for (Instruction instruction : block.instructions) {
                output.printf("\n\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t;%s\n", instruction);
                if (instruction instanceof ArithmeticInstruction) {

                    if (instruction instanceof DualInstruction) {
                        //aim = source1 + source2;
                        PhysicalRegister source1 = loadToRead(((DualInstruction) instruction).source, Register.r10);
                        PhysicalRegister source2 = loadToRead(((DualInstruction) instruction).source2, Register.r11);
                        PhysicalRegister aim = loadToWrite(((DualInstruction) instruction).aim, Register.rax);
                        moveFilter(aim, source1);
                        if (instruction instanceof AdditionInstruction) {
                            output.printf("\tadd %s, %s\n", aim, source2);
                        } else if (instruction instanceof AndInstruction) {
                            output.printf("\tand %s, %s\n", aim, source2);
                        } else if (instruction instanceof DivideInstruction) {
                            moveFilter(Register.rax, aim);
                            output.printf("\tcqo\n");
                            output.printf("\tidiv   %s\n", source2);
                            moveFilter(aim, Register.rax);
                        } else if (instruction instanceof EqualInstruction) {
                            output.printf("\tcmp %s, %s\n", aim, source2);
                            output.printf("\tsete   al\n");
                            output.printf("\tmovzx %s, al\n", aim);
                        } else if (instruction instanceof LessInstruction) {
                            output.printf("\tcmp %s, %s\n", aim, source2);
                            output.printf("\tsetl al\n");
                            output.printf("\tmovzx %s, al\n", aim);
                        } else if (instruction instanceof ModuleInstruction) {
                            moveFilter(Register.rax, aim);
                            output.printf("\tcqo\n");
                            output.printf("\tidiv   %s\n", source2);
                            moveFilter(aim, Register.rdx);
                        } else if (instruction instanceof MoreInstruction) {
                            output.printf("\tcmp %s, %s\n", aim, source2);
                            output.printf("\tsetg al\n");
                            output.printf("\tmovzx %s, al\n", aim);
                        } else if (instruction instanceof MoveLeftInstruction) {
                            output.printf("\tmov %s, %s\n", Register.rcx, source2);
                            output.printf("\tsal %s, cl\n", aim);
                        } else if (instruction instanceof MoveRightInstruction) {
                            output.printf("\tmov %s, %s\n", Register.rcx, source2);
                            output.printf("\tsar %s, cl\n", aim);
                        } else if (instruction instanceof MultiplyInstruction) {
                            output.printf("\timul %s, %s\n", aim, source2);
                        } else if (instruction instanceof NoLessInstruction) {
                            output.printf("\tcmp %s, %s\n", aim, source2);
                            output.printf("\tsetge al\n");
                            output.printf("\tmovzx %s, al\n", aim);
                        } else if (instruction instanceof NoMoreInstruction) {
                            output.printf("\tcmp %s, %s\n", aim, source2);
                            output.printf("\tsetle al\n");
                            output.printf("\tmovzx %s, al\n", aim);
                        } else if (instruction instanceof OrInstruction) {
                            output.printf("\tor %s, %s\n", aim, source2);
                        } else if (instruction instanceof SubtractInstruction) {
                            output.printf("\tsub %s, %s\n", aim, source2);
                        } else if (instruction instanceof UnEqualInstruction) {
                            output.printf("\tcmp %s, %s\n", aim, source2);
                            output.printf("\tsetne al\n");
                            output.printf("\tmovzx %s, al\n", aim);
                        } else if (instruction instanceof XorInstruction) {
                            output.printf("\txor %s, %s\n", aim, source2);
                        }
                        store(aim, ((DualInstruction) instruction).aim);
                    } else if (instruction instanceof UnitaryInstruction) {
                        PhysicalRegister source = loadToRead(((UnitaryInstruction) instruction).source, Register.rax);
                        PhysicalRegister aim = loadToWrite(((UnitaryInstruction) instruction).aim, Register.rax);
                        moveFilter(aim, source);
                        if (instruction instanceof NotInstruction) {
                            output.printf("\tnot %s\n", aim);
                        } else if (instruction instanceof MinusInstruction) {
                            output.printf("\tneg %s\n", aim);
                        }
                        store(aim, ((UnitaryInstruction) instruction).aim);
                    }
                }
                else if(instruction instanceof MemoryInstruction) {
                    if(instruction instanceof MoveInstruction) {
                        PhysicalRegister aim = loadToRead(((MoveInstruction) instruction).source, Register.rax);
                        move(aim, ((MoveInstruction) instruction).aim);
                    }
                    else if(instruction instanceof AllocateInstruction) {
                        PhysicalRegister size = loadToRead(((AllocateInstruction) instruction).size, Register.rax);
                        protectRegisters();
                        moveFilter(Register.rdi, size);
                        output.printf("\tcall   malloc\n");
                        restoreRegisters();
                        move(Register.rax, ((AllocateInstruction) instruction).aim);
                    }
                    else if (instruction instanceof LoadInstruction) {
                        PhysicalRegister base = loadToRead(((LoadInstruction) instruction).address.origin, Register.rax);
                        PhysicalRegister destR = loadToWrite(((LoadInstruction) instruction).aim, Register.r10);
                        moveFilter(Register.r11, base);
                        if (((LoadInstruction) instruction).address.offset.value != 0) {
                            output.printf("\tadd %s, %s\n", Register.r11, ((LoadInstruction) instruction).address.offset);
                        }
                        output.printf("\tmov %s, qword [%s]\n", destR, Register.r11);
                        store(destR, ((LoadInstruction) instruction).aim);
                    }
                    else if (instruction instanceof StoreInstruction) {
                        PhysicalRegister base = loadToRead(((StoreInstruction) instruction).address.origin, Register.r10);
                        PhysicalRegister source = loadToRead(((StoreInstruction) instruction).source, Register.rax);
                        moveFilter(Register.r11, base);
                        if (((StoreInstruction) instruction).address.offset.value != 0) {
                            output.printf("\tadd %s, %s\n", Register.r11, ((StoreInstruction) instruction).address.offset);
                        }
                        output.printf("\tmov qword [%s], %s\n", Register.r11, source);
                    }
                }
                else if (instruction instanceof ControlFlowInstruction) {
                    if (instruction instanceof JumpInstruction) {
                        output.printf("\tjmp %s\n", getBlockName(((JumpInstruction) instruction).label.block));
                    }
                    else if (instruction instanceof BranchInstruction) {
                        PhysicalRegister condition = loadToRead(((BranchInstruction) instruction).condition, Register.rax);
                        output.printf("\tcmp %s, 0\n", condition);
                        output.printf("\tjnz %s\n", getBlockName(((BranchInstruction) instruction).trueLabel.block));
                        output.printf("\tjz %s\n", getBlockName(((BranchInstruction) instruction).falseLabel.block));
                    }
                }
                else if (instruction instanceof FunctionInstruction) {
                    if (instruction instanceof ReturnInstruction) {
                        PhysicalRegister source = loadToRead(((ReturnInstruction) instruction).source, Register.rax);
                        moveFilter(Register.rax, source);
                        output.printf("\tjmp %s\n", getBlockName(midCode.exit));
                    }
                    else if (instruction instanceof CallInstruction) {
                        protectRegisters();
                        VirtualRegister aim = ((CallInstruction) instruction).aim;
                        Function function = ((CallInstruction) instruction).function;
                        List<Operand> parameters = ((CallInstruction) instruction).parameters;
                        List<PhysicalRegister> order = new ArrayList<PhysicalRegister>() {{
                            add(Register.rdi);
                            add(Register.rsi);
                            add(Register.rdx);
                            add(Register.rcx);
                            add(Register.r8);
                            add(Register.r9);
                        }};
                        for (int i = 0; i < 6 && i < parameters.size(); i++) {
                            PhysicalRegister cur = loadToRead(parameters.get(i), Register.rax);
                            if (order.contains(cur) && !cur.calleeSave) {
                                output.printf("\tmov %s, qword[rbp + (%d)]\n", order.get(i),
                                        -midCode.frame.size - (cur.identity + 1) * Register.size());
                            }
                            else {
                                output.printf("\tmov %s, %s\n", order.get(i), cur);
                            }
                        }
                        if (parameters.size() > 6) {
                            for (int i = parameters.size() - 1; i >= 6; i--) {
                                PhysicalRegister cur = loadToRead(parameters.get(i), Register.rax);
                                output.printf("\tpush %s\n", cur);
                            }
                        }
                        output.printf("\tcall %s\n", getFunctionName(function));
                        restoreRegisters();
                        if (aim != null) {
                            PhysicalRegister destR = loadToWrite(aim, Register.r11);
                            moveFilter(destR, Register.rax);
                            store(destR, aim);
                        }
                    }
                }
            }
        }
        for (PhysicalRegister physicalRegistor : allocator.getUsedPhysicalRegisters()) {
            if (physicalRegistor.calleeSave) {
                output.printf("\tmov %s, qword [rbp + (%d)]\n",
                        physicalRegistor,
                            -midCode.frame.size - (physicalRegistor.identity + 1) * Register.size());
                }
            }
            output.printf("\tleave\n");
            output.printf("\tret\n");
    }

}
