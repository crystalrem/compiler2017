package Compiler.Back.Translator.NasmTranslator;

import Compiler.Back.Allocator.PhysicalRegister;
import Compiler.Back.MidCode.Block;
import Compiler.Back.MidCode.Intruction.ArithmeticInstruction.ArithmeticInstruction;
import Compiler.Back.MidCode.Intruction.ArithmeticInstruction.DualInstruction.*;
import Compiler.Back.MidCode.Intruction.ArithmeticInstruction.MemoryInstruction.*;
import Compiler.Back.MidCode.Intruction.ArithmeticInstruction.UnitaryInsurction.MinusInstruction;
import Compiler.Back.MidCode.Intruction.ArithmeticInstruction.UnitaryInsurction.NotInstruction;
import Compiler.Back.MidCode.Intruction.ArithmeticInstruction.UnitaryInsurction.UnitaryInstruction;
import Compiler.Back.MidCode.Intruction.ControlFlowInstruction.BranchInstruction;
import Compiler.Back.MidCode.Intruction.ControlFlowInstruction.ControlFlowInstruction;
import Compiler.Back.MidCode.Intruction.ControlFlowInstruction.JumpInstruction;
import Compiler.Back.MidCode.Intruction.FunctionInstruction.CallInstruction;
import Compiler.Back.MidCode.Intruction.FunctionInstruction.FunctionInstruction;
import Compiler.Back.MidCode.Intruction.FunctionInstruction.ReturnInstruction;
import Compiler.Back.MidCode.Intruction.Instruction;
import Compiler.Back.MidCode.Intruction.LabelIntruction.LabelInstruction;
import Compiler.Back.MidCode.Intruction.Operand.ImmediateValue;
import Compiler.Back.MidCode.Intruction.Operand.Operand;
import Compiler.Back.MidCode.Intruction.Operand.VirtualRegister.StringRegister;
import Compiler.Back.MidCode.Intruction.Operand.VirtualRegister.VariableRegister.GlobalRegister;
import Compiler.Back.MidCode.Intruction.Operand.VirtualRegister.VariableRegister.ParameterRegister;
import Compiler.Back.MidCode.Intruction.Operand.VirtualRegister.VariableRegister.TemporaryRegister;
import Compiler.Back.MidCode.Intruction.Operand.VirtualRegister.VariableRegister.VariableRegister;
import Compiler.Back.MidCode.Intruction.Operand.VirtualRegister.VirtualRegister;
import Compiler.Back.MidCode.MidCode;
import Compiler.Back.Translator.Register.Register;
import Compiler.Front.Ast.Function;
import Compiler.Utility.Error.InternalError;

import java.io.PrintStream;

/**
 * Created by crystal on 5/27/17.
 */
public class NaiveTranslator extends NasmTranslator{
    public NaiveTranslator(PrintStream output) {
        super(output);
    }
    public MidCode midCode;


    public void load(Operand from, PhysicalRegister to) {
        if (from instanceof ImmediateValue) {
            output.printf("\tmov %s, %s\n", to, from);
        } else if (from instanceof VirtualRegister) {
            if (from instanceof StringRegister) {
                output.printf("\tmov %s, %s\n", to, getStringConstantName(from));
            } else if (from instanceof VariableRegister) {
                if (from instanceof GlobalRegister) {
                    output.printf("\tmov %s, qword [rel %s]\n", to, getGlobalVariableName(from));//????
                } else if (from instanceof TemporaryRegister) {
                    output.printf("\tmov %s, qword [%s + %d]\n", to, Register.rsp, midCode.frame.getOffset(from));
                } else if (from instanceof ParameterRegister) {
                    output.printf("\tmov %s, qword [%s + %d]\n", to, Register.rsp, midCode.frame.getOffset(from));
                }
            }
        }
    }

    public void store(VirtualRegister from, PhysicalRegister to) {
        if (from instanceof StringRegister) {
            throw new InternalError();
        } else if (from instanceof VariableRegister) {
            if (from instanceof GlobalRegister) {
                output.printf("\tmov qword [rel %s], %s\n",getGlobalVariableName(from), to);
            } else if (from instanceof TemporaryRegister) {
                output.printf("\tmov qword [%s + %d], %s\n", Register.rsp, midCode.frame.getOffset(from), to);
            } else if (from instanceof ParameterRegister) {
                output.printf("\tmov qword [%s + %d], %s\n", Register.rsp, midCode.frame.getOffset(from), to);
            }
        }
    }

    @Override
    public void translate(MidCode midCode) {
        this.midCode = midCode;
        output.printf("%s:\n", getFunctionName(midCode.function));
        /*push   rbp
	    mov    rbp, rsp*/
        output.printf("\tpush   rbp\n");
        output.printf("\tmov    rbp, rsp\n");
        output.printf("\tsub %s, %d\n", Register.rsp, midCode.frame.size - 8);
        //output.printf("\tmov qword [%s + %d], %s \n", Register.rsp,  midCode.frame.size - Register.rbp.identity * Register.size(), Register.rbp);
        for (int blockId = 0; blockId < midCode.blocks.size(); ++blockId) {
            Block block = midCode.blocks.get(blockId);
            output.printf("\n%s:\n", getBlockName(block));//BlockLabel
            for (Instruction instruction : block.instructions) {
				output.printf("\n;\t%s\n", instruction.toString());
                if (instruction instanceof ArithmeticInstruction) {
                    if (instruction instanceof DualInstruction) {
                        DualInstruction instr = (DualInstruction)instruction;

                        if(instr instanceof AdditionInstruction) {
                            load(instr.source, Register.r12);
                            load(instr.source2, Register.r13);
                            output.printf("\tadd r12, r13\n");
                            store(instr.aim, Register.r12);
                        }
                        else if(instr instanceof AndInstruction) {
                            load(instr.source, Register.r12);
                            load(instr.source2, Register.r13);
                            output.printf("\tand r12, r13\n");
                            store(instr.aim, Register.r12);
                        }
                        else if(instr instanceof DivideInstruction) {//??

                            load(instr.source, Register.rax);
                            load(instr.source2, Register.r13);
                            output.printf("\tcqo\n");
                            output.printf("\tidiv r13\n");
                            store(instr.aim, Register.rax);
                        }
                        else if(instr instanceof EqualInstruction) {
                            load(instr.source, Register.r12);
                            load(instr.source2, Register.r13);
                            output.printf("\tcmp r12, r13\n");
                            output.printf("\tsete al\n");
                            output.printf("\tmovzx r12, al\n");
                            store(instr.aim, Register.r12);
                        }
                        else if(instr instanceof LessInstruction) {
                            load(instr.source, Register.r12);
                            load(instr.source2, Register.r13);
                            output.printf("\tcmp r12, r13\n");
                            output.printf("\tsetl al\n");
                            output.printf("\tmovzx r12, al\n");
                            store(instr.aim, Register.r12);
                        }
                        else if(instr instanceof ModuleInstruction) {//??

                            load(instr.source, Register.rax);
                            load(instr.source2, Register.r13);
                            output.printf("\tcqo\n");
                            output.printf("\tidiv r13\n");
                            store(instr.aim, Register.rdx);
                        }
                        else if(instr instanceof MoreInstruction) {
                            load(instr.source, Register.r12);
                            load(instr.source2, Register.r13);
                            output.printf("\tcmp r12, r13\n");
                            output.printf("\tsetg al\n");
                            output.printf("\tmovzx r12, al\n");
                            store(instr.aim, Register.r12);
                        }
                        else if(instr instanceof MoveLeftInstruction) {
                            load(instr.source, Register.r12);
                            load(instr.source2, Register.rcx);
                            output.printf("\tsal r12, cl\n");
                            store(instr.aim, Register.r12);
                        }
                        else if(instr instanceof MoveRightInstruction) {
                            load(instr.source, Register.r12);
                            load(instr.source2, Register.rcx);
                            output.printf("\tsar r12, cl\n");
                            store(instr.aim, Register.r12);
                        }
                        else if(instr instanceof MultiplyInstruction) {
                            load(instr.source, Register.r12);
                            load(instr.source2, Register.r13);
                            output.printf("\timul r12, r13\n");
                            store(instr.aim, Register.r12);
                        }
                        else if(instr instanceof NoLessInstruction) {
                            load(instr.source, Register.r12);
                            load(instr.source2, Register.r13);
                            output.printf("\tcmp r12, r13\n");
                            output.printf("\tsetge al\n");
                            output.printf("\tmovzx r12, al\n");
                            store(instr.aim, Register.r12);
                        }
                        else if(instr instanceof NoMoreInstruction) {
                            load(instr.source, Register.r12);
                            load(instr.source2, Register.r13);
                            output.printf("\tcmp r12, r13\n");
                            output.printf("\tsetle al\n");
                            output.printf("\tmovzx r12, al\n");
                            store(instr.aim, Register.r12);
                        }
                        else if(instr instanceof OrInstruction) {
                            load(instr.source, Register.r12);
                            load(instr.source2, Register.r13);
                            output.printf("\tor r12, r13\n");
                            store(instr.aim, Register.r12);
                        }
                        else if(instr instanceof SubtractInstruction) {
                            load(instr.source, Register.r12);
                            load(instr.source2, Register.r13);
                            output.printf("\tsub r12, r13\n");
                            store(instr.aim, Register.r12);
                        }
                        else if(instr instanceof UnEqualInstruction) {
                            load(instr.source, Register.r12);
                            load(instr.source2, Register.r13);
                            output.printf("\tcmp r12, r13\n");
                            output.printf("\tsetne al\n");
                            output.printf("\tmovzx r12, al\n");
                            store(instr.aim, Register.r12);
                        }
                        else if(instr instanceof XorInstruction) {
                            load(instr.source, Register.r12);
                            load(instr.source2, Register.r13);
                            output.printf("\txor r12, r13\n");
                            store(instr.aim, Register.r12);
                        }
                    }
                    else if (instruction instanceof UnitaryInstruction) {
                        UnitaryInstruction instr = (UnitaryInstruction)instruction;
                        load(instr.source, Register.r12);
                        if(instr instanceof MinusInstruction) {
                            output.printf("\tneg r12\n");
                        }
                        else if(instr instanceof NotInstruction) {
                            output.printf("\tnot r12\n");
                        }
                        store(instr.aim, Register.r12);
                    }
                }
                else if (instruction instanceof ControlFlowInstruction) {
                    if (instruction instanceof BranchInstruction) {
                        BranchInstruction instr = (BranchInstruction)instruction;
                        load(instr.condition, Register.r12);
                        output.printf("\tcmp r12, 1\n");
                        output.printf("\tjnz %s\n", getBlockName(instr.falseLabel.block));
                        if (blockId + 1 == midCode.blocks.size() || midCode.blocks.get(blockId + 1) != instr.trueLabel.block) {
                            output.printf("\tjmp %s\n", getBlockName(instr.trueLabel.block));
                        }
                    } else if (instruction instanceof JumpInstruction) {
                        JumpInstruction i = (JumpInstruction)instruction;
                        if (blockId + 1 == midCode.blocks.size() || midCode.blocks.get(blockId + 1) != i.label.block) {
                            output.printf("\tjmp %s\n", getBlockName(i.label.block));
                        }
                    }
                }
                else if(instruction instanceof FunctionInstruction) {
                    if (instruction instanceof CallInstruction) {
                        CallInstruction instr = (CallInstruction) instruction;
                        Function function = instr.function;
                        if (function.name.startsWith("____builtin")) {
                            if (instr.parameters.size() >= 1) {
                                load(instr.parameters.get(0), Register.rdi);
                            }
                            if (instr.parameters.size() >= 2) {
                                load(instr.parameters.get(1), Register.rsi);
                            }
                            if (instr.parameters.size() >= 3) {
                                load(instr.parameters.get(2), Register.rdx);
                            }
                            if (instr.parameters.size() >= 4) {
                                load(instr.parameters.get(3), Register.rcx);
                            }
                        }
                        else {
                            for (int i = 0; i < function.parameters.size(); i++) {
                                load(instr.parameters.get(i), Register.r12);
                                /*if(function.midCode == null) {
                                    System.out.println(function.name);
                                    System.out.println(function.parameters.get(i).name);
                                }*/
                                int offset = function.midCode.frame.size - function.midCode.frame.getOffset(function.parameters.get(i).register);
                                //System.err.print("***Offset = " + Integer.toString(offset) +" = " + Integer.toString(function.midCode.frame.size)+ " - " + Integer.toString(function.midCode.frame.getOffset(function.parameters.get(i).register))+ "\n");
                                output.printf("\tmov qword [%s - %d], %s\n", Register.rsp, offset + 8, Register.r12);
                            }
                        }
                        output.printf("\tcall %s\n", getFunctionName(function));
                        if (instr.aim != null) {
                            store(instr.aim, Register.rax);
                            //aim = f();
                        }
                    }
                    else if (instruction instanceof ReturnInstruction) {
                        ReturnInstruction instr = (ReturnInstruction) instruction;
                        load(instr.source, Register.rax);
                        output.printf("\tmov qword [rbp - %d], %s\n", Register.rax.identity * Register.size(), Register.rax);
                        output.printf("\tmov rax, qword [rbp - %d]\n", Register.rax.identity * Register.size());
                        //output.printf("\tadd rsp, %d\n", midCode.frame.size);
                        //output.printf("\tmov rbp, qword [rbp - %d]\n", Register.rbp.identity * Register.size());
                        output.printf("\tleave\n\tret\n");

                    }
                }
                else if (instruction instanceof MemoryInstruction) {//???
                    if (instruction instanceof AllocateInstruction) {
                        AllocateInstruction instr = (AllocateInstruction)instruction;

                        load(instr.size, Register.r12);
                        //output.printf("\tadd r12, 8\n");
                        output.printf("\tmov rdi, r12\n");
                        output.printf("\tcall malloc\n");
                        store(instr.aim, Register.rax);
                        //output.printf("\tmov r12, rax\n");
                        //output.printf("\tadd r12, 0\n");//???
                        //output.printf("\t r12, 0\n");
                    } else if (instruction instanceof LoadInstruction) {
                        LoadInstruction instr = (LoadInstruction)instruction;
                        load(instr.address.origin, Register.r12);
                        output.printf("\tmov %s, qword [%s + %d]\n", Register.r13, Register.r12, instr.address.offset.value);
                        store(instr.aim, Register.r13);
                    } else if (instruction instanceof MoveInstruction) {
                        MoveInstruction instr = (MoveInstruction)instruction;
                        load(instr.source, Register.r12);
                        store(instr.aim, Register.r12);
                    } else if (instruction instanceof StoreInstruction) {
                        StoreInstruction instr = (StoreInstruction)instruction;
                        load(instr.source, Register.r12);
                        load(instr.address.origin, Register.r13);
                        output.printf("\tmov qword [%s + %d], %s\n", Register.r13, instr.address.offset.value, Register.r12);
                    }
                }
            }
        }
        output.printf("\tmov rax, qword [rbp - %d]\n", Register.rax.identity * Register.size());
        //output.printf("\tadd rsp, %d\n", midCode.frame.size);
        //output.printf("\tmov rbp, qword [rbp - %d]\n", Register.rbp.identity * Register.size());
        output.printf("\tleave\n\tret\n");
    }
}
