package Compiler.Back.Translator.NasmTranslator;

import Compiler.Back.MidCode.Block;
import Compiler.Back.MidCode.Intruction.Operand.Operand;
import Compiler.Back.MidCode.Intruction.Operand.VirtualRegister.StringRegister;
import Compiler.Back.MidCode.Intruction.Operand.VirtualRegister.VariableRegister.GlobalRegister;
import Compiler.Back.MidCode.Intruction.Operand.VirtualRegister.VirtualRegister;
import Compiler.Back.MidCode.MidCode;
import Compiler.Back.Translator.Translator;
import Compiler.Enviroment.Environment;
import Compiler.Front.Ast.Function;

import java.io.*;

/**
 * Created by crystal on 5/27/17.
 */
public class NasmTranslator extends Translator {

    public NasmTranslator(PrintStream output) {
        super(output);
    }


    @Override
    public void translate(MidCode midCode) {

    }

    String getFunctionName(Function function) {
        if (function.name.equals("main") || function.name.startsWith("____builtin")) {
            return function.name;
        }
        else {
            return String.format("____%s____function", function.name);
        }
    }

    String getBlockName(Block block) {
        return String.format("____%s_%d____%s", block.function.name, block.identity, block.name);
    }

    String getGlobalVariableName(Operand operand) {
        return String.format("____global_%d____variable", ((VirtualRegister)operand).identity);
    }

    String getStringConstantName(Operand operand) {
        return String.format("____global_%d____string", ((VirtualRegister)operand).identity);
    }



    @Override
    public void translate() throws IOException {
        output.println("\tglobal main");
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream("./lib/newlibrary.s")));
        String line = reader.readLine();
        while (!line.startsWith("SECTION .data")) {
            output.println(line);
            line = reader.readLine();
        }
        for (Function function : Environment.program.functions) {
            translate(function.midCode);
        }
        while (line != null) {
            output.println(line);
            line = reader.readLine();
        }
        for (VirtualRegister register : Environment.registerTable.registerTable) {
            if (register instanceof StringRegister) {
                String string = ((StringRegister)register).value;
                output.printf("%s:\n", getStringConstantName(register));
                char[] ch = string.toCharArray();
                output.printf("\tdb ");
                for (int i = 0; i < ch.length; i++)
                if(i < ch.length - 1 && ch[i] == '\\')
                {
                    if(ch[i + 1] == 'n') {
                        output.print("10, ");
                        i++;
                    }
                    else if(ch[i + 1] == '\\') {
                        output.print("92, ");
                        i++;
                    }
                    else if(ch[i + 1] == '\"') {
                        output.print("34, ");
                        i++;

                    }
                    else output.print("\"" +  "\\" +  "\"," );
                }
                else {
                    /*if(ch[i] == ' ') {
                        output.printf("32, ");
                    }*/
                    output.printf("\"%c\", ", ch[i]);
                }
                output.printf("0\n");
                //output.printf("\tdb \"%s\", 0\n", string);

            } else if (register instanceof GlobalRegister) {
                output.printf("%s:\n", getGlobalVariableName(register));
                output.printf("\tdq %d\n", 0);
            }
        }

    }

}
