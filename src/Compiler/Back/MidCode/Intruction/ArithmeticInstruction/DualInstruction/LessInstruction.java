package Compiler.Back.MidCode.Intruction.ArithmeticInstruction.DualInstruction;

import Compiler.Back.MidCode.Intruction.ArithmeticInstruction.MemoryInstruction.MoveInstruction;
import Compiler.Back.MidCode.Intruction.Instruction;
import Compiler.Back.MidCode.Intruction.Operand.ImmediateValue;
import Compiler.Back.MidCode.Intruction.Operand.Operand;
import Compiler.Back.MidCode.Intruction.Operand.VirtualRegister.VirtualRegister;

/**
 * Created by crystal on 5/23/17.
 */
public class LessInstruction extends DualInstruction{
    public LessInstruction(VirtualRegister aim, Operand source, Operand source2) {

        super(aim, source, source2);
        /*if(this.aim == null) {
            System.out.print("!aim\n");
        }
        if(this.source == null) {
            System.out.print("!source\n");
        }
        if(this.source2 == null) {
            System.out.print("!source2\n");
        }*/
    }
    public static Instruction getInstruction(Operand aim, Operand source, Operand source2) {
        /*if(aim == null) {
            System.out.print("!aim\n");
        }
        if(source == null) {
            System.out.print("!source\n");
        }
        if(source2 == null) {
            System.out.print("!source2\n");
        }*/
        if(aim instanceof VirtualRegister) {
            if (source instanceof ImmediateValue && source2 instanceof ImmediateValue) {
                int num1 = ((ImmediateValue) source).value;
                int num2 = ((ImmediateValue) source2).value;
                return MoveInstruction.getInstruction((VirtualRegister)aim, new ImmediateValue(num1 < num2 ? 1 : 0));
            } else if (source instanceof ImmediateValue) {
                Operand swap = source;
                source = source2;
                source2 = swap;
            }
            return new LessInstruction((VirtualRegister)aim, source, source2);
        }
        else {
            throw new InternalError("InternalError in [LessInstruction] !(this.aim instanceof VirtualRegister)");
        }
    }
    @Override
    public String toString() {
        return "less " + aim.toString() + " " + source.toString() + " " + source2.toString();
    }
}
