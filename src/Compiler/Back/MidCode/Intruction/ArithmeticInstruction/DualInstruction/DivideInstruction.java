package Compiler.Back.MidCode.Intruction.ArithmeticInstruction.DualInstruction;

import Compiler.Back.MidCode.Intruction.ArithmeticInstruction.MemoryInstruction.MoveInstruction;
import Compiler.Back.MidCode.Intruction.Instruction;
import Compiler.Back.MidCode.Intruction.Operand.ImmediateValue;
import Compiler.Back.MidCode.Intruction.Operand.Operand;
import Compiler.Back.MidCode.Intruction.Operand.VirtualRegister.VirtualRegister;

/**
 * Created by crystal on 5/23/17.
 */
public class DivideInstruction extends DualInstruction{
    public DivideInstruction(VirtualRegister aim, Operand source, Operand source2) {
        super(aim, source, source2);
    }
    public static Instruction getInstruction(Operand aim, Operand source, Operand source2) {
        if(aim instanceof VirtualRegister) {
            if (source instanceof ImmediateValue && source2 instanceof ImmediateValue) {
                int num1 = ((ImmediateValue) source).value;
                int num2 = ((ImmediateValue) source2).value;
                if(num2 != 0) {
                    return MoveInstruction.getInstruction((VirtualRegister)aim, new ImmediateValue(num1 / num2));
                }
            } else if (source instanceof ImmediateValue) {
                Operand swap = source;
                source = source2;
                source2 = swap;
            }
            return new DivideInstruction((VirtualRegister)aim, source, source2);
        }
        else {
            throw new InternalError("InternalError in [DivideInstruction] !(this.aim instanceof VirtualRegister)");
        }
    }
    @Override
    public String toString() {
        return String.format("div %s %s %s", aim.toString(), source.toString(), source2.toString());
    }
}
