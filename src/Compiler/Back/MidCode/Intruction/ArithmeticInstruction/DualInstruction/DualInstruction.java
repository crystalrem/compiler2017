package Compiler.Back.MidCode.Intruction.ArithmeticInstruction.DualInstruction;

import Compiler.Back.MidCode.Intruction.ArithmeticInstruction.ArithmeticInstruction;
import Compiler.Back.MidCode.Intruction.Operand.Operand;
import Compiler.Back.MidCode.Intruction.Operand.VirtualRegister.VirtualRegister;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by crystal on 5/23/17.
 */
public class DualInstruction extends ArithmeticInstruction{
    public VirtualRegister aim;
    public Operand source, source2;
    public DualInstruction(VirtualRegister aim, Operand source, Operand source2) {
        this.source = source;
        this.source2 = source2;
        this.aim = aim;
    }
    @Override
    public List<Operand> getDefinedOperands() {
        return Arrays.asList((Operand) aim);
    }

    @Override
    public List<Operand> getUsedOperands() {
        return Arrays.asList(source, source2);
    }

}
