package Compiler.Back.MidCode.Intruction.ArithmeticInstruction.DualInstruction;

import Compiler.Back.MidCode.Intruction.Instruction;
import Compiler.Back.MidCode.Intruction.Operand.ImmediateValue;
import Compiler.Back.MidCode.Intruction.Operand.Operand;
import Compiler.Back.MidCode.Intruction.Operand.VirtualRegister.VirtualRegister;

/**
 * Created by crystal on 5/23/17.
 */
public class OrInstruction extends DualInstruction{
    public OrInstruction(VirtualRegister aim, Operand source, Operand source2) {
        super(aim, source, source2);
    }
    public static Instruction getInstruction(Operand aim, Operand source, Operand source2) {
        if(aim instanceof VirtualRegister) {
            return new OrInstruction((VirtualRegister)aim, source, source2);
        }
        else {
            throw new InternalError("InternalError in [OrInstruction] !(this.aim instanceof VirtualRegister)");
        }
    }
    @Override
    public String toString() {
        return String.format("or %s %s %s", aim.toString(), source.toString(), source2.toString());
    }
}
