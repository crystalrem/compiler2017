package Compiler.Back.MidCode.Intruction.ArithmeticInstruction.UnitaryInsurction;

import Compiler.Back.MidCode.Intruction.ArithmeticInstruction.MemoryInstruction.MoveInstruction;
import Compiler.Back.MidCode.Intruction.Instruction;
import Compiler.Back.MidCode.Intruction.Operand.ImmediateValue;
import Compiler.Back.MidCode.Intruction.Operand.Operand;
import Compiler.Back.MidCode.Intruction.Operand.VirtualRegister.VirtualRegister;
import Compiler.Front.Ast.Expr.UnitaryExpr.UnitaryExpr;

/**
 * Created by crystal on 5/23/17.
 */
public class MinusInstruction extends UnitaryInstruction{
    public MinusInstruction(VirtualRegister aim, Operand source) {
        super(aim, source);
    }
    public static Instruction getInstruction(Operand aim, Operand source) {
        if(aim instanceof VirtualRegister) {
            if (source instanceof ImmediateValue) {
                int value = ((ImmediateValue)source).value;
                return MoveInstruction.getInstruction((VirtualRegister)aim, new ImmediateValue(-value));
            }
            else return new MinusInstruction((VirtualRegister)aim, source);
        }
        else {
            throw new InternalError("InternalError in [MinusInstruction] !(aim instanceof VirtualRegister)");
        }
    }
    @Override
    public String toString() {
        return "minus " + aim.toString() + " " + source.toString();
    }
}
