package Compiler.Back.MidCode.Intruction.ArithmeticInstruction.UnitaryInsurction;

import Compiler.Back.MidCode.Intruction.ArithmeticInstruction.ArithmeticInstruction;
import Compiler.Back.MidCode.Intruction.Operand.Operand;
import Compiler.Back.MidCode.Intruction.Operand.VirtualRegister.VirtualRegister;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by crystal on 5/23/17.
 */
public class UnitaryInstruction extends ArithmeticInstruction{
    public VirtualRegister aim;
    public Operand source;

    protected UnitaryInstruction(VirtualRegister aim, Operand source) {
        this.aim = aim;
        this.source = source;
    }
    @Override
    public List<Operand> getDefinedOperands() {
        return Arrays.asList((Operand) aim);
    }

    @Override
    public List<Operand> getUsedOperands() {
        return Arrays.asList(source);
    }

}
