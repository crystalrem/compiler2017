package Compiler.Back.MidCode.Intruction.ArithmeticInstruction.MemoryInstruction;

import Compiler.Back.MidCode.Intruction.Instruction;
import Compiler.Back.MidCode.Intruction.Operand.Operand;
import Compiler.Back.MidCode.Intruction.Operand.VirtualRegister.Address;
import Compiler.Utility.Error.InternalError;

import java.util.Arrays;
import java.util.List;

/**
 * Created by crystal on 5/23/17.
 */
public class StoreInstruction extends MemoryInstruction{
    public Operand source;
    public Address address;

    public StoreInstruction(Operand source, Address address) {
        this.source = source;
        this.address = address;
    }

    public static Instruction getInstruction(Operand source, Operand address) {
        if (address instanceof Address) {
            return new StoreInstruction(source, (Address)address);
        }
        else {
            throw new InternalError("InternalError in [StoreInstruction] !(address instanceof Address)");
        }
    }
    @Override
    public List<Operand> getDefinedOperands() {
        return Arrays.asList();
    }

    @Override
    public List<Operand> getUsedOperands() {
        return Arrays.asList(source, address.origin);
    }

    @Override
    public String toString() {
        return "store " + address.toString() + source.toString();
    }

}
