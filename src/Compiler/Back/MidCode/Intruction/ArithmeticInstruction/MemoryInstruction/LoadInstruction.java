package Compiler.Back.MidCode.Intruction.ArithmeticInstruction.MemoryInstruction;

import Compiler.Back.MidCode.Intruction.Instruction;
import Compiler.Back.MidCode.Intruction.Operand.Operand;
import Compiler.Back.MidCode.Intruction.Operand.VirtualRegister.Address;
import Compiler.Back.MidCode.Intruction.Operand.VirtualRegister.VirtualRegister;
import Compiler.Utility.Error.InternalError;

import java.util.Arrays;
import java.util.List;

/**
 * Created by crystal on 5/23/17.
 */
public class LoadInstruction extends MemoryInstruction{
    public VirtualRegister aim;
    public Address address;

    public LoadInstruction(VirtualRegister destination, Address address) {
        this.aim = destination;
        this.address = address;
    }

    public static Instruction getInstruction(Operand aim, Operand address) {
        if (aim instanceof VirtualRegister && address instanceof Address) {
            return new LoadInstruction((VirtualRegister)aim, (Address)address);
        }
        throw new InternalError("InternalError in [LoadInstruction] !(aim instanceof VirtualRegister && address instanceof Address)");
    }

    @Override
    public List<Operand> getDefinedOperands() {
        return Arrays.asList((Operand) aim);
    }

    @Override
    public List<Operand> getUsedOperands() {
        return Arrays.asList((Operand) address.origin);
    }

    @Override
    public String toString() {
        return "ld " + aim.toString() + address.toString();
    }
}
