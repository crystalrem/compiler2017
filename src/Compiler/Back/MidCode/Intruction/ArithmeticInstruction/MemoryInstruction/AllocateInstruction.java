package Compiler.Back.MidCode.Intruction.ArithmeticInstruction.MemoryInstruction;

import Compiler.Back.MidCode.Intruction.Instruction;
import Compiler.Back.MidCode.Intruction.Operand.Operand;
import Compiler.Back.MidCode.Intruction.Operand.VirtualRegister.VirtualRegister;
import Compiler.Utility.Error.InternalError;

import java.util.Arrays;
import java.util.List;

/**
 * Created by crystal on 5/23/17.
 */
public class AllocateInstruction extends MemoryInstruction{
    public VirtualRegister aim;
    public Operand size;
    public AllocateInstruction(VirtualRegister destination, Operand size) {
        this.aim = destination;
        this.size = size;
    }

    public static Instruction getInstruction(Operand aim, Operand size) {
        if (aim instanceof VirtualRegister) {
            return new AllocateInstruction((VirtualRegister)aim, size);
        }
        throw new InternalError("InternalError in [AllocateInstruction] !(aim instanceof VirtualRegister)");
    }

    @Override
    public List<Operand> getDefinedOperands() {
        return Arrays.asList((Operand) aim);
    }

    @Override
    public List<Operand> getUsedOperands() {
        return Arrays.asList(size);
    }

    @Override
    public String toString() {
        return "malloc " + " " + aim.toString() + " " + size.toString() ;
    }
}
