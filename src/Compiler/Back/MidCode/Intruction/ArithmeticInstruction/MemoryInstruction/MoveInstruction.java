package Compiler.Back.MidCode.Intruction.ArithmeticInstruction.MemoryInstruction;

import Compiler.Back.MidCode.Intruction.Instruction;
import Compiler.Back.MidCode.Intruction.Operand.Operand;
import Compiler.Back.MidCode.Intruction.Operand.VirtualRegister.Address;
import Compiler.Back.MidCode.Intruction.Operand.VirtualRegister.VirtualRegister;
import Compiler.Utility.Error.InternalError;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by crystal on 5/23/17.
 */
    public class MoveInstruction extends MemoryInstruction{
    public VirtualRegister aim;
    public Operand source;

    public MoveInstruction(VirtualRegister aim, Operand source) {
        this.aim = aim;
        this.source = source;
    }

    public static Instruction getInstruction(Operand aim, Operand source) {
        if (aim instanceof VirtualRegister) {
            return new MoveInstruction((VirtualRegister)aim, source);
        }
        throw new InternalError("InternalError in [MoveInstruction] !(aim instanceof VirtualRegister)");
    }
    @Override
    public List<Operand> getDefinedOperands() {
        return Arrays.asList((Operand) aim);
    }

    @Override
    public List<Operand> getUsedOperands() {
        return Arrays.asList(source);
    }

    @Override
    public String toString() {
        return "mov " + aim.toString() + " " + source.toString();
    }
}
