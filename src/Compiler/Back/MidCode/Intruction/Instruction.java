package Compiler.Back.MidCode.Intruction;

import Compiler.Back.MidCode.Intruction.Operand.Operand;
import Compiler.Back.MidCode.Intruction.Operand.VirtualRegister.VirtualRegister;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by crystal on 5/9/17.
 */
public abstract class Instruction {
    public abstract List<Operand> getDefinedOperands();
    public abstract List<Operand> getUsedOperands();
    public List<VirtualRegister> getDefinedRegisters() {
        return new ArrayList<VirtualRegister>() {{
            for (Operand operand : getDefinedOperands()) {
                if (operand instanceof VirtualRegister) {
                    add((VirtualRegister)operand);
                }
            }
        }};
    }

    public List<VirtualRegister> getUsedRegisters() {
        return new ArrayList<VirtualRegister>() {{
            for (Operand operand : getUsedOperands()) {
                if (operand instanceof VirtualRegister) {
                    add((VirtualRegister)operand);
                }
            }
        }};
    }
    public abstract String toString();
}
