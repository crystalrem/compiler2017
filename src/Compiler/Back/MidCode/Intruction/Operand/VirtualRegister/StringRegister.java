package Compiler.Back.MidCode.Intruction.Operand.VirtualRegister;

/**
 * Created by crystal on 5/18/17.
 */
public class StringRegister extends VirtualRegister{

    public String value;

    public StringRegister(String value) {
            this.value = value;
        }
    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append("(StringRegister): $" + identity);
        s.append("{ = " + "'" + value + "'}");
        return s.toString();
    }

}
