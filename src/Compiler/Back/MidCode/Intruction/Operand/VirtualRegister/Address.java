package Compiler.Back.MidCode.Intruction.Operand.VirtualRegister;

import Compiler.Back.MidCode.Intruction.Operand.ImmediateValue;
import Compiler.Back.MidCode.Intruction.Operand.Operand;

/**
 * Created by crystal on 5/21/17.
 */
public class Address extends Operand{
    public VirtualRegister origin;
    public ImmediateValue offset;
    public int size;
    public Address(VirtualRegister origin, ImmediateValue offset, int size) {
        if(size != 1 && size != 8) {
            throw new InternalError("Error in Address (size != 1 && size != 8)");
        }
        this.offset = offset;
        this.origin = origin;
        this.size = size;
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append("(Address): $[");
        s.append(origin.toString());
        s.append(" + " + offset.toString() + "]");
        return s.toString();
    }
}
