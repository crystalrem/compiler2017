package Compiler.Back.MidCode.Intruction.Operand.VirtualRegister;

import Compiler.Back.MidCode.Intruction.Operand.Operand;
import Compiler.Enviroment.Environment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by crystal on 5/17/17.
 */
public abstract class VirtualRegister extends Operand{
    public int identity;
    public List<VirtualRegister>clones;

    public VirtualRegister() {
        this.identity = Environment.registerTable.registerTable.size();
        this.clones = new ArrayList<>();
    }

    public VirtualRegister clone() {
        VirtualRegister newClone = new CloneRegister(this, clones.size());
        clones.add(newClone);
        return newClone;
    }
    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append("(Register): $");
        s.append(Integer.toString(identity));
        return s.toString();
    }


}
