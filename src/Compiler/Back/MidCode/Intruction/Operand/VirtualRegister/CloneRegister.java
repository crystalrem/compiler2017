package Compiler.Back.MidCode.Intruction.Operand.VirtualRegister;

import Compiler.Back.MidCode.Intruction.Operand.Operand;
import Compiler.Front.Ast.Statement.VariableDefinition;

/**
 * Created by crystal on 5/18/17.
 */
public class CloneRegister extends VirtualRegister {
    public VirtualRegister origin;
    int version;
    public CloneRegister(VirtualRegister origin, int version) {
        this.origin = origin;
        this.version = version;
    }
    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append("(CloneRegister): $");
        s.append(origin.toString());
        s.append("{." + Integer.toString(version) + " }");
        return s.toString();
    }
}
