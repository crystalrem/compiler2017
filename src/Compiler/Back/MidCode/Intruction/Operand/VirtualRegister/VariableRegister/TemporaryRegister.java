package Compiler.Back.MidCode.Intruction.Operand.VirtualRegister.VariableRegister;

import Compiler.Enviroment.SymbolTable.Symbol;

/**
 * Created by crystal on 5/18/17.
 */
public class TemporaryRegister extends VariableRegister{
    public TemporaryRegister(Symbol symbol) {
        super(symbol);
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append("$[Temporary]" + Integer.toString(identity) + "~" + (symbol== null ? "null" : symbol.name));
        return s.toString();
    }
}
