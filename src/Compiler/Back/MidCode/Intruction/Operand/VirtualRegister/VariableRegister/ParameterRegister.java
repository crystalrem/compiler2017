package Compiler.Back.MidCode.Intruction.Operand.VirtualRegister.VariableRegister;

import Compiler.Enviroment.SymbolTable.Symbol;

/**
 * Created by crystal on 5/18/17.
 */
public class ParameterRegister extends VariableRegister{
    public ParameterRegister(Symbol symbol) {
        super(symbol);
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append("$[Parameter]" + Integer.toString(identity) + "~" + symbol.name);
        return s.toString();
    }
}
