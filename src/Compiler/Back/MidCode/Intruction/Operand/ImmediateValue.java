package Compiler.Back.MidCode.Intruction.Operand;

/**
 * Created by crystal on 5/19/17.
 */
public class ImmediateValue extends Operand{
    public int value;

    public ImmediateValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return Integer.toString(value);
    }
}
