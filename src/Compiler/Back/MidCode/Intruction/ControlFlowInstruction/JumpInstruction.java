package Compiler.Back.MidCode.Intruction.ControlFlowInstruction;

import Compiler.Back.MidCode.Intruction.Instruction;
import Compiler.Back.MidCode.Intruction.LabelIntruction.LabelInstruction;
import Compiler.Back.MidCode.Intruction.Operand.Operand;

import java.util.Arrays;
import java.util.List;

/**
 * Created by crystal on 5/23/17.
 */
public class JumpInstruction extends ControlFlowInstruction{
    public LabelInstruction label;

    public JumpInstruction(LabelInstruction label) {
        this.label = label;
    }

    public static Instruction getInstruction(LabelInstruction label) {
        return new JumpInstruction(label);
    }

    @Override
    public String toString() {
        return "jmp " + label.name;
    }

    @Override
    public List<Operand> getDefinedOperands() {
        return Arrays.asList();
    }

    @Override
    public List<Operand> getUsedOperands() {
        return Arrays.asList();
    }

}
