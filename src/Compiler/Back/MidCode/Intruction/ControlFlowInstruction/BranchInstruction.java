package Compiler.Back.MidCode.Intruction.ControlFlowInstruction;

import Compiler.Back.MidCode.Intruction.Instruction;
import Compiler.Back.MidCode.Intruction.LabelIntruction.LabelInstruction;
import Compiler.Back.MidCode.Intruction.Operand.ImmediateValue;
import Compiler.Back.MidCode.Intruction.Operand.Operand;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by crystal on 5/23/17.
 */
public class BranchInstruction extends ControlFlowInstruction{
    public Operand condition;
    public LabelInstruction trueLabel, falseLabel;

    public BranchInstruction(Operand condition, LabelInstruction trueLabel, LabelInstruction falseLabel) {
        this.condition = condition;
        this.trueLabel = trueLabel;
        this.falseLabel = falseLabel;
    }

    public static Instruction getInstruction(Operand condition, LabelInstruction trueLabel, LabelInstruction falseLabel) {
        if (condition instanceof ImmediateValue) {
            if (((ImmediateValue)condition).value == 0) {
                return JumpInstruction.getInstruction(falseLabel);
            } else {
                return JumpInstruction.getInstruction(trueLabel);
            }
        }
        return new BranchInstruction(condition, trueLabel, falseLabel);
    }
    @Override
    public List<Operand> getDefinedOperands() {
        return Arrays.asList();
    }

    @Override
    public List<Operand> getUsedOperands() {
        return Arrays.asList(condition);
    }

    @Override
    public String toString() {
        return "branch " + condition.toString() + " " + trueLabel.toString() + " " + falseLabel.toString();
    }


}
