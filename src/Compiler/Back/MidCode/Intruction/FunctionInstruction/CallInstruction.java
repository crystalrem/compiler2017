package Compiler.Back.MidCode.Intruction.FunctionInstruction;

import Compiler.Back.MidCode.Intruction.Instruction;
import Compiler.Back.MidCode.Intruction.Operand.Operand;
import Compiler.Back.MidCode.Intruction.Operand.VirtualRegister.VirtualRegister;
import Compiler.Enviroment.Environment;
import Compiler.Front.Ast.Function;
import Compiler.Front.Ast.Statement.VariableDefinition;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by crystal on 5/23/17.
 */
public class CallInstruction extends FunctionInstruction{
    public VirtualRegister aim;
    public Function function;
    public List<Operand> parameters;

    public CallInstruction(VirtualRegister destination, Function function, List<Operand> parameters) {
        this.aim = destination;
        this.function = function;
        this.parameters = parameters;
    }

    public static Instruction getInstruction(Operand aim, Function function, List<Operand> parameters) {
        if (aim == null) {
            return new CallInstruction(null, function, parameters);
        } else if (aim instanceof VirtualRegister) {
            return new CallInstruction((VirtualRegister)aim, function, parameters);
        }
        throw new InternalError();
    }

    @Override
    public List<Operand> getDefinedOperands() {
        return new ArrayList<Operand>() {{
            if (aim != null) {
                add(aim);
            }
            if (!function.name.startsWith("____builtin")) {
                for (VariableDefinition variable : Environment.program.globalVariables) {
                    add(variable.symbol.register);
                }
            }
        }};
    }

    @Override
    public List<Operand> getUsedOperands() {
        return new ArrayList<Operand>() {{
            addAll(parameters);
            if (!function.name.startsWith("____builtin")) {
                for (VariableDefinition variable : Environment.program.globalVariables) {
                    add(variable.symbol.register);
                }
            }
        }};
    }

    @Override
    public String toString() {
        String string= "Call " + (aim == null ? "null" : aim.toString()) + " Func[" + function.name + "] {";
        for (int i = 0; i < parameters.size(); i++) {
            string = string + parameters.get(i).toString() + ", ";
        }
        string = string + "}";
        return string;
    }

}
