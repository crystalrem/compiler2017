package Compiler.Back.MidCode.Intruction.FunctionInstruction;

import Compiler.Back.MidCode.Intruction.Instruction;
import Compiler.Back.MidCode.Intruction.Operand.Operand;
import Compiler.Back.MidCode.Intruction.Operand.VirtualRegister.VirtualRegister;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by crystal on 5/23/17.
 */
public class ReturnInstruction extends FunctionInstruction{
    public Operand source;

    private ReturnInstruction(Operand source) {
        this.source = source;
    }

    public static Instruction getInstruction(Operand source) {
        return new ReturnInstruction(source);
    }
    @Override
    public List<Operand> getDefinedOperands() {
        return Arrays.asList();
    }

    @Override
    public List<Operand> getUsedOperands() {
        return Arrays.asList(source);
    }

    @Override
    public String toString() {
        return "ret " + source.toString();
    }

}
