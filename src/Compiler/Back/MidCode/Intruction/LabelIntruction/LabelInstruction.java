package Compiler.Back.MidCode.Intruction.LabelIntruction;

import Compiler.Back.MidCode.Block;
import Compiler.Back.MidCode.Intruction.Instruction;
import Compiler.Back.MidCode.Intruction.Operand.Operand;
import Compiler.Front.Ast.Statement.LoopStatement.LoopStatement;

import java.util.Arrays;
import java.util.List;

/**
 * Created by crystal on 5/9/17.
 */
public class LabelInstruction extends Instruction {
    public String name;
    public Block block;

    public LabelInstruction(String string) {
        this.name = string;
    }
    public static LabelInstruction getInstruction(String string) {
        return new LabelInstruction(string);
    }

    @Override
    public List<Operand> getDefinedOperands() {
        return Arrays.asList();
    }

    @Override
    public List<Operand> getUsedOperands() {
        return Arrays.asList();
    }

    @Override
    public String toString() {
        return "<Label -- " + name +">";
    }

}
