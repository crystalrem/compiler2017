package Compiler.Back.MidCode;


import Compiler.Back.MidCode.Intruction.ArithmeticInstruction.UnitaryInsurction.NotInstruction;
import Compiler.Back.MidCode.Intruction.ControlFlowInstruction.BranchInstruction;
import Compiler.Back.MidCode.Intruction.ControlFlowInstruction.ControlFlowInstruction;
import Compiler.Back.MidCode.Intruction.ControlFlowInstruction.JumpInstruction;
import Compiler.Back.MidCode.Intruction.Instruction;
import Compiler.Back.MidCode.Intruction.LabelIntruction.LabelInstruction;
import Compiler.Back.MidCode.Intruction.Operand.Operand;
import Compiler.Back.MidCode.Intruction.Operand.VirtualRegister.VariableRegister.TemporaryRegister;
import Compiler.Back.MidCode.Intruction.Operand.VirtualRegister.VirtualRegister;
import Compiler.Back.Translator.Register.Register;
import Compiler.Enviroment.Environment;
import Compiler.Enviroment.SymbolTable.Symbol;
import Compiler.Front.Ast.Function;
import Compiler.Front.Ast.Statement.VariableDefinition;
import Compiler.Utility.Error.InternalError;

import java.util.*;

/**
 * Created by crystal on 5/9/17.
 */
public class MidCode {
    public Function function;
    public Frame frame;
    public List<Block> blocks;
    public Block enter, entry, exit;
    public ArrayList<Instruction> instructions;
    public MidCode(Function function){
        this.function = function;
        buildMidCode();
    }
    public class Frame {
        public int size;
        public Map<VirtualRegister, Integer> temporary, parameter;

        public Frame() {
            size = 0;
            temporary = new HashMap<>();
            parameter = new HashMap<>();
        }

        public int getOffset(Operand operand) {
            if (operand instanceof VirtualRegister) {
                if (temporary.containsKey(operand)) {
                    return temporary.get(operand);
                }
                if (parameter.containsKey(operand)) {
                    return parameter.get(operand);
                }
            }
            throw new InternalError();
        }
    }

    public void buildMidCode() {
        instructions = new ArrayList<>();
        function.enter = LabelInstruction.getInstruction("enter");
        function.entry = LabelInstruction.getInstruction("entry");
        function.exit = LabelInstruction.getInstruction("exit");
        instructions.add(function.enter);
        if(function.name.equals("main")) {
            for (VariableDefinition globalVariable : Environment.program.globalVariables) {
                globalVariable.emit(instructions);
            }
        }
        instructions.add(JumpInstruction.getInstruction(function.entry));
        instructions.add(function.entry);
        function.blockStatement.emit(instructions);
        instructions.add(function.exit);

        blocks = new ArrayList<>();
        for (int i = 0, j; i < instructions.size(); i = j) {
            if (!(instructions.get(i) instanceof LabelInstruction)) {
                j = i + 1;
            }
            else {
                LabelInstruction label = (LabelInstruction)instructions.get(i);
                Block block = new Block(function, label.name, blocks.size(), label);
                blocks.add(block);
                label.block = block;
                for (j = i + 1; j < instructions.size(); ++j) {
                    if (instructions.get(j) instanceof LabelInstruction) {
                        break;
                    }
                    block.instructions.add(instructions.get(j));
                    //new
                    if (instructions.get(j) instanceof ControlFlowInstruction) {
                        break;
                    }
                    //
                }
            }
        }
        //new
        for (Block block : blocks) {
            if (block.name.equals("enter")) {
                enter = block;
            } else if (block.name.equals("entry")) {
                entry = block;
            } else if (block.name.equals("exit")) {
                exit = block;
            }
        }
        //

        //for (int i = 0; i < instructions.size(); i++) {
            //System.out.print(instructions.get(i).toString() + "\n");
        //}
        refresh();
        //analysisFrame();
    }
    public MidCode refresh() {

        for(Block block : blocks) {
            List<Instruction> instructions = block.instructions;
            block.instructions = new ArrayList<>();
            for (Instruction instruction : instructions) {
                if (instruction != null) {
                    block.instructions.add(instruction);
                }
            }
        }
        //new
        refreshGraph();
        analysisLiveliness();
        //
        analysisFrame();
        return this;
    }


    public void refreshGraph() {
        for (Block block : blocks) {
            //System.err.println("[" + block.name + "]");
            block.predecessors = new ArrayList<>();
            block.successors = new ArrayList<>();
        }
        for (Block block : blocks) {
            if(block.instructions.size() != 0) {
                //System.err.println("[" + block.name + "]***");
                Instruction instruction = block.instructions.get(block.instructions.size() - 1);
                if(instruction instanceof JumpInstruction) {
                    System.err.println("[" + block.name + "]" + " JUMP " + "[" + ((JumpInstruction) instruction).label.block.name + "]");
                    block.successors.add(((JumpInstruction) instruction).label.block);
                }
                else if(instruction instanceof BranchInstruction) {
                    System.err.println("[" + block.name + "]" + " BRANCH " + "[" + ((BranchInstruction) instruction).trueLabel.block.name + "]");
                    System.err.println("[" + block.name + "]" + " BRANCH " + "[" + ((BranchInstruction) instruction).falseLabel.block.name + "]");
                    block.successors.add(((BranchInstruction) instruction).trueLabel.block);
                    block.successors.add(((BranchInstruction) instruction).falseLabel.block);
                }
            }
        }
        /*Set<Block>visit = new HashSet<>();
        blocks = dfs(enter, visit);
        for (Block block : blocks) {
            for (Block currentSuccessor : block.successors) {
                currentSuccessor.predecessors.add(block);
            }
        }
        for (Block block : blocks) {
            System.err.println(block.name + ":");
            for(Block succ: block.successors) {
                System.err.println(block.name + " " + succ.name);
            }
        }*/
    }

    public List<Block> dfs(Block currentBlock, Set<Block> visit) {
        visit.add(currentBlock);
        List<Block> list = new ArrayList<>();
        list.add(currentBlock);

        for (Block successor : currentBlock.successors) {
            if (visit.contains(successor)) {
                continue;
            }
            if (successor != exit) {
                visit.add(successor);
                list.addAll(dfs(successor, visit));
            }
        }
        if (currentBlock == enter) {
            list.add(exit);
        }
        return list;
    }

    public void analysisLiveliness() {
        for (Block block : blocks) {
            block.liveliness.used = new ArrayList<>();
            block.liveliness.defined = new ArrayList<>();
            for (Instruction instruction : block.instructions) {
                for (VirtualRegister register : instruction.getUsedRegisters()) {
                    if (!block.liveliness.defined.contains(register)) {
                        block.liveliness.used.add(register);
                    }
                }
                for (VirtualRegister register : instruction.getDefinedRegisters()) {
                    block.liveliness.defined.add(register);
                }
            }
        }
        for (Block block : blocks) {
            block.liveliness.liveIn = new HashSet<>();
            block.liveliness.liveOut = new HashSet<>();
        }
        while (true) {
            for (Block block : blocks) {
                block.liveliness.liveIn = new HashSet<>();
                for (VirtualRegister virtualRegister : block.liveliness.liveOut) {
                    block.liveliness.liveIn.add(virtualRegister);
                }
                for (VirtualRegister virtualRegister : block.liveliness.defined) {
                    block.liveliness.liveIn.remove(virtualRegister);
                }
                for (VirtualRegister virtualRegister : block.liveliness.used) {
                    block.liveliness.liveIn.add(virtualRegister);
                }
            }
            boolean modified = false;
            for (Block block : blocks) {
                Set<VirtualRegister> origin = block.liveliness.liveOut;
                block.liveliness.liveOut = new HashSet<>();
                for (Block succBlock : block.successors) {
                    block.liveliness.liveOut.addAll(succBlock.liveliness.liveIn);
                }
                if (!block.liveliness.liveOut.equals(origin)) {
                    modified = true;
                }
            }
            if (!modified) {
                break;
            }
        }
    }


    public void analysisFrame() {
        Set<VirtualRegister> registers = new HashSet<VirtualRegister>() {{
            for (int i = 0; i < blocks.size(); i++) {
                Block block = blocks.get(i);
                for (int j = 0; j < block.instructions.size(); j++) {
                    Instruction instruction = block.instructions.get(j);
                    for (VirtualRegister register : instruction.getUsedRegisters()) {
                        if (register instanceof TemporaryRegister) {
                            add(register);
                        }
                    }
                    for (VirtualRegister register : instruction.getDefinedRegisters()) {
                        if (register instanceof TemporaryRegister) {
                            add(register);
                        }
                    }
                }
            }
        }};
        frame = new Frame();
        //ForNaive
        /*for (Symbol parameter : function.parameters) {
            frame.parameter.put(parameter.register, frame.size);
            frame.size += parameter.type.size();
        }
        for (VirtualRegister register : registers) {
            frame.temporary.put(register, frame.size);
            frame.size += Register.size();
        }
        frame.size += Register.size() * Register.count();*/
        frame.size = Register.size() * 8;
        int tmp = frame.size;
        for (VirtualRegister register : registers) {
            frame.temporary.put(register, -frame.size);
            frame.size += Register.size();
        }
        for (int i = 0; i < 6 && i < function.parameters.size(); i++) {
            Symbol parameter = function.parameters.get(i);
            frame.parameter.put(parameter.register, -(i + 1) * Register.size());
        }
        for (int i = 6; i < function.parameters.size(); i++) {
            Symbol parameter = function.parameters.get(i);
            frame.parameter.put(parameter.register, (i - 6) * Register.size() + 16);
        }

    }
}
