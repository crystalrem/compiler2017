package Compiler.Back.MidCode;

import Compiler.Back.MidCode.Intruction.Instruction;
import Compiler.Back.MidCode.Intruction.Operand.VirtualRegister.VirtualRegister;
import Compiler.Front.Ast.Function;
import Compiler.Back.MidCode.Intruction.LabelIntruction.LabelInstruction;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * Created by crystal on 5/9/17.
 */
public class Block {
    public Function function;
    public String name;
    public int identity;
    public LabelInstruction label;
    public List<Instruction> instructions;
    //new
    public List<Block> successors, predecessors;
    public Liveliness liveliness;


    public Block(Function function, String name, int identity, LabelInstruction label) {
        this.function = function;
        this.name = name;
        this.identity = identity;
        this.label = label;
        this.instructions = new ArrayList<>();
        this.liveliness = new Liveliness();
    }

    //new
    public class Liveliness {
        public List<VirtualRegister> used, defined;
        public Set<VirtualRegister> liveIn, liveOut;

        public Liveliness() {
            this.used = new ArrayList<>();
            this.defined = new ArrayList<>();
            this.liveIn = new HashSet<>();
            this.liveOut = new HashSet<>();
        }
    }

    @Override
    public String toString() {
        return "Block " + function.name + "." + identity + "." + name;
    }

}
