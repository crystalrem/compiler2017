package Compiler.Enviroment;

import Compiler.Front.Ast.Type.Class.Class;
import Compiler.Utility.Error.GrammarError;

import java.util.HashMap;
import java.util.Map;

public class ClassTable {
    public Map<String, Class> classMap;
    public ClassTable () {
        classMap = new HashMap<>();
    }
    public void addClass(String string, Class currentClass) {
        if(classMap.containsKey(string)) {
            throw new GrammarError("Error in addClass -classMap.containsKey(string) :" + string);
        }
        classMap.put(string, currentClass);
    }

    public Class getClass(String string) {
        if(!classMap.containsKey(string)) {
            throw new GrammarError("Error in getClass -!classMap.containsKey(string) :" + string);
        }
        return classMap.get(string);
    }

    public boolean containClass(String string) {
        return classMap.containsKey(string);
    }
}