package Compiler.Enviroment;

import Compiler.Back.MidCode.Intruction.Operand.VirtualRegister.StringRegister;
import Compiler.Back.MidCode.Intruction.Operand.VirtualRegister.VariableRegister.GlobalRegister;
import Compiler.Back.MidCode.Intruction.Operand.VirtualRegister.VariableRegister.ParameterRegister;
import Compiler.Back.MidCode.Intruction.Operand.VirtualRegister.VariableRegister.TemporaryRegister;
import Compiler.Back.MidCode.Intruction.Operand.VirtualRegister.VariableRegister.VariableRegister;
import Compiler.Back.MidCode.Intruction.Operand.VirtualRegister.VirtualRegister;
import Compiler.Enviroment.SymbolTable.Symbol;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by crystal on 5/19/17.
 */
public class RegisterTable {
    public Set<VirtualRegister>registerTable;

    public RegisterTable() {
        registerTable = new HashSet<>();
    }

    public VirtualRegister addGlobalRegister(Symbol symbol) {
        VirtualRegister register = new GlobalRegister(symbol);
        registerTable.add(register);
        return register;
    }

    public VirtualRegister addParameterRegister(Symbol symbol) {
        VirtualRegister register = new ParameterRegister(symbol);
        registerTable.add(register);
        return register;
    }

    public VirtualRegister addTemporaryRegister(Symbol symbol) {
        VirtualRegister register = new TemporaryRegister(symbol);
        registerTable.add(register);
        return register;
    }

    public VirtualRegister addTemporaryRegister() {
        VirtualRegister register = new TemporaryRegister(null);
        registerTable.add(register);
        return register;
    }

    public VirtualRegister addStringRegister(String string) {
        VirtualRegister register  = new StringRegister(string);
        registerTable.add(register);
        return register;
    }
}
