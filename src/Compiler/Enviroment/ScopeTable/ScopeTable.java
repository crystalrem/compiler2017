package Compiler.Enviroment.ScopeTable;

import Compiler.Front.Ast.Function;
import Compiler.Front.Ast.Statement.LoopStatement.LoopStatement;
import Compiler.Front.Ast.Type.Class.Class;
import Compiler.Front.Ast.Statement.Statement;
import Compiler.Utility.Error.GrammarError;


import java.util.Stack;

public class ScopeTable {
    public Stack<Scope>scope;
    public Stack<Class> classScope;
    public Stack<Function>functionScope;
    public Stack<LoopStatement>statementScope;
    public ScopeTable() {
        scope = new Stack<>();
        classScope = new Stack<>();
        functionScope = new Stack<>();
        statementScope = new Stack<>();
    }
    public void enterScope(Scope scopeEnter) {
        scope.push(scopeEnter);
        //System.out.print("scope.push(scopeEnter);");
        //System.out.print(scopeEnter == null);
        if(scopeEnter instanceof Class) {
            classScope.push((Class)scopeEnter);
            //System.out.print("classScope.push((Class)scopeEnter);");
        }
        else if(scopeEnter instanceof Function) {
            functionScope.push((Function)scopeEnter);
        }
        else if(scopeEnter instanceof LoopStatement) {
            statementScope.push((LoopStatement)scopeEnter);
        }
    }
    public void enterClassScope(Scope scopeEnter) {
        scope.push(scopeEnter);
        classScope.push((Class)scopeEnter);
    }
    public void enterFunctionScope(Scope scopeEnter) {
        scope.push(scopeEnter);
        functionScope.push((Function)scopeEnter);
    }
    public void enterStatementScope(Scope scopeEnter) {
        scope.push(scopeEnter);
        statementScope.push((LoopStatement)scopeEnter);
    }
    public void exitScope() {
        if(scope.empty()) {
            throw new GrammarError("Error in exitScope - scope.empty()");
        }
        Scope currentScope = scope.peek();
        scope.pop();
        if(currentScope instanceof Class) {
            if(classScope.empty()) {
                throw new GrammarError("Error in exitScope -currentScope.empty() -Class");
            }
            classScope.pop();
        }
        else if(currentScope instanceof Function) {
            if(functionScope.empty()) {
                throw new GrammarError("Error in exitScope -currentScope.empty() -Function");
            }
            functionScope.pop();
        }
        else if(currentScope instanceof LoopStatement) {
            if(statementScope.empty()) {
                throw new GrammarError("Error in exitScope -currentScope.empty() -Statement");
            }
            statementScope.pop();
        }

    }
    public void exitClassScope() {
        if (scope.empty() || classScope.empty()) {
            throw new GrammarError("Error in exitClassScope -scope.empty() || classScope.empty()");
        }
        scope.pop();
        classScope.pop();
    }
    public void exitFunctionScope() {
        if (scope.empty() || functionScope.empty()) {
            throw new GrammarError("Error in exitFunctionScope -scope.empty() || functionScope.empty()");
        }
        scope.pop();
        functionScope.pop();
    }
    public void exitStatementScope() {
        if (scope.empty() || statementScope.empty()) {
            throw new GrammarError("Error in exitStatementScope -scope.empty() || statementScope.empty()");
        }
        scope.pop();
        statementScope.pop();
    }

    public Scope getScope() {
        if (scope.empty()) return null;
        return scope.peek();
    }

    public Class getClassScope() {
        if(classScope.empty()) {
            return null;
        }
        return classScope.peek();
    }

    public Function getFunctionScope() {
        if(functionScope.empty()) {
            return null;
        }
        return functionScope.peek();
    }

    public LoopStatement getLoopScope() {
        if(statementScope.empty()) {
            return null;
        }
        return statementScope.peek();
    }
    public boolean empty() {
        if(scope.size() == 1) return true;
        else return false;
    }
}