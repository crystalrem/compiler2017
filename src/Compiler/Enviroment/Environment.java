package Compiler.Enviroment;

import Compiler.Enviroment.ScopeTable.Scope;
import Compiler.Enviroment.ScopeTable.ScopeTable;
import Compiler.Enviroment.SymbolTable.Symbol;
import Compiler.Enviroment.SymbolTable.SymbolTable;
import Compiler.Front.Ast.Function;
import Compiler.Front.Ast.Program;
import Compiler.Front.Ast.Type.BasicType.BoolType;
import Compiler.Front.Ast.Type.BasicType.IntegerType;
import Compiler.Front.Ast.Type.BasicType.StringType;
import Compiler.Front.Ast.Type.BasicType.VoidType;

import java.util.ArrayList;

public class Environment {

        public static Program program;
        public static SymbolTable symbolTable;
        public static ScopeTable scopeTable;
        public static ClassTable classTable;
        public static RegisterTable registerTable;

        public static void initialize() {
            classTable = new ClassTable();
            symbolTable = new SymbolTable();
            scopeTable = new ScopeTable();
            registerTable = new RegisterTable();
            enterScope(program = Program.getProgram());
            loadBuiltIn();

        }

        public static void enterScope(Scope scope) {
            scopeTable.enterScope(scope);
            symbolTable.enterScope();
        }

        public static void exitScope() {
            symbolTable.exitScope();
            scopeTable.exitScope();
        }

        public static void loadBuiltIn() {
            symbolTable.add(Function.getFunction(
                    IntegerType.getType(),
                    "____builtin____arraySize",
                    new ArrayList<Symbol>(){{
                        add(new Symbol(VoidType.getType(), "this"));
                    }}),
                    "arraySize"
            );

            symbolTable.add(Function.getFunction(
                    VoidType.getType(),
                    "____builtin____print",
                    new ArrayList<Symbol>() {{
                        add(new Symbol(StringType.getType(), "str"));
                    }}),
                    "print"
            );

            symbolTable.add(Function.getFunction(VoidType.getType(),
                    "____builtin____println",
                    new ArrayList<Symbol>() {{
                        add(new Symbol(StringType.getType(), "str"));
                    }}),
                    "println"
            );

            symbolTable.add(Function.getFunction(VoidType.getType(),
                    "____builtin____printInt",
                    new ArrayList<Symbol>() {{
                        add(new Symbol(IntegerType.getType(), "int"));
                    }}),
                    "printInt"

            );

            symbolTable.add(Function.getFunction(
                    StringType.getType(),
                    "____builtin____getString",
                    new ArrayList<Symbol>()),
                    "getString"
            );

            symbolTable.add(Function.getFunction(
                    IntegerType.getType(),
                    "____builtin____getInt",
                    new ArrayList<Symbol>()),
                    "getInt"
            );

            symbolTable.add(Function.getFunction(
                    StringType.getType(),
                    "____builtin____toString",
                    new ArrayList<Symbol>() {{
                        add(new Symbol(IntegerType.getType(), "i"));
                    }}),
                    "toString"
            );

            symbolTable.add(Function.getFunction(
                    IntegerType.getType(),
                    "____builtin____stringLength",
                    new ArrayList<Symbol>(){{
                        add(new Symbol(StringType.getType(), "this"));
                    }}),
                    "stringLength"
            );

            symbolTable.add(Function.getFunction(
                    IntegerType.getType(),
                    "____builtin____stringParseInt",
                    new ArrayList<Symbol>(){{
                        add(new Symbol(StringType.getType(), "this"));
                    }}),
                    "stringParseInt"
            );

            symbolTable.add(Function.getFunction(
                    StringType.getType(),
                    "____builtin____stringSubString",
                    new ArrayList<Symbol>(){{
                        add(new Symbol(StringType.getType(), "this"));
                        add(new Symbol(IntegerType.getType(), "left"));
                        add(new Symbol(IntegerType.getType(), "right"));
                    }}),
                    "stringSubString"
            );

            symbolTable.add(Function.getFunction(
                    IntegerType.getType(),"____builtin____stringOrd",
                    new ArrayList<Symbol>() {{
                        add(new Symbol(StringType.getType(), "this"));
                        add(new Symbol(IntegerType.getType(), "pos"));
                    }}),
                    "stringOrd"
            );

            symbolTable.add(Function.getFunction(
                    StringType.getType(),
                    "____builtin_string____concatenate",
                    new ArrayList<Symbol>() {{
                        add(new Symbol(StringType.getType(), "lhs"));
                        add(new Symbol(StringType.getType(), "rhs"));
                    }}),
                    "____builtin_string____concatenate"
            );

            symbolTable.add(Function.getFunction(
                    BoolType.getType(),
                    "____builtin_string____equal_to",
                    new ArrayList<Symbol>() {{
                        add(new Symbol(StringType.getType(), "lhs"));
                        add(new Symbol(StringType.getType(), "rhs"));
                    }}),
                    "____builtin_string____equal_to"
            );

            symbolTable.add(Function.getFunction(
                    BoolType.getType(),
                    "____builtin_string____greater_than",
                    new ArrayList<Symbol>() {{
                        add(new Symbol(StringType.getType(), "lhs"));
                        add(new Symbol(StringType.getType(), "rhs"));
                    }}),
                    "____builtin_string____greater_than"
            );

            symbolTable.add(Function.getFunction(
                    BoolType.getType(),
                    "____builtin_string____greater_than_or_equal_to",
                    new ArrayList<Symbol>() {{
                        add(new Symbol(StringType.getType(), "lhs"));
                        add(new Symbol(StringType.getType(), "rhs"));
                    }}),
                    "____builtin_string____greater_than_or_equal_to"
            );

            symbolTable.add(Function.getFunction(
                    BoolType.getType(),
                    "____builtin_string____less_than",
                    new ArrayList<Symbol>() {{
                        add(new Symbol(StringType.getType(), "lhs"));
                        add(new Symbol(StringType.getType(), "rhs"));
                    }}),
                    "____builtin_string____less_than"
            );

            symbolTable.add(Function.getFunction(
                    BoolType.getType(),
                    "____builtin_string____less_than_or_equal_to",
                    new ArrayList<Symbol>() {{
                        add(new Symbol(StringType.getType(), "lhs"));
                        add(new Symbol(StringType.getType(), "rhs"));
                    }}),
                    "____builtin_string____less_than_or_equal_to"
            );

            symbolTable.add(Function.getFunction(
                    BoolType.getType(),
                    "____builtin_string____not_equal_to",
                    new ArrayList<Symbol>() {{
                        add(new Symbol(StringType.getType(), "lhs"));
                        add(new Symbol(StringType.getType(), "rhs"));
                    }}),
                    "____builtin_string____not_equal_to"
            );
        }


}