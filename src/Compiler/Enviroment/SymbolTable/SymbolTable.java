package Compiler.Enviroment.SymbolTable;


import Compiler.Enviroment.Environment;
import Compiler.Front.Ast.Type.Type;
import Compiler.Utility.Error.GrammarError;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;


public class SymbolTable {
    public Map<String, Stack<Symbol>> currentSymbolTable;
    public Stack<Map<String, Symbol>> symbolTableScope;
    public SymbolTable (){
        symbolTableScope = new Stack<Map<String, Symbol>>();
        currentSymbolTable = new HashMap<String, Stack<Symbol>>();
    }
    public Symbol add (Type type, String name) {
        Symbol symbol = new Symbol(type, name);
        if (symbolTableScope.peek().containsKey(name)) {
            throw new GrammarError("the scope cannot have two symbols named \"" + name + "\"");
        }
        if (!currentSymbolTable.containsKey(name)) {
            currentSymbolTable.put(name, new Stack<Symbol>());
        }
        //System.out.println("\n::Symbol add : name :" + name);
        currentSymbolTable.get(name).push(symbol);
        symbolTableScope.peek().put(name, symbol);
        return symbol;

    }

    public Symbol addGlobalVariable(Type type, String name) {
        Symbol symbol = add(type, name);
        symbol.register = Environment.registerTable.addGlobalRegister(symbol);
        return symbol;
    }

    public Symbol addTemporaryVariable(Type type, String name) {
        Symbol symbol = add(type, name);
        symbol.register = Environment.registerTable.addTemporaryRegister(symbol);
        return symbol;
    }

    public Symbol addParameterVariable(Type type, String name) {
        Symbol symbol = add(type, name);
        symbol.register = Environment.registerTable.addParameterRegister(symbol);
        return symbol;
    }

    public Symbol get(String name) {
        return (currentSymbolTable.get(name) == null ? null : currentSymbolTable.get(name).peek());
    }

    public boolean contains(String name) {
        return (currentSymbolTable.containsKey(name) && !currentSymbolTable.get(name).empty());
    }

    public void enterScope() {
        symbolTableScope.push(new HashMap<String, Symbol>());
    }

    public void exitScope() {
        Map<String, Symbol>table =  symbolTableScope.peek();
        for(String name : table.keySet()) {
            currentSymbolTable.get(name).pop();
        }
        symbolTableScope.pop();
    }


}
