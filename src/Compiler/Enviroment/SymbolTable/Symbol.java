package Compiler.Enviroment.SymbolTable;

import Compiler.Back.MidCode.Intruction.Operand.VirtualRegister.VirtualRegister;
import Compiler.Enviroment.Environment;
import Compiler.Enviroment.ScopeTable.Scope;
import Compiler.Front.Ast.Type.Class.Class;
import Compiler.Front.Ast.Type.Type;
import Compiler.Utility.AstTool.AstOutput;

public class Symbol {
    public Type type;
    public String name;
    public Scope scope;
    public VirtualRegister register;

    public Symbol(Type type, String name) {
        this.type = type;
        this.name = name;
        this.scope = Environment.scopeTable.getScope();
    }

    public static Symbol getSymbol(Type type, String name) {
        return new Symbol(type, name);
    }

    public String toString(int level) {
        StringBuilder s = new StringBuilder();
        s.append(AstOutput.outputIndent(level) + "[symbol]" + name);
        s.append(AstOutput.outputIndent(level + 1) + "[type]");
        if(this.type instanceof Class){
            s.append(((Class) this.type).name);
        }
        else {
            s.append(this.type.toString(0));
        }
        return s.toString();
    }
}