package Compiler.Utility.Error;

public class GrammarError extends Error{
    public GrammarError(String string) {
        super(string);
    }
    public GrammarError() {
        super();
    }

}