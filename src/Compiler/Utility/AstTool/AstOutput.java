package Compiler.Utility.AstTool;

public class AstOutput {
    public static String outputIndent(int level) {
        StringBuilder string = new StringBuilder();
        for (int i = 1; i <= level; i++) {
            string.append("\t");
        }
        return string.toString();
    }

}