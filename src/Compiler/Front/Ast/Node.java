package Compiler.Front.Ast;

import org.antlr.v4.runtime.tree.ParseTreeProperty;

public interface Node {
    public String toString (int level);
}