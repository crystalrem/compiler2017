package Compiler.Front.Ast.Expr.DualityExpr;

import Compiler.Back.MidCode.Intruction.ArithmeticInstruction.DualInstruction.AdditionInstruction;
import Compiler.Back.MidCode.Intruction.Instruction;
import Compiler.Enviroment.Environment;
import Compiler.Front.Ast.Expr.ConstantExpr.StringConstant;
import Compiler.Front.Ast.Expr.Expr;
import Compiler.Front.Ast.Expr.FunctionCallExpr;
import Compiler.Front.Ast.Function;
import Compiler.Front.Ast.Type.BasicType.IntegerType;
import Compiler.Front.Ast.Type.BasicType.StringType;
import Compiler.Front.Ast.Type.Type;
import Compiler.Utility.AstTool.AstOutput;
import Compiler.Utility.Error.GrammarError;

import java.util.ArrayList;
import java.util.List;

public class DualityAddExpr extends DualityExpr {

    public DualityAddExpr(Type type, boolean isLeftVal, Expr left, Expr right) {
        super(type, isLeftVal);
        this.left = left;
        this.right = right;
        this.type = type;
        this.isLeftVal = isLeftVal;
    }
    public static Expr getDualityAddExpr(Expr left, Expr right) {
        if((right.type instanceof IntegerType) && (left.type instanceof IntegerType)) {
            return new DualityAddExpr(left.type, false, left, right);
        }
        else if((right.type instanceof StringType) && (left.type instanceof StringType)) {
            System.err.println("*****************************************");
            if((right instanceof StringConstant) && (left instanceof StringConstant)) {
                String string1 = ((StringConstant)left).string;
                String string2 = ((StringConstant)right).string;
                return StringConstant.getConstant(string1 + string2);
            }
            ArrayList<Expr>parameters = new ArrayList<>();
            parameters.add(left);
            parameters.add(right);

            return FunctionCallExpr.getFunctionCallExpr(
                    ((Function)Environment.symbolTable.get("____builtin_string____concatenate").type),
                    parameters
            );
        }
        else {
            throw new GrammarError("Error in getDualityAddExpr - right.type & left.type wrong");
        }
    }
    @Override
    public String toString(int level) {
        StringBuilder string = new StringBuilder();
        string.append(AstOutput.outputIndent(level) + "[DualityAddExpr]\n");
        string.append(AstOutput.outputIndent(level + 1) + "[ExprLeft]\n");
        string.append(left.toString(level + 1));
        string.append(AstOutput.outputIndent(level + 1) + "[ExprRight]\n");
        string.append(right.toString(level + 1));
        return string.toString();
    }

    @Override
    public void emit(List<Instruction> instructions) {
        left.emit(instructions);
        left.load(instructions);
        right.emit(instructions);
        right.load(instructions);
        operand = Environment.registerTable.addTemporaryRegister(null);
        instructions.add(AdditionInstruction.getInstruction(operand, left.operand, right.operand));
    }
}