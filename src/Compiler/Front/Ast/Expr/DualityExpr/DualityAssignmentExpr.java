package Compiler.Front.Ast.Expr.DualityExpr;

import Compiler.Back.MidCode.Intruction.ArithmeticInstruction.MemoryInstruction.LoadInstruction;
import Compiler.Back.MidCode.Intruction.ArithmeticInstruction.MemoryInstruction.MoveInstruction;
import Compiler.Back.MidCode.Intruction.ArithmeticInstruction.MemoryInstruction.StoreInstruction;
import Compiler.Back.MidCode.Intruction.Instruction;
import Compiler.Back.MidCode.Intruction.Operand.VirtualRegister.Address;
import Compiler.Enviroment.Environment;
import Compiler.Front.Ast.Expr.Expr;
import Compiler.Front.Ast.Type.BasicType.VoidType;
import Compiler.Front.Ast.Type.Type;
import Compiler.Utility.AstTool.AstOutput;
import Compiler.Utility.Error.GrammarError;

import java.util.List;

public class DualityAssignmentExpr extends DualityExpr {

    public DualityAssignmentExpr(Type type, boolean isLeftVal, Expr left, Expr right) {
        super(type, isLeftVal);
        this.left = left;
        this.right = right;
        this.type = type;
        this.isLeftVal = isLeftVal;
    }
    public static DualityAssignmentExpr getDualityAssignmentExpr(Expr left, Expr right) {
        //System.out.println(left.type.toString(0) +" "+ right.type.toString(0));
        if(!left.isLeftVal) {
            throw  new GrammarError("Error in - getDualityAssignmentExpr -!left.isLeftVal");
        }
        else if(!left.type.compare(right.type)) {
            //System.out.println(left.toString(0)
            //+right.toString(0));

            throw new GrammarError("Error in - getDualityAssignmentExpr -!left.type.compare(right.type)");
        }
        return new DualityAssignmentExpr(VoidType.getType(), true, left, right);
    }
    @Override
    public String toString(int level) {
        StringBuilder string = new StringBuilder();
        string.append(AstOutput.outputIndent(level) + "[DualityAndExpr]\n");
        string.append(AstOutput.outputIndent(level + 1) + "[ExprLeft]\n");
        string.append(left.toString(level + 1));
        string.append(AstOutput.outputIndent(level + 1) + "[ExprRight]\n");
        string.append(right.toString(level + 1));
        return string.toString();
    }

    @Override
    public void emit(List<Instruction> instructions) {
        left.emit(instructions);
        right.emit(instructions);
        right.load(instructions);
        operand = left.operand;
        if (left.operand instanceof Address) {
            instructions.add(StoreInstruction.getInstruction(right.operand, left.operand));
        } else {
            instructions.add(MoveInstruction.getInstruction(left.operand, right.operand));
        }
    }

    @Override
    public void load(List<Instruction> instructions) {
        if (operand instanceof Address) {
            Address address = (Address)operand;
            operand = Environment.registerTable.addTemporaryRegister();
            instructions.add(LoadInstruction.getInstruction(operand, address));
        }
    }
}