package Compiler.Front.Ast.Expr.DualityExpr;

import Compiler.Back.MidCode.Intruction.ArithmeticInstruction.MemoryInstruction.MoveInstruction;
import Compiler.Back.MidCode.Intruction.ControlFlowInstruction.BranchInstruction;
import Compiler.Back.MidCode.Intruction.ControlFlowInstruction.JumpInstruction;
import Compiler.Back.MidCode.Intruction.Instruction;
import Compiler.Back.MidCode.Intruction.LabelIntruction.LabelInstruction;
import Compiler.Back.MidCode.Intruction.Operand.ImmediateValue;
import Compiler.Enviroment.Environment;
import Compiler.Front.Ast.Expr.Expr;
import Compiler.Front.Ast.Type.BasicType.BoolType;
import Compiler.Front.Ast.Type.Type;
import Compiler.Utility.AstTool.AstOutput;
import Compiler.Utility.Error.GrammarError;

import java.util.List;

public class DualityLogicalOrExpr extends DualityExpr {

    public DualityLogicalOrExpr(Type type, boolean isLeftVal, Expr left, Expr right) {
        super(type, isLeftVal);
        this.left = left;
        this.right = right;
        this.type = type;
        this.isLeftVal = isLeftVal;
    }
    public static DualityLogicalOrExpr getDualityLogicalOrExpr(Expr left, Expr right) {
        if((left.type instanceof BoolType) && (right.type instanceof BoolType)) {
            return new DualityLogicalOrExpr(left.type, false, left, right);
        }
        else {
            throw new GrammarError("Error in getDualityLogicalOrExpr - right.type & left.type wrong");
        }
    }
    @Override
    public String toString(int level) {
        StringBuilder string = new StringBuilder();
        string.append(AstOutput.outputIndent(level) + "[DualityLogicalOrExpr]\n");
        string.append(AstOutput.outputIndent(level + 1) + "[ExprLeft]\n");
        string.append(left.toString(level + 1));
        string.append(AstOutput.outputIndent(level + 1) + "[ExprRight]\n");
        string.append(right.toString(level + 1));
        return string.toString();
    }
    @Override
    public void emit(List<Instruction> instructions) {
        LabelInstruction trueLabel = LabelInstruction.getInstruction("logicalOr_true");
        LabelInstruction falseLabel = LabelInstruction.getInstruction("logicalOr_false");
        LabelInstruction endLabel = LabelInstruction.getInstruction("logicalOr_end");
        operand = Environment.registerTable.addTemporaryRegister();
        left.emit(instructions);
        left.load(instructions);
        instructions.add(BranchInstruction.getInstruction(left.operand, trueLabel, falseLabel));
        instructions.add(falseLabel);
        right.emit(instructions);
        right.load(instructions);
        operand = right.operand;
        instructions.add(JumpInstruction.getInstruction(endLabel));
        instructions.add(trueLabel);
        instructions.add(MoveInstruction.getInstruction(operand, new ImmediateValue(1)));
        instructions.add(JumpInstruction.getInstruction(endLabel));
        instructions.add(endLabel);
    }
}