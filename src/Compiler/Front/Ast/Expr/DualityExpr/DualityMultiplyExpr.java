package Compiler.Front.Ast.Expr.DualityExpr;

import Compiler.Back.MidCode.Intruction.ArithmeticInstruction.DualInstruction.MultiplyInstruction;
import Compiler.Back.MidCode.Intruction.Instruction;
import Compiler.Enviroment.Environment;
import Compiler.Front.Ast.Expr.Expr;
import Compiler.Front.Ast.Type.BasicType.IntegerType;
import Compiler.Front.Ast.Type.Type;
import Compiler.Utility.AstTool.AstOutput;
import Compiler.Utility.Error.GrammarError;

import java.util.List;

public class DualityMultiplyExpr extends DualityExpr {

    public DualityMultiplyExpr(Type type, boolean isLeftVal, Expr left, Expr right) {
        super(type, isLeftVal);
        this.left = left;
        this.right = right;
        this.type = type;
        this.isLeftVal = isLeftVal;
    }
    public static DualityMultiplyExpr getDualityMultiplyExpr(Expr left, Expr right) {
        if((left.type instanceof IntegerType) && (right.type instanceof IntegerType)) {
            return new DualityMultiplyExpr(left.type, false, left, right);
        }
        else {
            throw new GrammarError("Error in getDualityMultiplyExpr - right.type & left.type wrong");
        }
    }
    @Override
    public String toString(int level) {
        StringBuilder string = new StringBuilder();
        string.append(AstOutput.outputIndent(level) + "[DualityMultiplyExpr]\n");
        string.append(AstOutput.outputIndent(level + 1) + "[ExprLeft]\n");
        string.append(left.toString(level + 1));
        string.append(AstOutput.outputIndent(level + 1) + "[ExprRight]\n");
        string.append(right.toString(level + 1));
        return string.toString();
    }
    @Override
    public void emit(List<Instruction> instructions) {
        left.emit(instructions);
        left.load(instructions);
        right.emit(instructions);
        right.load(instructions);
        operand = Environment.registerTable.addTemporaryRegister(null);
        instructions.add(MultiplyInstruction.getInstruction(operand, left.operand, right.operand));
    }
}