package Compiler.Front.Ast.Expr.DualityExpr;

import Compiler.Back.MidCode.Intruction.ArithmeticInstruction.DualInstruction.MoreInstruction;
import Compiler.Back.MidCode.Intruction.Instruction;
import Compiler.Enviroment.Environment;
import Compiler.Front.Ast.Expr.ConstantExpr.BoolConstant;
import Compiler.Front.Ast.Expr.ConstantExpr.IntegerConstant;
import Compiler.Front.Ast.Expr.ConstantExpr.StringConstant;
import Compiler.Front.Ast.Expr.Expr;
import Compiler.Front.Ast.Expr.FunctionCallExpr;
import Compiler.Front.Ast.Function;
import Compiler.Front.Ast.Type.BasicType.BoolType;
import Compiler.Front.Ast.Type.BasicType.IntegerType;
import Compiler.Front.Ast.Type.BasicType.StringType;
import Compiler.Front.Ast.Type.Type;
import Compiler.Utility.AstTool.AstOutput;
import Compiler.Utility.Error.GrammarError;

import java.util.ArrayList;
import java.util.List;

public class DualityMoreExpr extends DualityExpr {

    public DualityMoreExpr(Type type, boolean isLeftVal, Expr left, Expr right) {
        super(type, isLeftVal);
        this.left = left;
        this.right = right;
        this.type = type;
        this.isLeftVal = isLeftVal;
    }
    public static Expr getDualityMoreExpr(Expr left, Expr right) {
        if((right.type instanceof IntegerType) && (left.type instanceof IntegerType)) {
            if (left instanceof IntegerConstant && right instanceof IntegerConstant) {
                int integer1 = ((IntegerConstant)left).integer;
                int integer2 = ((IntegerConstant)right).integer;
                return BoolConstant.getConstant(integer1 > integer2);
            }
            return new DualityMoreExpr(BoolType.getType(), false, left, right);
        }
        else if (left.type instanceof StringType && right.type instanceof StringType) {
            if (left instanceof StringConstant && right instanceof StringConstant) {
                String string1 = ((StringConstant)left).string;
                String string2 = ((StringConstant)right).string;
                return BoolConstant.getConstant(string1.compareTo(string2) > 0);
            }
            ArrayList<Expr> parameters = new ArrayList<>();
            parameters.add(left);
            parameters.add(right);
            return FunctionCallExpr.getFunctionCallExpr(

                    (Function)Environment.symbolTable.get("____builtin_string____greater_than").type,
                    parameters
            );
        }
        else {
            throw new GrammarError("Error in getDualityMoreExpr - right.type & left.type wrong");
        }
    }
    @Override
    public String toString(int level) {
        StringBuilder string = new StringBuilder();
        string.append(AstOutput.outputIndent(level) + "[DualityMoreExpr]\n");
        string.append(AstOutput.outputIndent(level + 1) + "[ExprLeft]\n");
        string.append(left.toString(level + 1));
        string.append(AstOutput.outputIndent(level + 1) + "[ExprRight]\n");
        string.append(right.toString(level + 1));
        return string.toString();
    }
    @Override
    public void emit(List<Instruction> instructions) {
        left.emit(instructions);
        left.load(instructions);
        right.emit(instructions);
        right.load(instructions);
        operand = Environment.registerTable.addTemporaryRegister(null);
        instructions.add(MoreInstruction.getInstruction(operand, left.operand, right.operand));
    }
}