package Compiler.Front.Ast.Expr.DualityExpr;

import Compiler.Back.MidCode.Intruction.ArithmeticInstruction.DualInstruction.EqualInstruction;
import Compiler.Back.MidCode.Intruction.Instruction;
import Compiler.Enviroment.Environment;
import Compiler.Front.Ast.Expr.ConstantExpr.BoolConstant;
import Compiler.Front.Ast.Expr.ConstantExpr.IntegerConstant;
import Compiler.Front.Ast.Expr.ConstantExpr.NullConstant;
import Compiler.Front.Ast.Expr.ConstantExpr.StringConstant;
import Compiler.Front.Ast.Expr.Expr;
import Compiler.Front.Ast.Expr.FunctionCallExpr;
import Compiler.Front.Ast.Function;
import Compiler.Front.Ast.Type.BasicType.BoolType;
import Compiler.Front.Ast.Type.BasicType.StringType;
import Compiler.Front.Ast.Type.Type;
import Compiler.Utility.AstTool.AstOutput;
import Compiler.Utility.Error.GrammarError;

import java.util.ArrayList;
import java.util.List;

public class DualityEqualExpr extends DualityExpr {

    public DualityEqualExpr(Type type, boolean isLeftVal, Expr left, Expr right) {
        super(type, isLeftVal);
        this.left = left;
        this.right = right;
        this.type = type;
        this.isLeftVal = isLeftVal;
    }
    public static Expr getDualityEqualExpr(Expr left, Expr right) {
        if(!left.type.compare(right.type)) {
            throw new GrammarError("Error in getDualityEqualExpr - right.type & left.type wrong");
        }
        if (left instanceof NullConstant && right instanceof NullConstant) {
            return BoolConstant.getConstant(true);
        } else if (left instanceof BoolConstant && right instanceof BoolConstant) {
            boolean bool1 = ((BoolConstant)left).bool;
            boolean bool2 = ((BoolConstant)right).bool;
            return BoolConstant.getConstant(bool1 == bool2);
        } else if (left instanceof IntegerConstant && right instanceof IntegerConstant) {
            int integer1 = ((IntegerConstant)left).integer;
            int integer2 = ((IntegerConstant)right).integer;
            return BoolConstant.getConstant(integer1 == integer2);
        } else if (left instanceof StringConstant && right instanceof StringConstant) {
            String string1 = ((StringConstant)left).string;
            String string2 = ((StringConstant)right).string;
            return BoolConstant.getConstant(string1.equals(string2));
        }
        if (left.type instanceof StringType && right.type instanceof StringType) {
            ArrayList<Expr>parameters = new ArrayList<>();
            parameters.add(left);
            parameters.add(right);
            return FunctionCallExpr.getFunctionCallExpr(
                    (Function)Environment.symbolTable.get("____builtin_string____equal_to").type,
                    parameters
            );
        }
        return new DualityEqualExpr(BoolType.getType(), false, left, right);
    }
    @Override
    public String toString(int level) {
        StringBuilder string = new StringBuilder();
        string.append(AstOutput.outputIndent(level) + "[DualityEqualExpr]\n");
        string.append(AstOutput.outputIndent(level + 1) + "[ExprLeft]\n");
        string.append(left.toString(level + 1));
        string.append(AstOutput.outputIndent(level + 1) + "[ExprRight]\n");
        string.append(right.toString(level + 1));
        return string.toString();
    }

    @Override
    public void emit(List<Instruction> instructions) {
        left.emit(instructions);
        left.load(instructions);
        right.emit(instructions);
        right.load(instructions);
        operand = Environment.registerTable.addTemporaryRegister(null);
        instructions.add(EqualInstruction.getInstruction(operand, left.operand, right.operand));
    }
}