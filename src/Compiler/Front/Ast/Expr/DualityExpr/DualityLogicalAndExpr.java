package Compiler.Front.Ast.Expr.DualityExpr;

import Compiler.Back.MidCode.Intruction.ArithmeticInstruction.MemoryInstruction.MoveInstruction;
import Compiler.Back.MidCode.Intruction.ControlFlowInstruction.BranchInstruction;
import Compiler.Back.MidCode.Intruction.ControlFlowInstruction.JumpInstruction;
import Compiler.Back.MidCode.Intruction.Instruction;
import Compiler.Back.MidCode.Intruction.LabelIntruction.LabelInstruction;
import Compiler.Back.MidCode.Intruction.Operand.ImmediateValue;
import Compiler.Enviroment.Environment;
import Compiler.Front.Ast.Expr.Expr;
import Compiler.Front.Ast.Type.BasicType.BoolType;
import Compiler.Front.Ast.Type.Type;
import Compiler.Utility.AstTool.AstOutput;
import Compiler.Utility.Error.GrammarError;

import java.util.List;

public class DualityLogicalAndExpr extends DualityExpr {

    public DualityLogicalAndExpr(Type type, boolean isLeftVal, Expr left, Expr right) {
        super(type, isLeftVal);
        this.left = left;
        this.right = right;
        this.type = type;
        this.isLeftVal = isLeftVal;
    }
    public static DualityLogicalAndExpr getDualityLogicalAndExpr(Expr left, Expr right) {
        if((left.type instanceof BoolType) && (right.type instanceof BoolType)) {
            return new DualityLogicalAndExpr(left.type, false, left, right);
        }
        else {
            throw new GrammarError("Error in getDualityLogicalAndExpr - right.type & left.type wrong");
        }
    }
    @Override
    public String toString(int level) {
        StringBuilder string = new StringBuilder();
        string.append(AstOutput.outputIndent(level) + "[DualityLogicalAndExpr]\n");
        string.append(AstOutput.outputIndent(level + 1) + "[ExprLeft]\n");
        string.append(left.toString(level + 1));
        string.append(AstOutput.outputIndent(level + 1) + "[ExprRight]\n");
        string.append(right.toString(level + 1));
        return string.toString();
    }
    @Override
    public void emit(List<Instruction> instructions) {
        LabelInstruction trueLabel = LabelInstruction.getInstruction("logicalAnd_true");
        LabelInstruction falseLabel = LabelInstruction.getInstruction("logicalAnd_false");
        LabelInstruction endLabel = LabelInstruction.getInstruction("logicalAnd_end");
        left.emit(instructions);
        left.load(instructions);
        instructions.add(BranchInstruction.getInstruction(left.operand, trueLabel, falseLabel));
        instructions.add(trueLabel);
        right.emit(instructions);
        right.load(instructions);
        operand = right.operand;
        instructions.add(JumpInstruction.getInstruction(endLabel));
        instructions.add(falseLabel);
        instructions.add(MoveInstruction.getInstruction(operand, new ImmediateValue(0)));
        instructions.add(JumpInstruction.getInstruction(endLabel));
        instructions.add(endLabel);
    }
}