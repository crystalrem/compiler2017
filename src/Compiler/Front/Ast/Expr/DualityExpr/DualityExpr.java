package Compiler.Front.Ast.Expr.DualityExpr;

import Compiler.Front.Ast.Expr.Expr;
import Compiler.Front.Ast.Type.Type;
import Compiler.Utility.AstTool.AstOutput;

public abstract class DualityExpr extends Expr {
    public Expr left, right;
    protected DualityExpr(Type type, boolean isLeftVal) {
        super(type, isLeftVal);
    }
    /*@Override
    public String toString(int level) {
        StringBuilder string = new StringBuilder();
        string.append(AstOutput.outputIndent(level) + "[DualityExpr]\n");
        string.append(AstOutput.outputIndent(level + 1) + "[ExprLeft]\n");
        string.append(left.toString(level + 1));
        string.append(AstOutput.outputIndent(level + 1) + "[ExprRight]\n");
        string.append(right.toString(level + 1));
        return string.toString();
    }*/
}