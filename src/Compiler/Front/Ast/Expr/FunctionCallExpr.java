package Compiler.Front.Ast.Expr;

import Compiler.Back.MidCode.Intruction.FunctionInstruction.CallInstruction;
import Compiler.Back.MidCode.Intruction.Instruction;
import Compiler.Back.MidCode.Intruction.Operand.Operand;
import Compiler.Enviroment.Environment;
import Compiler.Front.Ast.Expr.Expr;
import Compiler.Front.Ast.Expr.VariableExpression.ClassMemberExpr;
import Compiler.Front.Ast.Function;
import Compiler.Front.Ast.Type.BasicType.VoidType;
import Compiler.Front.Ast.Type.Type;
import Compiler.Utility.AstTool.AstOutput;
import Compiler.Utility.Error.GrammarError;
import Compiler.Utility.Error.InternalError;

import java.util.ArrayList;
import java.util.List;

public class FunctionCallExpr extends Expr {
    public Function function;
    public List<Expr> parameters;
    public FunctionCallExpr(Type type, boolean isLeftVal, Function function, ArrayList<Expr> parameters) {
        super(type, isLeftVal);
        this.function = function;
        this.parameters = parameters;
    }
    public static FunctionCallExpr getFunctionCallExpr(Function function, ArrayList<Expr> parameters) {
        return new FunctionCallExpr(function.type, false, function, parameters);
    }
    public static FunctionCallExpr getFunctionCallExpr(Expr expr, ArrayList<Expr> parameters) {
        if(expr.type instanceof Function) {
            Function function = (Function)expr.type;
            if(expr instanceof ClassMemberExpr) {
                parameters.add(0, ((ClassMemberExpr)expr).expr);
            }
            if(parameters.size() != function.parameters.size()) {
                //System.out.print("\n##" + expr.toString(0) + "\n--##");
                //System.out.println(parameters.size());
                //for (int i = 0; i < parameters.size(); i++)
                   // System.out.println("\n" + parameters.get(i).toString(0) + "\n");
                 throw new GrammarError("Error in getFunctionCallTExpr - parameters.size() != function.parameters.size()");
            }
            for (int i = 0; i < parameters.size(); i++) {
                if((expr instanceof ClassMemberExpr) && i == 0) continue;
                else {
                    if(!parameters.get(i).type.compare(function.parameters.get(i).type)) {
                        //System.out.print("\n##" + expr.toString(0) + "\n--##");
                        //for (int j = 0; j < parameters.size(); j++)
                            //System.out.println("\n" + parameters.get(j).toString(0) + "\n");
                        throw new  GrammarError("Error in getFunctionCallTExpr - !parameters.get(i).type.compare(function.parameters.get(i).type)");
                    }
                }
            }
            return new FunctionCallExpr(function.type, false, function, parameters);
        }
        else {
            throw new GrammarError("Error in getFunctionCallTExpr - !expr.type instanceof Function");
        }
    }

    @Override
    public String toString(int level) {
        StringBuilder string = new StringBuilder();
        string.append(AstOutput.outputIndent(level) + "[FunctionCallExpr]\n");
        string.append(AstOutput.outputIndent(level + 1) + "[functionName]" + function.name + "\n");
        string.append(AstOutput.outputIndent(level + 1) + "[parameters]" + "\n");
        for(int i = 0 ; i < parameters.size(); i++) {
            parameters.get(i).toString(level + 1);
        }
        string.append("\n");
        return string.toString();
    }

    public void emit(List<Instruction> instructions) {
        if(!check(instructions)) {
            List<Operand> operands = new ArrayList<>();
            for (Expr parameter : parameters) {
                parameter.emit(instructions);
                parameter.load(instructions);
                operands.add(parameter.operand);
            }
            if (type instanceof VoidType) {
                instructions.add(CallInstruction.getInstruction(null, function, operands));
            } else {
                operand = Environment.registerTable.addTemporaryRegister();
                instructions.add(CallInstruction.getInstruction(operand, function, operands));
            }
        }
    }
    public Boolean check(List<Instruction> instructions) {
        if (function.name.startsWith("____builtin____print")) {
            if (parameters.size() != 1) {
                throw new InternalError();
            }
            Expr parameter = parameters.get(0);
            if (parameter instanceof FunctionCallExpr) {
                FunctionCallExpr call = (FunctionCallExpr)parameter;
                List<Expr> expressions = new ArrayList<>();
                if (call.function.name.equals("____builtin_string____concatenate")) {
                    Expr current = call;
                    while (current instanceof FunctionCallExpr) {
                        FunctionCallExpr now = (FunctionCallExpr)current;
                        if (!now.function.name.equals("____builtin_string____concatenate")) {
                            break;
                        }
                        current = now.parameters.get(0);
                        expressions.add(0, now.parameters.get(1));
                    }
                    expressions.add(0, current);
                } else {
                    expressions.add(call);
                }
                int size = expressions.size() - (function.name.endsWith("ln") ? 1 : 0);
                for (int i = 0; i < size; ++i) {
                    if (expressions.get(i) instanceof FunctionCallExpr) {
                        FunctionCallExpr sub = (FunctionCallExpr)expressions.get(i);
                    }
                    emit(instructions, "print", expressions.get(i));
                }
                for (int i = size; i < expressions.size(); ++i) {
                    emit(instructions, "println", expressions.get(i));
                }
                return true;
            }
        }
        return false;
    }

    public void emit(List<Instruction> instructions, String name, Expr parameter) {
        Function function = (Function)Environment.symbolTable.get(name).type;
        parameter.emit(instructions);
        parameter.load(instructions);
        List<Operand> operands = new ArrayList<>();
        operands.add(parameter.operand);
        instructions.add(CallInstruction.getInstruction(null, function, operands));
    }
}