package Compiler.Front.Ast.Expr.ConstantExpr;

import Compiler.Back.MidCode.Intruction.Instruction;
import Compiler.Back.MidCode.Intruction.Operand.ImmediateValue;
import Compiler.Front.Ast.Type.BasicType.BoolType;
import Compiler.Utility.AstTool.AstOutput;

import java.util.List;


public class BoolConstant extends Constant {

    public Boolean bool;
    public BoolConstant(Boolean bool) {
        super(BoolType.getType());
        this.bool = bool;
    }
    public static Constant getConstant(boolean bool) {
        return new BoolConstant(bool);
    }

    @Override
    public String toString(int level) {
        return  AstOutput.outputIndent(level) + "BoolConstant =  "+ Boolean.toString(bool) + " "+ type.toString(0) + "\n";
    }

    @Override
    public void emit(List<Instruction> instructions) {
        operand = new ImmediateValue(this.bool ? 1 : 0);
    }


}