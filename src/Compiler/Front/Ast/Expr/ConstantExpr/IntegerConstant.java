package Compiler.Front.Ast.Expr.ConstantExpr;

import Compiler.Back.MidCode.Intruction.Instruction;
import Compiler.Back.MidCode.Intruction.Operand.ImmediateValue;
import Compiler.Front.Ast.Type.BasicType.IntegerType;
import Compiler.Front.Ast.Type.Type;
import Compiler.Utility.AstTool.AstOutput;

import java.util.List;


public class IntegerConstant extends Constant {

    public int integer;
    public IntegerConstant(int integer) {
        super(IntegerType.getType());
        this.integer = integer;
    }
    public Constant getConstant(int integer) {
        return new IntegerConstant(integer);
    }

    @Override
    public String toString(int level) {
        return AstOutput.outputIndent(level) + "IntegerConstant =  "+ Integer.toString(integer) +"\n";
    }

    @Override
    public void emit(List<Instruction> instructions) {
        operand = new ImmediateValue(integer);
    }
}