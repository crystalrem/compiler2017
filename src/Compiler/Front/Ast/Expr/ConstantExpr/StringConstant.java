package Compiler.Front.Ast.Expr.ConstantExpr;

import Compiler.Back.MidCode.Intruction.Instruction;
import Compiler.Enviroment.Environment;
import Compiler.Front.Ast.Type.BasicType.StringType;
import Compiler.Utility.AstTool.AstOutput;

import java.util.List;


public class StringConstant extends Constant {

    public String string;
    public StringConstant(String string) {
        super(StringType.getType());
        this.string = string;
        //System.err.println("**************" + string);
    }
    public static Constant getConstant(String string) {
        return new StringConstant(string);
    }
    @Override
    public String toString(int level) {
        return AstOutput.outputIndent(level) + "StringConstant = " + string ;
    }

    @Override
    public void emit(List<Instruction> instructions) {
        operand = Environment.registerTable.addStringRegister(string);
    }


}