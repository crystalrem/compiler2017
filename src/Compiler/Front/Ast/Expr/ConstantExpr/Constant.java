package Compiler.Front.Ast.Expr.ConstantExpr;

import Compiler.Front.Ast.Expr.Expr;
import Compiler.Front.Ast.Type.Type;

public abstract class Constant extends Expr {
    protected Constant(Type type) {
        super(type, false);
    }
    @Override
    public abstract String toString(int level);

}