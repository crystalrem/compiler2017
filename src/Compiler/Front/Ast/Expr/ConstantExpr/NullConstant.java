package Compiler.Front.Ast.Expr.ConstantExpr;

import Compiler.Back.MidCode.Intruction.Instruction;
import Compiler.Back.MidCode.Intruction.Operand.ImmediateValue;
import Compiler.Front.Ast.Type.BasicType.NullType;
import Compiler.Front.Ast.Type.Type;
import Compiler.Utility.AstTool.AstOutput;

import java.util.List;


public class NullConstant extends Constant {

    public NullConstant() {
        super(NullType.getType());
    }
    public Constant getConstant() {
        return new NullConstant();
    }

    @Override
    public String toString(int level) {
        return AstOutput.outputIndent(level) + "NullString = null \n";
    }

    @Override
    public void emit(List<Instruction> instructions) {
        operand = new ImmediateValue(0);
    }
}