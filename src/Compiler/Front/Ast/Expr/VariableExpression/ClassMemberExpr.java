package Compiler.Front.Ast.Expr.VariableExpression;

import Compiler.Back.MidCode.Intruction.ArithmeticInstruction.MemoryInstruction.LoadInstruction;
import Compiler.Back.MidCode.Intruction.Instruction;
import Compiler.Back.MidCode.Intruction.Operand.ImmediateValue;
import Compiler.Back.MidCode.Intruction.Operand.VirtualRegister.Address;
import Compiler.Back.MidCode.Intruction.Operand.VirtualRegister.VirtualRegister;
import Compiler.Enviroment.Environment;
import Compiler.Front.Ast.Expr.Expr;
import Compiler.Front.Ast.Function;
import Compiler.Front.Ast.Type.ArrayType;
import Compiler.Front.Ast.Type.BasicType.IntegerType;
import Compiler.Front.Ast.Type.BasicType.StringType;
import Compiler.Front.Ast.Type.Class.Class;
import Compiler.Front.Ast.Type.Class.ClassMember.Member;
import Compiler.Front.Ast.Type.Class.ClassMember.MemberFunction;
import Compiler.Front.Ast.Type.Class.ClassMember.MemberVariable;
import Compiler.Front.Ast.Type.Type;
import Compiler.Utility.AstTool.AstOutput;
import Compiler.Utility.Error.GrammarError;
import sun.reflect.generics.tree.ClassTypeSignature;

import java.util.List;

public class ClassMemberExpr extends Expr {
    public Expr expr;
    public String memberName;
    private ClassMemberExpr(Type type, boolean isLeftValue, Expr expr, String memberName) {
        super(type, isLeftValue);
        this.expr = expr;
        this.memberName = memberName;
    }
    public static ClassMemberExpr getClassMemberExpr(Expr expr, String memberName) {
        if(expr.type instanceof Class) {
            Member member = ((Class) expr.type).findMember(memberName);
            //System.out.println("\n::getClassMemberExpr : member = " + member);
            if(member instanceof MemberFunction) {
                return new ClassMemberExpr(((MemberFunction)member).memberFunction, expr.isLeftVal, expr, memberName);
            }
            else if(member instanceof MemberVariable) {
                return new ClassMemberExpr(((MemberVariable)member).symbol.type, expr.isLeftVal, expr, memberName);
            }
            else {
                throw new GrammarError("Error in getClassMemberExpr - No Member in expr' Class : " + memberName);
            }
        }
        else if(expr.type instanceof StringType) {
            if(memberName.equals("length")) {
                return new ClassMemberExpr(Environment.symbolTable.get("stringLength").type, expr.isLeftVal, expr, memberName);
            }

            else if(memberName.equals("parseInt")) {
                return new ClassMemberExpr(Environment.symbolTable.get("stringParseInt").type, expr.isLeftVal, expr, memberName);
            }

            else if(memberName.equals("ord")) {
                return new ClassMemberExpr(Environment.symbolTable.get("stringOrd").type, expr.isLeftVal, expr, memberName);
            }
            else if(memberName.equals("substring")) {
                return new ClassMemberExpr(Environment.symbolTable.get("stringSubString").type, expr.isLeftVal, expr, memberName);
            }
            else {
                throw new GrammarError("Error in getClassMemberExpr - expr.type instanceof StringType");
            }
        }
        else if(expr.type instanceof ArrayType) {
            if (memberName.equals("size")) {
                return new ClassMemberExpr(Environment.symbolTable.get("arraySize").type, expr.isLeftVal, expr, memberName);
            }
            else {
                throw new GrammarError("Error in getClassMemberExpr - expr.type instanceof ArrayType");
            }
        }
        else {
            //System.out.print("\n###" + expr.toString(0) + "\n" + memberName);
            throw new GrammarError("Error in getClassMemberExpr - expr.type is other Type");
        }
    }

    @Override
    public String toString(int level) {
        StringBuilder string = new StringBuilder();
        string.append(AstOutput.outputIndent(level) + "[ClassMemberExpr]\n");
        string.append(AstOutput.outputIndent(level + 1) + "[Type]\n");
        if(type instanceof Class) {
            string.append(AstOutput.outputIndent(level + 1) + "[Class] " + ((Class)type).name);
        }
        else string.append(type.toString(level + 1));
        string.append(AstOutput.outputIndent(level + 1) + "[memberName] " + memberName + "\n");
        string.append(AstOutput.outputIndent(level + 1) + "[expr]\n");
        string.append(expr.toString(level + 1));
        string.append(AstOutput.outputIndent(level + 1) + "[isLeftVal] " + Boolean.toString(isLeftVal));
        string.append("\n");
        return string.toString();
    }

    @Override
    public void emit(List<Instruction> instructions) {
        Class currentClass = (Class)expr.type;
        Member member = currentClass.findMember(memberName);
        if(member instanceof MemberVariable) {
            MemberVariable memberVariable = (MemberVariable)member;
            expr.emit(instructions);
            expr.load(instructions);
            VirtualRegister address = (VirtualRegister)expr.operand;
            ImmediateValue offset = new ImmediateValue(memberVariable.offset);
            operand = new Address(address, offset, memberVariable.originClass.size());
        }
    }

    @Override
    public void load(List<Instruction> instructions) {
        if (operand instanceof Address) {
            Address address = (Address)operand;
            operand = Environment.registerTable.addTemporaryRegister();
            instructions.add(LoadInstruction.getInstruction(operand, address));
        }
    }
}