package Compiler.Front.Ast.Expr.VariableExpression;

import Compiler.Back.MidCode.Intruction.Instruction;
import Compiler.Enviroment.Environment;
import Compiler.Enviroment.SymbolTable.Symbol;
import Compiler.Front.Ast.Expr.Expr;
import Compiler.Front.Ast.Function;
import Compiler.Front.Ast.Type.Class.ClassMember.Member;
import Compiler.Front.Ast.Type.Type;
import Compiler.Front.Ast.Type.Class.Class;
import Compiler.Utility.AstTool.AstOutput;
import Compiler.Utility.Error.GrammarError;

import java.util.List;

public class IdentifierExpr extends Expr {

    Symbol symbol;

    protected IdentifierExpr(Type type, boolean isLeftVal, Symbol symbol) {
        super(type, isLeftVal);
        this.type = type;
        this.isLeftVal = isLeftVal;
        this.symbol = symbol;
    }

    public static Expr getIdentifierExpr(String name) {

        Symbol symbol = Environment.symbolTable.get(name);
        /*if(Environment.scopeTable.getScope() != null)
            System.out.println("\n::getIdentifierExpr - name: " + name + "Scope : " + Environment.scopeTable.getScope().toString());
        if(symbol == null) {
            System.out.println("\n**::getIdentifierExpr - name: " + name + "Scope : " + Environment.scopeTable.getScope().toString());
        }
        if(symbol.scope == null) {
            System.out.println("\n*::getIdentifierExpr - name: " + name + "Scope : " + Environment.scopeTable.getScope().toString());
        }*/
        if(symbol.scope instanceof Class) {
            //Member member = ((Class) type).findMember(name);
            return ClassMemberExpr.getClassMemberExpr(IdentifierExpr.getIdentifierExpr("this"), name);
        }
        else {
            if(symbol.type instanceof Function) {
                return new IdentifierExpr(symbol.type, false, symbol);
            }
            else {
                return new IdentifierExpr(symbol.type, true, symbol);
            }
        }
    }
    @Override
    public String toString(int level) {
        StringBuilder s = new StringBuilder();
        s.append(AstOutput.outputIndent(level) + "[IdentifierExpression]\n");
        s.append(AstOutput.outputIndent(level + 1) + "[Symbol]\n");
        s.append(symbol.toString(level + 1));
        s.append(AstOutput.outputIndent(level + 1) + "[Type]\n");
        s.append(type.toString(level + 1));
        s.append(AstOutput.outputIndent(level + 1) + "[isLeftVal]\n");
        s.append(AstOutput.outputIndent(level + 1) + String.valueOf(isLeftVal));
        s.append("\n");
        return s.toString();
    }

    @Override
    public void emit(List<Instruction> instructions) {
        operand = symbol.register;
    }

}