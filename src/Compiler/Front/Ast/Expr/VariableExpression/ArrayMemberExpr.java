package Compiler.Front.Ast.Expr.VariableExpression;

import Compiler.Back.MidCode.Intruction.ArithmeticInstruction.DualInstruction.AdditionInstruction;
import Compiler.Back.MidCode.Intruction.ArithmeticInstruction.DualInstruction.MultiplyInstruction;
import Compiler.Back.MidCode.Intruction.ArithmeticInstruction.MemoryInstruction.LoadInstruction;
import Compiler.Back.MidCode.Intruction.Instruction;
import Compiler.Back.MidCode.Intruction.Operand.ImmediateValue;
import Compiler.Back.MidCode.Intruction.Operand.VirtualRegister.Address;
import Compiler.Back.MidCode.Intruction.Operand.VirtualRegister.VirtualRegister;
import Compiler.Enviroment.Environment;
import Compiler.Front.Ast.Expr.Expr;
import Compiler.Front.Ast.Type.ArrayType;
import Compiler.Front.Ast.Type.BasicType.IntegerType;
import Compiler.Front.Ast.Type.Class.Class;
import Compiler.Front.Ast.Type.Type;
import Compiler.Utility.AstTool.AstOutput;
import Compiler.Utility.Error.GrammarError;

import java.util.List;

public class ArrayMemberExpr extends Expr {
    Expr array, index;
    public ArrayMemberExpr(Type type, boolean isLeftVal, Expr array, Expr index) {
        super(type, isLeftVal);
        this.type = type;
        this.isLeftVal = isLeftVal;
        this.array = array;
        this.index = index;
    }

    public static ArrayMemberExpr getArrayMemberExpr(Expr array, Expr index) {
        if(!(array.type instanceof ArrayType)) {
            //System.out.println("\n##ArrayMemberExpr:" + array.toString(0));
            //System.out.println("\n##ArrayMemberExpr:" + array.type.toString(0));
            throw new GrammarError("Error in getArrayMemberExpr - array should be ArrayType");
        }
        else if(!(index.type instanceof IntegerType)) {
            throw new GrammarError("Error in getArrayMemberExpr - index should be IntegerType");
        }
        else {
            return new ArrayMemberExpr(((ArrayType)array.type).superStratum(), array.isLeftVal, array, index);
        }
    }

    @Override
    public String toString(int level) {
        StringBuilder string = new StringBuilder();
        string.append(AstOutput.outputIndent(level) + "[ArrayMemberExpr]\n" );
        string.append(AstOutput.outputIndent(level + 1) + "[Type] \n");
        if(type instanceof Class) {
            string.append(AstOutput.outputIndent(level + 1) + "[Class] " + ((Class)type).name);
        }
        else string.append(type.toString(level + 1));
        string.append(AstOutput.outputIndent(level + 1) + "[array] \n");
        string.append(array.toString(level + 1));
        string.append(AstOutput.outputIndent(level + 1) + "[index] \n");
        string.append(index.toString(level + 1));
        string.append(AstOutput.outputIndent(level + 1) + "[isLeftVal] " + Boolean.toString(isLeftVal));
        string.append("\n");
        return string.toString();
    }

    @Override
    public void emit(List<Instruction> instructions) {
        array.emit(instructions);
        array.load(instructions);
        index.emit(instructions);
        index.load(instructions);
        VirtualRegister tmp = Environment.registerTable.addTemporaryRegister();
        instructions.add(MultiplyInstruction.getInstruction(tmp, index.operand, new ImmediateValue(type.size())));
        VirtualRegister offset = Environment.registerTable.addTemporaryRegister();
        instructions.add(AdditionInstruction.getInstruction(offset, tmp, array.operand));
        operand = new Address(offset, new ImmediateValue(0), type.size());
    }

    @Override
    public void load(List<Instruction> instructions) {
        if (operand instanceof Address) {
            Address address = (Address)operand;
            operand = Environment.registerTable.addTemporaryRegister();
            instructions.add(LoadInstruction.getInstruction(operand, address));
        }
    }

}