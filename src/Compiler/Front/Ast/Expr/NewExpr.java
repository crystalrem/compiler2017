package Compiler.Front.Ast.Expr;

import Compiler.Back.MidCode.Intruction.ArithmeticInstruction.DualInstruction.AdditionInstruction;
import Compiler.Back.MidCode.Intruction.ArithmeticInstruction.DualInstruction.LessInstruction;
import Compiler.Back.MidCode.Intruction.ArithmeticInstruction.DualInstruction.MultiplyInstruction;
import Compiler.Back.MidCode.Intruction.ArithmeticInstruction.MemoryInstruction.AllocateInstruction;
import Compiler.Back.MidCode.Intruction.ArithmeticInstruction.MemoryInstruction.MoveInstruction;
import Compiler.Back.MidCode.Intruction.ArithmeticInstruction.MemoryInstruction.StoreInstruction;
import Compiler.Back.MidCode.Intruction.ControlFlowInstruction.BranchInstruction;
import Compiler.Back.MidCode.Intruction.ControlFlowInstruction.JumpInstruction;
import Compiler.Back.MidCode.Intruction.FunctionInstruction.CallInstruction;
import Compiler.Back.MidCode.Intruction.Instruction;
import Compiler.Back.MidCode.Intruction.LabelIntruction.LabelInstruction;
import Compiler.Back.MidCode.Intruction.Operand.ImmediateValue;
import Compiler.Back.MidCode.Intruction.Operand.Operand;
import Compiler.Back.MidCode.Intruction.Operand.VirtualRegister.Address;
import Compiler.Back.MidCode.Intruction.Operand.VirtualRegister.VirtualRegister;
import Compiler.Enviroment.Environment;
import Compiler.Front.Ast.Type.ArrayType;
import Compiler.Front.Ast.Type.BasicType.IntegerType;
import Compiler.Front.Ast.Type.BasicType.VoidType;
import Compiler.Front.Ast.Type.Class.Class;
import Compiler.Front.Ast.Type.Class.ClassMember.Member;
import Compiler.Front.Ast.Type.Class.ClassMember.MemberVariable;
import Compiler.Front.Ast.Type.Type;
import Compiler.Utility.AstTool.AstOutput;
import Compiler.Utility.Error.GrammarError;
import Compiler.Utility.Error.InternalError;

import javax.naming.OperationNotSupportedException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class NewExpr extends Expr {
    public List<Expr> exprList;
    public NewExpr(Type type, boolean isLeftVal, List<Expr>exprList) {
        super(type, isLeftVal);
        this.exprList = exprList;
    }
    public static NewExpr getNewExpr(Type type, List<Expr> exprList) {
        if (exprList.isEmpty()) {
            if (type instanceof Class) {
                return new NewExpr(type, false, exprList);
            }
            throw new GrammarError("Error in getNewExpr - Class should be new when d = 0");
        }
        else{
            Type arrayType = ArrayType.getType(type, exprList.size());
            return new NewExpr(arrayType, false, exprList);
            //return new NewExpr(arrayType, false, exprList);
        }
    }

    @Override
    public String toString(int level) {
        //return super.toString();
        StringBuilder string = new StringBuilder();
        string.append(AstOutput.outputIndent(level) + "NewExpr:\n");
        string.append(AstOutput.outputIndent(level + 1) + "Type: \n");
        if(this.type instanceof  Class) {
            string.append(AstOutput.outputIndent(level + 1) + "Class " + ((Class)type).name);
        }
        else string.append(type.toString(level + 1));
        string.append(AstOutput.outputIndent(level + 1) + "exprList: \n");
        for(int i = 0; i < exprList.size(); i++) {
            if(exprList.get(i) != null)
            string.append(exprList.get(i).toString(level + 1));
        }
        return string.toString();
    }

    @Override
    public void emit(List<Instruction> instructions) {
        for(int i = 0; i < exprList.size(); i++){
                Expr expr = exprList.get(i);
                if(expr != null) {
                    expr.emit(instructions);
                    expr.load(instructions);
                }
            }
        operand = Environment.registerTable.addTemporaryRegister();

        if (type instanceof Class) {
            Class currentClass = (Class)type;
            instructions.add(AllocateInstruction.getInstruction(operand, new ImmediateValue(currentClass.classSize)));
            //	handle the class members' initial values
            for (Map.Entry<String, MemberVariable> entry : currentClass.memberVariableList.entrySet()) {
                String name = entry.getKey();
                MemberVariable memberVariable = entry.getValue();
                Address address = new Address((VirtualRegister)operand, new ImmediateValue(memberVariable.offset), memberVariable.originClass.size());
                if(memberVariable.expr != null) {
                    memberVariable.expr.emit(instructions);
                    memberVariable.expr.load(instructions);
                    instructions.add(StoreInstruction.getInstruction(memberVariable.expr.operand, (Operand)address));
                }
            }
            if(currentClass.constructor != null) {
                List<Operand>  operands = new ArrayList<Operand>();
                operands.add(this.operand);
                instructions.add(CallInstruction.getInstruction(null, currentClass.constructor, operands));
            }

        }
        else if (type instanceof ArrayType) {
            ArrayType arrayType = (ArrayType)type;
            VirtualRegister size = Environment.registerTable.addTemporaryRegister();
            instructions.add(MultiplyInstruction.getInstruction(size, exprList.get(0).operand, new ImmediateValue(arrayType.superStratum().size())));
            instructions.add(AdditionInstruction.getInstruction(size, size, new ImmediateValue(new IntegerType().size())));
            instructions.add(AllocateInstruction.getInstruction(operand, size));
            instructions.add(StoreInstruction.getInstruction(exprList.get(0).operand, new Address((VirtualRegister)operand, new ImmediateValue(0), new IntegerType().size())));
            instructions.add(AdditionInstruction.getInstruction(operand, operand, new ImmediateValue(new IntegerType().size())));
            if ((exprList.size() > 1 && exprList.get(1) != null) || (((ArrayType) type).dimension == 1 && (((ArrayType) type).basicType instanceof Class))) {
                LabelInstruction condition = LabelInstruction.getInstruction("new_condition");
                LabelInstruction body = LabelInstruction.getInstruction("new_body");
                LabelInstruction loop = LabelInstruction.getInstruction("new_loop");
                LabelInstruction exit = LabelInstruction.getInstruction("new_exit");

                Type superStratumType = ((ArrayType) type).superStratum();
                List<Expr> superExprList;
                if (exprList.size() <= 1) {
                    superExprList = new ArrayList<>();
                } else {
                    superExprList = exprList.subList(1, exprList.size());
                }
                NewExpr SuperNewExpr = NewExpr.getNewExpr(superStratumType, superExprList);

                VirtualRegister tmp0 = Environment.registerTable.addTemporaryRegister();
                instructions.add(MoveInstruction.getInstruction(tmp0, new ImmediateValue(0)));
                instructions.add(JumpInstruction.getInstruction(condition));

                instructions.add(condition);
                VirtualRegister tmp1 = Environment.registerTable.addTemporaryRegister();
                instructions.add(LessInstruction.getInstruction(tmp1, tmp0, exprList.get(0).operand));
                instructions.add(BranchInstruction.getInstruction(tmp1, body, exit));

                instructions.add(body);
                SuperNewExpr.emit(instructions);
                VirtualRegister tmp2 = Environment.registerTable.addTemporaryRegister();
                instructions.add(MultiplyInstruction.getInstruction(tmp2, tmp0, new ImmediateValue(((ArrayType) type).basicType.size())));
                VirtualRegister tmp3 = Environment.registerTable.addTemporaryRegister();
                instructions.add(AdditionInstruction.getInstruction(tmp3, operand, tmp2));
                Address cur = new Address(tmp3, new ImmediateValue(0), ((ArrayType) type).basicType.size());
                instructions.add(StoreInstruction.getInstruction(SuperNewExpr.operand, cur));
                instructions.add(JumpInstruction.getInstruction(loop));

                instructions.add(loop);
                instructions.add(AdditionInstruction.getInstruction(tmp0, tmp0, new ImmediateValue(1)));
                instructions.add(JumpInstruction.getInstruction(condition));
                instructions.add(JumpInstruction.getInstruction(exit));

                instructions.add(exit);
            }
        }
        else {
            throw new InternalError();
        }
    }
}