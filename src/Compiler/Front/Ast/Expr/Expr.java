package Compiler.Front.Ast.Expr;

import Compiler.Back.MidCode.Intruction.Instruction;
import Compiler.Back.MidCode.Intruction.Operand.ImmediateValue;
import Compiler.Back.MidCode.Intruction.Operand.Operand;
import Compiler.Front.Ast.Node;
import Compiler.Front.Ast.Type.Type;

import java.util.List;

public abstract class Expr implements Node {
    public boolean isLeftVal;
    public Type type;
    public Operand operand;
    protected Expr(Type type, boolean isLeftVal) {
        this.type = type;
        this.isLeftVal = isLeftVal;
    }


    public String toString(int level) {
        return null;
    }

    public abstract void emit(List<Instruction> instructions);

    public void load(List<Instruction> instructions) {
    }
}