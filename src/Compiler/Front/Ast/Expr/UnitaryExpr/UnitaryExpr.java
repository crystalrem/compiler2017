package Compiler.Front.Ast.Expr.UnitaryExpr;

import Compiler.Front.Ast.Expr.Expr;
import Compiler.Front.Ast.Type.Type;

public abstract class UnitaryExpr extends Expr {
    Expr expr;
    public UnitaryExpr(Type type, boolean isLeftVal, Expr expr)
    {
        super(type, isLeftVal);
        this.type = type;
        this.isLeftVal = isLeftVal;
        this.expr = expr;
    }
}