package Compiler.Front.Ast.Expr.UnitaryExpr;

import Compiler.Back.MidCode.Intruction.ArithmeticInstruction.UnitaryInsurction.MinusInstruction;
import Compiler.Back.MidCode.Intruction.Instruction;
import Compiler.Enviroment.Environment;
import Compiler.Front.Ast.Expr.Expr;
import Compiler.Front.Ast.Type.BasicType.IntegerType;
import Compiler.Front.Ast.Type.Type;
import Compiler.Utility.AstTool.AstOutput;
import Compiler.Utility.Error.GrammarError;

import java.util.List;

public class MinusExpr extends UnitaryExpr {

    public MinusExpr(Type type, boolean isLeftVal, Expr expr) {
        super(type, isLeftVal, expr);
    }

    public static MinusExpr getMinusExpr(Expr expr) {
        if(expr.type instanceof IntegerType) {
            return new MinusExpr(IntegerType.getType(), false, expr);
        }
        else {
            throw new GrammarError("Error in getPlusExpr - !type instanceof IntegerType");
        }
    }
    @Override
    public String toString(int level) {
        StringBuilder string = new StringBuilder();
        string.append(AstOutput.outputIndent(level) + "[MinusExprr]\n");
        string.append(AstOutput.outputIndent(level + 1) + "type : " + type.toString(0));
        string.append(AstOutput.outputIndent(level + 1) + "isLeftVal : " + isLeftVal);
        string.append(AstOutput.outputIndent(level + 1) + "expr :\n");
        string.append(expr.toString(level + 1));
        return string.toString();
    }

    @Override
    public void emit(List<Instruction> instructions) {
        expr.emit(instructions);
        expr.load(instructions);
        operand = Environment.registerTable.addTemporaryRegister();
        instructions.add(MinusInstruction.getInstruction(operand, expr.operand));
    }
}