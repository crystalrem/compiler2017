package Compiler.Front.Ast.Expr.UnitaryExpr;

import Compiler.Back.MidCode.Intruction.Instruction;
import Compiler.Front.Ast.Expr.Expr;
import Compiler.Front.Ast.Type.BasicType.IntegerType;
import Compiler.Front.Ast.Type.Type;
import Compiler.Utility.AstTool.AstOutput;
import Compiler.Utility.Error.GrammarError;

import java.util.List;

public class PlusExpr extends UnitaryExpr {

    public PlusExpr(Type type, boolean isLeftVal, Expr expr) {
        super(type, isLeftVal, expr);
    }

    public static PlusExpr getPlusExpr(Expr expr) {
        if(expr.type instanceof IntegerType) {
            return new PlusExpr(IntegerType.getType(), false, expr);
        }
        else {
            throw new GrammarError("Error in getPlusExpr - !type instanceof IntegerType");
        }
    }
    @Override
    public String toString(int level) {
        StringBuilder string = new StringBuilder();
        string.append(AstOutput.outputIndent(level) + "[PlusExpr]\n");
        string.append(AstOutput.outputIndent(level + 1) + "type : " + type.toString(0));
        string.append(AstOutput.outputIndent(level + 1) + "isLeftVal : " + isLeftVal);
        string.append(AstOutput.outputIndent(level + 1) + "expr :\n");
        string.append(expr.toString(level + 1));
        return string.toString();
    }

    @Override
    public void emit(List<Instruction> instructions) {
        operand = expr.operand;
    }
}