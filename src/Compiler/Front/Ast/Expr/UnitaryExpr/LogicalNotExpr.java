package Compiler.Front.Ast.Expr.UnitaryExpr;

import Compiler.Back.MidCode.Intruction.ArithmeticInstruction.DualInstruction.XorInstruction;
import Compiler.Back.MidCode.Intruction.Instruction;
import Compiler.Back.MidCode.Intruction.Operand.ImmediateValue;
import Compiler.Enviroment.Environment;
import Compiler.Front.Ast.Expr.Expr;
import Compiler.Front.Ast.Type.BasicType.BoolType;
import Compiler.Front.Ast.Type.Type;
import Compiler.Utility.AstTool.AstOutput;
import Compiler.Utility.Error.GrammarError;

import java.util.List;

public class LogicalNotExpr extends UnitaryExpr {

    public LogicalNotExpr(Type type, boolean isLeftVal, Expr expr) {
        super(type, isLeftVal, expr);
    }

    public static LogicalNotExpr getLogicalNotExpr(Expr expr) {
        if(expr.type instanceof BoolType) {
            return new LogicalNotExpr(expr.type, false, expr);
        }
        else {
            throw new GrammarError("Error in getLogicalNotExpr - !type instanceof BoolType");
        }
    }

    @Override
    public String toString(int level) {
        StringBuilder string = new StringBuilder();
        string.append(AstOutput.outputIndent(level) + "[LogicalNotExpr]\n");
        string.append(AstOutput.outputIndent(level + 1) + "type : " + type.toString(0));
        string.append(AstOutput.outputIndent(level + 1) + "isLeftVal : " + isLeftVal);
        string.append(AstOutput.outputIndent(level + 1) + "expr :\n");
        string.append(expr.toString(level + 1));
        return string.toString();
    }

    @Override
    public void emit(List<Instruction> instructions) {
        expr.emit(instructions);
        expr.load(instructions);
        operand = Environment.registerTable.addTemporaryRegister();
        instructions.add(XorInstruction.getInstruction(operand, expr.operand, new ImmediateValue(1)));
    }

}