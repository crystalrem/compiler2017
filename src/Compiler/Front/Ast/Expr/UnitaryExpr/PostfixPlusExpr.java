package Compiler.Front.Ast.Expr.UnitaryExpr;

import Compiler.Back.MidCode.Intruction.ArithmeticInstruction.DualInstruction.AdditionInstruction;
import Compiler.Back.MidCode.Intruction.ArithmeticInstruction.MemoryInstruction.MoveInstruction;
import Compiler.Back.MidCode.Intruction.ArithmeticInstruction.MemoryInstruction.StoreInstruction;
import Compiler.Back.MidCode.Intruction.Instruction;
import Compiler.Back.MidCode.Intruction.Operand.ImmediateValue;
import Compiler.Back.MidCode.Intruction.Operand.VirtualRegister.Address;
import Compiler.Enviroment.Environment;
import Compiler.Front.Ast.Expr.Expr;
import Compiler.Front.Ast.Type.BasicType.IntegerType;
import Compiler.Front.Ast.Type.Type;
import Compiler.Utility.AstTool.AstOutput;
import Compiler.Utility.Error.GrammarError;

import java.util.List;

public class PostfixPlusExpr extends UnitaryExpr {

    public PostfixPlusExpr(Type type, boolean isLeftVal, Expr expr) {
        super(type, isLeftVal, expr);
    }

    public static PostfixPlusExpr getPostfixPlusExpr(Expr expr) {
        if(expr.type instanceof IntegerType) {
            if(expr.isLeftVal == false) {
                throw new GrammarError("Error in PostfixPlusExpr -expr.isLeftVal == false");
            }
            return new PostfixPlusExpr(expr.type, false, expr);
        }
        else {
            throw new GrammarError("Error in getPostfixPlusExpr - !type instanceof IntegerType");
        }
    }

    @Override
    public String toString(int level) {
        StringBuilder string = new StringBuilder();
        string.append(AstOutput.outputIndent(level) + "[PostfixPlusExpr]\n");
        string.append(AstOutput.outputIndent(level + 1) + "type : " + type.toString(0));
        string.append(AstOutput.outputIndent(level + 1) + "isLeftVal : " + isLeftVal);
        string.append(AstOutput.outputIndent(level + 1) + "expr :\n");
        string.append(expr.toString(level + 1));
        return string.toString();
    }
    @Override
    public void emit(List<Instruction> instructions) {
        expr.emit(instructions);
        operand = Environment.registerTable.addTemporaryRegister();
        if(!(expr.operand instanceof Address)) {
            expr.load(instructions);
            instructions.add(MoveInstruction.getInstruction(operand, expr.operand));
            instructions.add(AdditionInstruction.getInstruction(expr.operand, expr.operand, new ImmediateValue(1)));
        }
        else {
            Address address = (Address)expr.operand;
            address = new Address(address.origin, address.offset, address.size);
            expr.load(instructions);
            instructions.add(MoveInstruction.getInstruction(operand, expr.operand));
            instructions.add(AdditionInstruction.getInstruction(expr.operand, expr.operand, new ImmediateValue(1)));
            instructions.add(StoreInstruction.getInstruction(expr.operand, address));
        }
    }
}