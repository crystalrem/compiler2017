package Compiler.Front.Ast.Type;

import Compiler.Front.Ast.Node;

public abstract class Type implements Node {
    public int size() {
        return 8;
    }
    public abstract String toString(int level);
    public abstract boolean compare(Type type);
}