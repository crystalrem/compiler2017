package Compiler.Front.Ast.Type;

import Compiler.Front.Ast.Type.BasicType.NullType;
import Compiler.Front.Ast.Type.BasicType.VoidType;
import Compiler.Front.Ast.Type.Type;
import Compiler.Utility.AstTool.AstOutput;
import Compiler.Utility.Error.GrammarError;

public class ArrayType extends Type {
    public Type basicType;
    public int dimension;

    public ArrayType(Type basicType, int dimension) {
        this.basicType = basicType;
        this.dimension = dimension;
    }
    
    public static Type getType(Type basicType, int dimension) {
        if(basicType instanceof ArrayType) {
            return new ArrayType(((ArrayType) basicType).basicType, dimension);
        }
        else {
            if(basicType instanceof VoidType) {
                throw new GrammarError("Error in getArrayType - VoidType can not be basicType");
            }
            return new ArrayType(basicType, dimension);
        }
    }
    @Override
    public String toString(int level) {
        StringBuilder string = new StringBuilder();
        string.append(AstOutput.outputIndent(level) + "[ArrayType]" + "\n");
        string.append(AstOutput.outputIndent(level + 1) +"[basicType]" + "\n");
        string.append(basicType.toString(level + 1));
        string.append(AstOutput.outputIndent(level + 1) +"[dimension]" + "\n");
        string.append(AstOutput.outputIndent(level + 1) + Integer.valueOf(dimension));
        string.append("\n");
        return string.toString();
    }

    public Type superStratum() {
        if(this.dimension == 1) return basicType;
        else return getType(basicType, dimension - 1);
    }
    @Override
    public boolean compare(Type type) {
        if(type instanceof NullType) return true;
        else if(type instanceof ArrayType) {
            //if(!((ArrayType) type).basicType.compare(this.basicType)) System.out.print("####BasicTypeDiff");
            //if(!(((ArrayType) type).dimension == this.dimension))System.out.print("####DimensionDiff");
            return (((ArrayType) type).basicType.compare(this.basicType) && ((ArrayType) type).dimension == this.dimension);
        }
        return false;
    }


}