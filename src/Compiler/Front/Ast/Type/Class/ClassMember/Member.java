package Compiler.Front.Ast.Type.Class.ClassMember;

import Compiler.Enviroment.Environment;
import Compiler.Front.Ast.Type.Class.Class;

public abstract class Member {
    public String name;
    public Class originClass;
    public Member(String name) {
        this.name = name;
        this.originClass = Environment.scopeTable.getClassScope();
    }
    public String toString(int level) {
        return "";
    }
}