package Compiler.Front.Ast.Type.Class.ClassMember;

import Compiler.Enviroment.SymbolTable.Symbol;
import Compiler.Front.Ast.Function;
import Compiler.Front.Ast.Type.Type;

import java.util.HashMap;
import java.util.List;

public class MemberFunction extends Member {
    //public Type type;
    //public Map<String, Symbol> parameters;
    /*public MemberFunction(Type type, String name, Map<String, Symbol> parameters) {
        super(name);
        this.type = type;
        for(String parameterName:parameters.keySet()) {
            this.parameters.put(parameterName, parameters.get(parameterName));
        }
    }*/
    public Function memberFunction;
    public MemberFunction(Type type, String name, List<Symbol> parameters) {
        super(name);
        memberFunction = Function.getFunction(type, name, parameters);
    }
    public MemberFunction getMemberFunction(Type type, String name, List<Symbol> parameters) {
        return new MemberFunction(type, name, parameters);
    }
    public MemberFunction(String name, Function memberFunction) {
        super(name);
        this.memberFunction = memberFunction;
    }
    public MemberFunction getMemberFunction(String name, Function memberFunction) {
        return new MemberFunction(name, memberFunction);
    }

    @Override
    public String toString(int level) {
        StringBuilder string = new StringBuilder();
        string.append(memberFunction.toString(level + 1));
        string.append("\n");
        return string.toString();
    }
}