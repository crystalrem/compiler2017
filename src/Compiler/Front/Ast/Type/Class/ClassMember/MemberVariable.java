package Compiler.Front.Ast.Type.Class.ClassMember;

import Compiler.Back.Translator.Register.Register;
import Compiler.Enviroment.SymbolTable.Symbol;
import Compiler.Front.Ast.Expr.Expr;
import Compiler.Front.Ast.Type.Type;
import Compiler.Utility.AstTool.AstOutput;

public class MemberVariable extends Member {

    public Symbol symbol;
    public Expr expr;
    public int offset;
    public MemberVariable(String name, Symbol symbol) {
        super(name);
        this.symbol = symbol;
        offset = originClass.classSize;
        originClass.classSize += (symbol.type.size() + Register.size() - 1) / Register.size() * Register.size();
        //System.err.println("************" + Integer.toString(symbol.type.size()));
    }

    public MemberVariable(String name, Type type) {
        super(name);
        this.symbol = Symbol.getSymbol(type, name);
        offset = originClass.classSize;
        originClass.classSize += (symbol.type.size() + Register.size() - 1) / Register.size() * Register.size();
        //System.err.println("************" + Integer.toString(symbol.type.size()));
    }
    public MemberVariable getMemberVariable(String name, Symbol symbol) {
        return new MemberVariable(name, symbol);
    }

    public MemberVariable getMemberVariable(String name, Type type) {
        return new MemberVariable(name, type);
    }
    @Override
    public String toString(int level) {
        StringBuilder string = new StringBuilder();
        string.append(AstOutput.outputIndent(level) + "[MemberVariable]" + name + "\n");
        string.append(symbol.toString(level + 1));
        string.append("\n");
        return string.toString();
    }

}