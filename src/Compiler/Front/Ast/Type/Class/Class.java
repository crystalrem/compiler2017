package Compiler.Front.Ast.Type.Class;

import Compiler.Enviroment.ScopeTable.Scope;
import Compiler.Enviroment.SymbolTable.Symbol;
import Compiler.Front.Ast.Function;
import Compiler.Front.Ast.Type.BasicType.NullType;
import Compiler.Front.Ast.Type.Class.ClassMember.Member;
import Compiler.Front.Ast.Type.Class.ClassMember.MemberFunction;
import Compiler.Front.Ast.Type.Class.ClassMember.MemberVariable;
import Compiler.Front.Ast.Type.Type;
import Compiler.Utility.AstTool.AstOutput;
import Compiler.Utility.Error.GrammarError;

import java.util.HashMap;
import java.util.Map;

public class Class extends Type implements Scope {
    public String name;
    public Map<String, MemberVariable> memberVariableList;
    public Map<String, MemberFunction> memberFunctionList;
    public Function constructor;
    public int classSize;


    public Class () {
        name = "";
        memberVariableList = new HashMap<String, MemberVariable>();
        memberFunctionList = new HashMap<String, MemberFunction>();
        constructor = null;
    }
    public Class(String name) {
        this.name = name;
        memberVariableList = new HashMap<String, MemberVariable>();
        memberFunctionList = new HashMap<String, MemberFunction>();
        constructor = null;
    }
    public void addMember(String name, Member member) {
        if(member instanceof MemberVariable) {
            memberVariableList.put(name, (MemberVariable) member);
        }
        else if(member instanceof MemberFunction) {
            memberFunctionList.put(name, (MemberFunction)member);
        }
    }

    public static Class getClass(String name) {
        return new Class(name);
    }

    public void addMember(Type type, String name) {
        if(memberFunctionList.containsKey(name) || memberVariableList.containsKey(name)) {
            throw new GrammarError("Error in -Class getClass(String name) - memberFunctionList.containsKey(name) || memberVariableList.containsKey(name) : " + name);
        }
        if(type instanceof Function) {
            Function currentFunction = (Function)type;
            String memberName = this.name + "." + name;
            MemberFunction memberFunction = new MemberFunction(memberName, currentFunction);
            memberFunctionList.put(name, memberFunction);
        } else {
            //System.out.println("\n::addVariableMember : " + name);
            MemberVariable currentVariable = new MemberVariable(name, type);
            memberVariableList.put(name, currentVariable);
        }
    }

    public void addConstructor(Function constructor) {
        if(this.constructor != null) {
            throw new GrammarError("Error in addConstructor - this.constructor != null : " + name);
        }
        this.constructor = constructor;
    }

    public Member findMember(String name) {
        //System.out.print("\n::findMember : name : " + name + "size : ");
        //System.out.print(memberVariableList.size());
        //System.out.print(" ");
        //System.out.println(memberFunctionList.size());
        //for(String vname : memberVariableList.keySet()) {
            //System.out.println("\n::findMember VariableList ---- " + vname +" compare " + name);
        //}
        if(memberVariableList.containsKey(name)) {
            //System.out.println("\n::findMember in Variable ...........................True");
            return memberVariableList.get(name);
        }
        else if(memberFunctionList.containsKey(name)) {
            return memberFunctionList.get(name);
        }
        else return null;
    }
    @Override
    public String toString(int level) {
        StringBuilder string = new StringBuilder();
        string.append(AstOutput.outputIndent(level) + "[Class]" + name +  "\n");
        string.append(AstOutput.outputIndent(level + 1) + "[memberVariableList]" + "\n");
        for(MemberVariable memberVariable: memberVariableList.values()) {
            string.append(memberVariable.toString(level + 1));
        }
        string.append(AstOutput.outputIndent(level + 1) + "[memberFunctionList]" + "\n");
        for(MemberFunction memberFunction: memberFunctionList.values()) {
            string.append(memberFunction.toString(level + 1));
        }
        string.append(AstOutput.outputIndent(level + 1) + "[constructor]" + "\n");
        if(constructor != null)
        string.append(constructor.toString(level + 1));
        string.append("\n");
        return string.toString();
    }

    @Override
    public boolean compare(Type type) {
        return (type == this || type instanceof NullType);
    }
    @Override
    public int size() {
        return 8;
    }

}