package Compiler.Front.Ast.Type.BasicType;

import Compiler.Front.Ast.Type.ArrayType;
import Compiler.Front.Ast.Type.Class.Class;
import Compiler.Front.Ast.Type.Type;
import Compiler.Utility.AstTool.AstOutput;

public class NullType extends Type {
    public static Type getType() {
        //System.out.println("NullType.getType()");
        return new NullType();
    }
    @Override
    public String toString(int level) {
        return AstOutput.outputIndent(level) + "NullType\n";
    }

    @Override
    public boolean compare(Type type) {
        return (type instanceof NullType || type instanceof ArrayType || type instanceof Class);
    }
}