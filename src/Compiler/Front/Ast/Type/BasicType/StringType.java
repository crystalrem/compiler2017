package Compiler.Front.Ast.Type.BasicType;

import Compiler.Front.Ast.Type.Type;
import Compiler.Utility.AstTool.AstOutput;

public class StringType extends Type {
    public static Type getType() {

        return new StringType();
    }

    @Override
    public String toString(int level) {
        return AstOutput.outputIndent(level) + "StringType\n";
    }

    @Override
    public boolean compare(Type type) {
        return (type instanceof StringType);
    }
}