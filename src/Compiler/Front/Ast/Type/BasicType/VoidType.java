package Compiler.Front.Ast.Type.BasicType;

import Compiler.Front.Ast.Type.Type;
import Compiler.Utility.AstTool.AstOutput;

public class VoidType extends Type {
    public static Type getType() {
        return new VoidType();
    }
    @Override
    public String toString(int level) {
        return AstOutput.outputIndent(level) + "VoidType\n";
    }

    @Override
    public boolean compare(Type type) {
        return false;
    }


}