package Compiler.Front.Ast.Type.BasicType;

import Compiler.Front.Ast.Type.Type;
import Compiler.Utility.AstTool.AstOutput;

public class IntegerType extends Type {
    public static Type getType() {
        //System.out.println("IntegerType.getType()");
        return new IntegerType();
    }
    @Override
    public String toString(int level) {
        return AstOutput.outputIndent(level) + "IntegerType\n";
    }
    @Override
    public boolean compare(Type type) {
        return (type instanceof IntegerType);
    }

}