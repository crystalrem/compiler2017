package Compiler.Front.Ast.Type.BasicType;

import Compiler.Front.Ast.Type.Type;
import Compiler.Utility.AstTool.AstOutput;

public class BoolType extends Type {
    @Override
    public int size() {
        return 8;
    }
    public static Type getType() {
        //System.out.println("IntegerType.getType()");
        return new BoolType();
    }
    @Override
    public String toString(int level) {
        return AstOutput.outputIndent(level) + "BoolType";
    }

    public boolean compare(Type type) {
        return (type instanceof BoolType);
    }
}