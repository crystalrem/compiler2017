package Compiler.Front.Ast.Statement.JumpStatement;

import Compiler.Back.MidCode.Intruction.ControlFlowInstruction.JumpInstruction;
import Compiler.Back.MidCode.Intruction.Instruction;
import Compiler.Enviroment.Environment;
import Compiler.Front.Ast.Statement.LoopStatement.LoopStatement;
import Compiler.Front.Ast.Statement.Statement;
import Compiler.Utility.AstTool.AstOutput;
import Compiler.Utility.Error.GrammarError;

import java.util.List;

public class BreakStatement extends Statement {
    public LoopStatement loop;
    public BreakStatement(LoopStatement loop) {
        this.loop = loop;
    }
    public static BreakStatement getBreakStatement() {

        if (Environment.scopeTable.getLoopScope() == null) {
            throw new GrammarError("Error in getContinueStatement: getLoopScope == null");
        }
        return new BreakStatement(Environment.scopeTable.getLoopScope());
    }
    public String toString(int level) {
        return AstOutput.outputIndent(level) + "[break]";
    }

    public void emit(List<Instruction> instructions) {
        instructions.add(JumpInstruction.getInstruction(loop.endLabel));
    }
}