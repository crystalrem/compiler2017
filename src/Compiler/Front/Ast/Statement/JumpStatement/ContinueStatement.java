package Compiler.Front.Ast.Statement.JumpStatement;

import Compiler.Back.MidCode.Intruction.ControlFlowInstruction.JumpInstruction;
import Compiler.Back.MidCode.Intruction.Instruction;
import Compiler.Enviroment.Environment;
import Compiler.Front.Ast.Statement.LoopStatement.LoopStatement;
import Compiler.Front.Ast.Statement.Statement;
import Compiler.Utility.AstTool.AstOutput;
import Compiler.Utility.Error.GrammarError;

import java.util.List;

public class ContinueStatement extends Statement {
    public LoopStatement loop;
    public ContinueStatement(LoopStatement loop) {
        this.loop = loop;
    }
    public static ContinueStatement getContinueStatement() {
        if (Environment.scopeTable.getLoopScope() == null) {
            throw new GrammarError("Error in getContinueStatement: getLoopScope == null");
        }
        return new ContinueStatement(Environment.scopeTable.getLoopScope());
    }
    public String toString(int level) {
        return AstOutput.outputIndent(level) + "[continue]";
    }
    public void emit(List<Instruction> instructions) {
        instructions.add(JumpInstruction.getInstruction(loop.loopLabel));
    }

}