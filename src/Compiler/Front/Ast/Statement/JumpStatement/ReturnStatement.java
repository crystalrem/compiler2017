package Compiler.Front.Ast.Statement.JumpStatement;

import Compiler.Back.MidCode.Intruction.ControlFlowInstruction.JumpInstruction;
import Compiler.Back.MidCode.Intruction.FunctionInstruction.ReturnInstruction;
import Compiler.Back.MidCode.Intruction.Instruction;
import Compiler.Enviroment.Environment;
import Compiler.Front.Ast.Expr.Expr;
import Compiler.Front.Ast.Function;
import Compiler.Front.Ast.Statement.Statement;
import Compiler.Front.Ast.Type.BasicType.VoidType;
import Compiler.Utility.AstTool.AstOutput;
import Compiler.Utility.Error.GrammarError;

import java.util.List;

public class ReturnStatement extends Statement {
    public Expr expr;
    public Function function;
    public ReturnStatement(Expr expr, Function function) {
        this.expr = expr;
        this.function = function;
    }
    public static ReturnStatement getReturnStatement(Expr expr) {
        Function function = Environment.scopeTable.getFunctionScope();
        if (function == null) {
            throw new GrammarError("Error in getReturnStatement - returnStatement should in a function");
        }
        else {
            if (expr == null) {
                if (function.type instanceof VoidType) {
                    return new ReturnStatement(expr, function);
                }
                else {
                    throw new GrammarError("Error in getReturnStatement - !expr.type.compare(function.type) VoidType");
                }
            } else {
                if (expr.type.compare(function.type)) {
                    return new ReturnStatement(expr, function);
                }
                else {
                    //System.out.println("####Return - " + expr.toString(0) + "\n" + function.type.toString(0));
                    throw new GrammarError("Error in getReturnStatement - !expr.type.compare(function.type)");
                }
            }
        }
        //return new ReturnStatement(expr);
    }
    public String toString(int level) {
        StringBuilder s = new StringBuilder();
        s.append(AstOutput.outputIndent(level) + "[return]");
        s.append(AstOutput.outputIndent(level + 1) + "[return.expr]");
        s.append(expr.toString(level + 1));
        return s.toString();
    }
    public void emit(List<Instruction> instructions) {
        if (expr != null) {
            expr.emit(instructions);
            expr.load(instructions);
            instructions.add(ReturnInstruction.getInstruction(expr.operand));
        }
        instructions.add(JumpInstruction.getInstruction(function.exit));
    }

}