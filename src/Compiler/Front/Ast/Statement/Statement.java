package Compiler.Front.Ast.Statement;

import Compiler.Back.MidCode.Intruction.Instruction;
import Compiler.Front.Ast.Node;

import java.util.List;

public abstract class Statement implements Node {
    public abstract void emit(List<Instruction> instructions);
}