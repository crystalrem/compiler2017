package Compiler.Front.Ast.Statement;

import Compiler.Back.MidCode.Intruction.Instruction;
import Compiler.Enviroment.ScopeTable.Scope;
import Compiler.Front.Ast.Expr.Expr;
import Compiler.Front.Ast.Statement.Statement;
import Compiler.Utility.AstTool.AstOutput;

import java.util.ArrayList;
import java.util.List;

public class BlockStatement extends Statement implements Scope {
    List<Statement> statements;
    public BlockStatement() {
        this.statements = new ArrayList<>();
    }
    public BlockStatement(List<Statement> statements) {
        this.statements.addAll(statements);
    }
    public BlockStatement getBlockStatement(List<Statement> statements) {
        return new BlockStatement(statements);
    }
    public static BlockStatement getBlockStatement() {
        return new BlockStatement();
    }
    public void addStatement(Statement statement) {
        statements.add(statement);
    }
    public String toString(int level) {
        StringBuilder s = new StringBuilder(AstOutput.outputIndent(level) + "[BlockStatement]" + "\n");
        for (int i = 0; i < statements.size(); i++) {
            s.append(statements.get(i).toString(level + 1));
        }
        s.append("\n");
        return s.toString();
    }
    public void emit(List<Instruction> instructions) {
        for (int i = 0; i < statements.size(); i++) {
            statements.get(i).emit(instructions);
        }
        return ;
    }

}