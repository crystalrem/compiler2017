package Compiler.Front.Ast.Statement.LoopStatement;

import Compiler.Back.MidCode.Intruction.ControlFlowInstruction.BranchInstruction;
import Compiler.Back.MidCode.Intruction.ControlFlowInstruction.JumpInstruction;
import Compiler.Back.MidCode.Intruction.Instruction;
import Compiler.Back.MidCode.Intruction.LabelIntruction.LabelInstruction;
import Compiler.Enviroment.ScopeTable.Scope;
import Compiler.Front.Ast.Expr.Expr;
import Compiler.Front.Ast.Statement.Statement;
import Compiler.Front.Ast.Type.BasicType.BoolType;
import Compiler.Utility.AstTool.AstOutput;
import Compiler.Utility.Error.GrammarError;

import java.util.List;

public class WhileStatement extends LoopStatement implements Scope {
    public Expr condition;
    public Statement statement;

    public static  WhileStatement getWhileStatement() {
        return new WhileStatement();
    }

    public void addCondition(Expr condition) {
        if (!(condition.type instanceof BoolType)) {
            throw new GrammarError("Error in WhileStatement - addCondition -!(condition.type instanceof BoolType)");
        }
        this.condition = condition;
    }

    public void addStatement(Statement statement) {
        this.statement = statement;
    }

    @Override
    public String toString (int level) {
        StringBuilder string = new StringBuilder(AstOutput.outputIndent(level) + "[WhileStatement]\n");
        string.append(AstOutput.outputIndent(level + 1) + "[condition]\n");
        string.append(condition.toString(level + 1));
        string.append(AstOutput.outputIndent(level + 1) + "[blockStatement]\n");
        string.append(statement.toString(level + 1));
        string.append("\n");
        return string.toString();
    }
    public void emit(List<Instruction> instructions) {
        this.loopLabel = LabelInstruction.getInstruction("while_loop_Label");
        LabelInstruction statementLabel = LabelInstruction.getInstruction("while_statement_label");
        this.endLabel = LabelInstruction.getInstruction("while_end_label");
        /*
        while_loop_label:
            condition
            branch(!condition) while_end_label
        while_statement_label
            statement
            jump while_begin_label
        while_end_label
        * */
        instructions.add(JumpInstruction.getInstruction(loopLabel));
        instructions.add(loopLabel);
        condition.emit(instructions);
        instructions.add(BranchInstruction.getInstruction(condition.operand, statementLabel, endLabel));
        instructions.add(statementLabel);
        statement.emit(instructions);
        instructions.add(JumpInstruction.getInstruction(loopLabel));
        instructions.add(endLabel);
    }

}