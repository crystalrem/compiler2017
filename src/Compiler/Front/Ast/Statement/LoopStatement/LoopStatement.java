package Compiler.Front.Ast.Statement.LoopStatement;

import Compiler.Back.MidCode.Intruction.Instruction;
import Compiler.Back.MidCode.Intruction.LabelIntruction.LabelInstruction;
import Compiler.Front.Ast.Statement.Statement;

import java.util.List;

public abstract class LoopStatement extends Statement {
    public abstract void emit(List<Instruction> instructions);
    public LabelInstruction endLabel, loopLabel;
}