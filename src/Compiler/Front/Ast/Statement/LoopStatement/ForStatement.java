package Compiler.Front.Ast.Statement.LoopStatement;

import Compiler.Back.MidCode.Intruction.ControlFlowInstruction.BranchInstruction;
import Compiler.Back.MidCode.Intruction.ControlFlowInstruction.JumpInstruction;
import Compiler.Back.MidCode.Intruction.Instruction;
import Compiler.Back.MidCode.Intruction.LabelIntruction.LabelInstruction;
import Compiler.Enviroment.ScopeTable.Scope;
import Compiler.Front.Ast.Expr.ConstantExpr.BoolConstant;
import Compiler.Front.Ast.Expr.Expr;
import Compiler.Front.Ast.Statement.BlockStatement;
import Compiler.Front.Ast.Statement.Statement;
import Compiler.Front.Ast.Type.BasicType.BoolType;
import Compiler.Utility.AstTool.AstOutput;
import Compiler.Utility.Error.GrammarError;

import java.util.List;

public class ForStatement extends LoopStatement implements Scope{
    public Expr initialization, condition, step;
    public Statement statement;

    public void addInitialization(Expr initialization) {
        this.initialization = initialization;
    }

    public void addCondition(Expr condition) {

        if(condition == null) {
            BoolConstant tmp = new BoolConstant(true);
            condition = tmp;
        }
        if (!(condition.type instanceof BoolType)) {
            throw new GrammarError("Error in ForStatement - addCondition -!(condition.type instanceof BoolType)");
        }
        this.condition = condition;
    }

    public void addStep(Expr step) {
        this.step = step;
    }

    public void addStatement(Statement statement) {
        this.statement = statement;
    }

    public static ForStatement getForStatement() {
        return new ForStatement();
    }

    @Override
    public String toString(int level) {
        StringBuilder string = new StringBuilder(AstOutput.outputIndent(level) + "[ForStatement]\n");
        string.append(AstOutput.outputIndent(level + 1) + "[initialization]\n");
        if(initialization != null)
        string.append(initialization.toString(level + 1));
        string.append(AstOutput.outputIndent(level + 1) + "[condition]\n");
        if(condition != null)
        string.append(condition.toString(level + 1));
        string.append(AstOutput.outputIndent(level + 1) + "[step]\n");
        if(step != null)
        string.append(step.toString(level + 1));
        string.append(AstOutput.outputIndent(level + 1) + "[blockStatement]\n");
        string.append(statement.toString(level + 1));
        string.append("\n");
        return string.toString();
    }
    public void emit(List<Instruction> instructions) {
        LabelInstruction conditionLabel = LabelInstruction.getInstruction("for_condition_label");
        LabelInstruction statementLabel = LabelInstruction.getInstruction("for_statement_label");
        this.loopLabel = LabelInstruction.getInstruction("for_loop_label");
        this.endLabel = LabelInstruction.getInstruction("for_end_label");
        if(initialization != null) {
            initialization.emit(instructions);
        }
        instructions.add(JumpInstruction.getInstruction(conditionLabel));
        instructions.add(conditionLabel);
        if(condition == null) {
            addCondition(null);
        }
        condition.emit(instructions);
        instructions.add(BranchInstruction.getInstruction(condition.operand, statementLabel, endLabel));
        instructions.add(statementLabel);
        if(statement != null) {
            statement.emit(instructions);
        }
        instructions.add(JumpInstruction.getInstruction(loopLabel));
        instructions.add(loopLabel);
        if(step != null) {
            step.emit(instructions);
        }
        instructions.add(JumpInstruction.getInstruction(conditionLabel));
        instructions.add(endLabel);
    }


}
