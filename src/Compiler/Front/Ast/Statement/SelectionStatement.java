package Compiler.Front.Ast.Statement;

import Compiler.Back.MidCode.Intruction.ControlFlowInstruction.BranchInstruction;
import Compiler.Back.MidCode.Intruction.ControlFlowInstruction.JumpInstruction;
import Compiler.Back.MidCode.Intruction.Instruction;
import Compiler.Back.MidCode.Intruction.LabelIntruction.LabelInstruction;
import Compiler.Front.Ast.Expr.Expr;
import Compiler.Front.Ast.Statement.Statement;
import Compiler.Front.Ast.Type.BasicType.BoolType;
import Compiler.Utility.AstTool.AstOutput;
import Compiler.Utility.Error.GrammarError;

import java.util.List;

public class SelectionStatement extends Statement {
    Expr expr;
    Statement ifStatements, elseStatements;

    public SelectionStatement(Expr expr, Statement ifStatements, Statement elseStatements) {
        this.ifStatements = ifStatements;
        this.elseStatements = elseStatements;
        this.expr = expr;
    }

    public static SelectionStatement getSelectionStatement(Expr expr, Statement ifStatements, Statement elseStatements) {
       // System.out.println("\n### getSelectionStatement :" + expr.toString(0));
        if(!(expr.type instanceof BoolType)) {
            throw new GrammarError("Error in getSelectionStatement - !(expr.type instanceof BoolType)");
        }
        return new SelectionStatement(expr, ifStatements, elseStatements);
    }

    public String toString(int level) {
        StringBuilder s = new StringBuilder(AstOutput.outputIndent(level) + "[SelectionStatement]" + "\n");

        s.append(AstOutput.outputIndent(level + 1) + "[if(expr)]\n");
        s.append(expr.toString(level + 1));

        s.append(AstOutput.outputIndent(level + 1) + "[ifStatements]\n");

        s.append(ifStatements.toString(level + 1));


        s.append(AstOutput.outputIndent(level + 1) + "[elseStatements]\n");
        if(elseStatements != null)
        s.append(elseStatements.toString(level + 1));

        s.append("\n");
        return s.toString();
    }

    public void emit(List<Instruction> instructions) {
        LabelInstruction ifLabel = LabelInstruction.getInstruction("branch_if_label");
        LabelInstruction elseLabel = LabelInstruction.getInstruction("branch_else_label");
        LabelInstruction endLabel =  LabelInstruction.getInstruction("branch_end_label");

        expr.emit(instructions);
        expr.load(instructions);
        instructions.add(BranchInstruction.getInstruction(expr.operand, ifLabel, elseLabel));
        instructions.add(ifLabel);
        if(ifStatements != null) {
            ifStatements.emit(instructions);
        }
        instructions.add(JumpInstruction.getInstruction(endLabel));
        instructions.add(elseLabel);
        if(elseStatements != null) {
            elseStatements.emit(instructions);
        }
        instructions.add(JumpInstruction.getInstruction(endLabel));
        instructions.add(endLabel);


    }

}