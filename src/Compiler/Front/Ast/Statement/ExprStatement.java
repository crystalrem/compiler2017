package Compiler.Front.Ast.Statement;

import Compiler.Back.MidCode.Intruction.Instruction;
import Compiler.Front.Ast.Expr.Expr;

import java.util.List;

public class ExprStatement extends Statement{

    public Expr expr;
    public ExprStatement(Expr expr) {
        this.expr = expr;
    }
    public static ExprStatement getExprStatement(Expr expr) {
        return new ExprStatement(expr);
    }
    @Override
    public String toString(int level) {
        return expr.toString(level);
    }
    public void emit(List<Instruction> instructions) {
        if(expr != null) expr.emit(instructions);
    }
}