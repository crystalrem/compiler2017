package Compiler.Front.Ast.Statement;

import Compiler.Back.MidCode.Intruction.ArithmeticInstruction.MemoryInstruction.MoveInstruction;
import Compiler.Back.MidCode.Intruction.Instruction;
import Compiler.Enviroment.SymbolTable.Symbol;
import Compiler.Front.Ast.Expr.Expr;
import Compiler.Front.Ast.Type.BasicType.VoidType;
import Compiler.Utility.AstTool.AstOutput;
import Compiler.Utility.Error.GrammarError;

import java.util.List;

public class VariableDefinition extends Statement {
    public Expr expr;
    public Symbol symbol;


    public VariableDefinition(Symbol symbol, Expr expr) {
        if (symbol.type instanceof VoidType) {
            throw new GrammarError("Error in getVariableDefinition - variable can not be VoidType");
        }
        this.symbol = symbol;
        this.expr = expr;
    }

    public static VariableDefinition getVariableDefinition(Symbol symbol, Expr expr) {
        if((expr != null) && (!symbol.type.compare(expr.type))){
            //System.out.print("\n####" + symbol.type + " " + expr.type);
            throw new GrammarError("Error in getVariableDefinition - symol & expr are not the same type");
        }
        return new VariableDefinition(symbol, expr);
    }
    @Override
    public String toString(int level) {
        StringBuilder string = new StringBuilder();
        string.append(AstOutput.outputIndent(level) + "[VariableDefinition] \n");
        string.append(AstOutput.outputIndent(level + 1) + "[symbol] \n");
        string.append(symbol.toString(level + 1));
        string.append(AstOutput.outputIndent(level + 1) + "[expr] \n");
        if(expr != null)
        string.append(expr.toString(level + 1));
        string.append("\n");
        return string.toString();
    }
    public void emit(List<Instruction> instructions) {
        if (expr != null) {
            expr.emit(instructions);
            expr.load(instructions);
            instructions.add(MoveInstruction.getInstruction(symbol.register, expr.operand));
        }
    }

}