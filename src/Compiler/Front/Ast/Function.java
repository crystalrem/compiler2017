package Compiler.Front.Ast;

import Compiler.Back.Allocator.GlobalRegisterAllocator.GlobalRegisterAllocator;
import Compiler.Back.MidCode.Intruction.LabelIntruction.LabelInstruction;
import Compiler.Back.MidCode.MidCode;
import Compiler.Enviroment.ScopeTable.Scope;
import Compiler.Enviroment.SymbolTable.Symbol;
import Compiler.Front.Ast.Statement.BlockStatement;
import Compiler.Front.Ast.Type.Class.Class;
import Compiler.Front.Ast.Type.Type;
import Compiler.Utility.AstTool.AstOutput;

import java.util.ArrayList;
import java.util.List;

public class Function extends Type implements Node, Scope {
    public Type type;
    public String name;
    public List<Symbol> parameters;
    public BlockStatement blockStatement;
    public MidCode midCode;
    public LabelInstruction enter;
    public LabelInstruction entry, exit;
    public GlobalRegisterAllocator allocator;

    public Function (){
        type = null;
        name = "";
        parameters = new ArrayList<>();
        blockStatement = null;
    }

    public Function(Type type, String name, List<Symbol> parameters) {
        //System.out.print(name + " Function -ListSize = ");
        //System.out.println(parameters.size());
        this.name = name;
        this.type = type;
        this.parameters = parameters;
    }

    public static Function getFunction(Type type, String name, List<Symbol> parameters) {
        return new Function(type, name, parameters);
    }
    public void addBlockStatement (BlockStatement blockStatement) {
        this.blockStatement = blockStatement;
    }

    @Override
    public String toString(int level) {
        StringBuilder string = new StringBuilder();
        string.append(AstOutput.outputIndent(level) + "[Function]" );
        if(this.type instanceof Class){
            string.append(((Class) this.type).name);
        }
        else {
            //string.append(this.type.toString(0));
        }
        string.append(AstOutput.outputIndent(level + 1) +"[FunctionName] " +this.name + "\n");
        string.append(AstOutput.outputIndent(level + 1) + "[parameters]");
        //System.out.print("parameters.size() = ");
        //System.out.println(parameters.size());
        for(int i = 0; i < parameters.size(); i++) {
            string.append(this.parameters.get(i).toString(level + 1));
        }
        string.append("\n");
        string.append(AstOutput.outputIndent(level + 1) + "[blockStatement] {");
        //if(blockStatement != null)string.append(blockStatement.toString(level + 1));
        string.append(AstOutput.outputIndent(level + 1) + "}");
        string.append("\n");
        return string.toString();
    }

    @Override
    public boolean compare(Type type) {
        return false;
    }

}