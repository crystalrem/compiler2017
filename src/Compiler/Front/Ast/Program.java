package Compiler.Front.Ast;

import Compiler.Enviroment.ScopeTable.Scope;
import Compiler.Front.Ast.Statement.VariableDefinition;
import Compiler.Utility.AstTool.AstOutput;
import Compiler.Utility.Error.GrammarError;
import Compiler.Front.Ast.Type.Class.Class;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Program implements Node, Scope {
    public String name;
    public Map<String, Function> functionMap;
    public List<Function> functions;
    public Map<String, Class> classMap;
    public Map<String, VariableDefinition> variableDefinitionMap;
    public List<VariableDefinition> globalVariables;

    public Program () {
        name = "Program";
        functionMap = new HashMap<>();
        classMap = new HashMap<>();
        variableDefinitionMap = new HashMap<>();
        functions = new ArrayList<>();
        globalVariables = new ArrayList<>();
    }

    public static Program getProgram() {
        return new Program();
    }

    public void addClass(String name, Class newClass) {
        if(classMap.containsKey(name)) {
            throw new GrammarError("ProgramError: Double Class " + name);
        }
        classMap.put(name, newClass);
    }

    public void addFunction(String name, Function newFunction) {
        //System.out.println("++++++++++++++++AddFunction" + name);
        //if(functionMap.containsKey(name)) {
        //    throw new GrammarError("ProgramError: Double Function " + name);
        //}
        functionMap.put(name, newFunction);
        functions.add(newFunction);
    }

    public void addVariableDefinition(VariableDefinition newVariableDefinition) {
        String name = newVariableDefinition.symbol.name;
        if(variableDefinitionMap.containsKey(name)) {
            throw new GrammarError("ProgramError: Double variableDefinition " + name);
        }
        variableDefinitionMap.put(name, newVariableDefinition);
        globalVariables.add(newVariableDefinition);
    }
    @Override
    public String toString(int level) {
        StringBuilder string = new StringBuilder();
        string.append(AstOutput.outputIndent(level) + "[Program]" + "\n");
        for(Function currentFunction : functionMap.values()) {
            string.append(currentFunction.toString(level + 1) + "\n");
        }
        for(Class currentClass : classMap.values()) {
            string.append(currentClass.toString(level + 1) + "\n");

        }
        for(VariableDefinition currentVariableDefinition : variableDefinitionMap.values()) {
            string.append(currentVariableDefinition.toString(level + 1) + "\n");
        }
        string.append("\n");
        return string.toString();
    }
}