package Compiler.Front.Cst.Listener;

import Compiler.Enviroment.Environment;
import Compiler.Front.Ast.Program;
import Compiler.Front.Cst.Listener.BaseListener;
import Compiler.Front.Cst.Parser.MxParser;
import Compiler.Front.Ast.Type.Class.Class;

public class ClassListener extends BaseListener {
    @Override
    public void exitClassDefinition(MxParser.ClassDefinitionContext ctx) {
        String name = ctx.IDENTIFIER(0).getText();
        Class newClass = Class.getClass(name);
        Environment.program.addClass(name, newClass);
        Environment.classTable.addClass(name, newClass);
        returnNode.put(ctx, newClass);
        //System.out.println("");
        //System.out.print(ctx);
        //System.out.print(" # ");
        //System.out.println(newClass);
    }
}