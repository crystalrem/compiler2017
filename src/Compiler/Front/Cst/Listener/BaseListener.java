package Compiler.Front.Cst.Listener;

import Compiler.Front.Ast.Node;
import Compiler.Front.Cst.Parser.MxBaseListener;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTreeProperty;

public abstract class BaseListener extends MxBaseListener {
    static ParseTreeProperty<Node> returnNode = new ParseTreeProperty<>();
    static int positionRow, positionColumn;
    @Override
    public void exitEveryRule(ParserRuleContext ctx) {
        positionRow = ctx.getStart().getLine();
        positionColumn = ctx.getStart().getCharPositionInLine();
    }

}