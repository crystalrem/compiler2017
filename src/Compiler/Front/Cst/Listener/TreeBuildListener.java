package Compiler.Front.Cst.Listener;// Generated from Mx.g4 by ANTLR 4.6

import Compiler.Enviroment.Environment;
import Compiler.Enviroment.ScopeTable.Scope;
import Compiler.Enviroment.SymbolTable.Symbol;
import Compiler.Front.Ast.Expr.ConstantExpr.BoolConstant;
import Compiler.Front.Ast.Expr.ConstantExpr.IntegerConstant;
import Compiler.Front.Ast.Expr.ConstantExpr.NullConstant;
import Compiler.Front.Ast.Expr.ConstantExpr.StringConstant;
import Compiler.Front.Ast.Expr.DualityExpr.*;
import Compiler.Front.Ast.Expr.Expr;
import Compiler.Front.Ast.Expr.FunctionCallExpr;
import Compiler.Front.Ast.Expr.NewExpr;
import Compiler.Front.Ast.Expr.UnitaryExpr.*;
import Compiler.Front.Ast.Expr.VariableExpression.ArrayMemberExpr;
import Compiler.Front.Ast.Expr.VariableExpression.ClassMemberExpr;
import Compiler.Front.Ast.Expr.VariableExpression.IdentifierExpr;
import Compiler.Front.Ast.Function;
import Compiler.Front.Ast.Statement.*;
import Compiler.Front.Ast.Statement.JumpStatement.BreakStatement;
import Compiler.Front.Ast.Statement.JumpStatement.ContinueStatement;
import Compiler.Front.Ast.Statement.JumpStatement.ReturnStatement;
import Compiler.Front.Ast.Statement.LoopStatement.ForStatement;
import Compiler.Front.Ast.Statement.LoopStatement.WhileStatement;
import Compiler.Front.Ast.Type.ArrayType;
import Compiler.Front.Ast.Type.BasicType.BoolType;
import Compiler.Front.Ast.Type.BasicType.IntegerType;
import Compiler.Front.Ast.Type.BasicType.StringType;
import Compiler.Front.Ast.Type.BasicType.VoidType;
import Compiler.Front.Ast.Type.Class.ClassMember.Member;
import Compiler.Front.Ast.Type.Class.ClassMember.MemberFunction;
import Compiler.Front.Ast.Type.Class.ClassMember.MemberVariable;
import Compiler.Front.Ast.Type.Type;
import Compiler.Front.Ast.Type.Class.Class;
import Compiler.Front.Cst.Parser.MxListener;
import Compiler.Front.Cst.Parser.MxParser;
import Compiler.Utility.Error.GrammarError;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.TerminalNode;

import java.util.ArrayList;
import java.util.List;

import static Compiler.Front.Cst.Parser.MxParser.*;

public class TreeBuildListener extends BaseListener {

    @Override public void exitProgram(ProgramContext ctx) {
        List<VariableDefinitionContext> variableDefinitionContexts = ctx.variableDefinition();
        for (int i = 0; i < variableDefinitionContexts.size(); i++) {
            VariableDefinition variableDefinition = (VariableDefinition) returnNode.get(variableDefinitionContexts.get(i));
            Environment.program.addVariableDefinition(variableDefinition);
        }
    }

    @Override public void enterClassDefinition(ClassDefinitionContext ctx) {
        Class currentClass = (Class)returnNode.get(ctx);
        Environment.enterScope(currentClass);
        for(MemberVariable currentVariable : currentClass.memberVariableList.values()) {
            Environment.symbolTable.add(currentVariable.symbol.type, currentVariable.symbol.name);
        }
        for(MemberFunction currentFunction : currentClass.memberFunctionList.values()) {
            Environment.symbolTable.add(currentFunction.memberFunction, currentFunction.name);
        }
    }

    @Override public void exitClassDefinition(ClassDefinitionContext ctx) {
        /*Class currentClass = (Class)returnNode.get(ctx);
        for(int i = 0; i < ctx.variableDefinition().size(); i++) {
            VariableDefinitionContext variableDefinitionContext = ctx.variableDefinition().get(i);

            //............................................................
            System.out.print("\n# [exitClassDefinition] ");
            System.out.println(variableDefinitionContext.IDENTIFIER().size());
            //............................................................

            String name = variableDefinitionContext.IDENTIFIER(0).getText();
            if(variableDefinitionContext.expr(0) != null) {
                Member member = currentClass.findMember(name);
                if(member instanceof  MemberVariable) {
                    MemberVariable memberVariable = (MemberVariable) member;
                    memberVariable.expr = (Expr)returnNode.get(variableDefinitionContext.expr(0));
                }
            }
        }*/
        Environment.exitScope();
    }

    @Override public void enterFunctionDefinition(FunctionDefinitionContext ctx) {
        Function function = (Function)returnNode.get(ctx);
        Environment.enterScope(function);
    }

    @Override public void exitFunctionDefinition(FunctionDefinitionContext ctx) {
        Function function = (Function)returnNode.get(ctx);
        function.addBlockStatement((BlockStatement)returnNode.get(ctx.blockStatement()));
        Environment.exitScope();
    }

    @Override public void exitVariableDefinition(VariableDefinitionContext ctx) {
        if (!(ctx.parent instanceof ClassDefinitionContext)) {
            Type type = (Type)returnNode.get(ctx.type());
            String name = ctx.IDENTIFIER(0).getText();
            if(name.equals("this")) {
                throw new GrammarError("Error in exitVariableDefinition -VariableName Can not be this");
            }
            Symbol symbol;
            if (Environment.scopeTable.getScope() == Environment.program) {
                symbol = Environment.symbolTable.addGlobalVariable(type, name);
            } else {
                symbol = Environment.symbolTable.addTemporaryVariable(type, name);
            }
            Expr expression = (Expr)returnNode.get(ctx.expr(0));
            returnNode.put(ctx, VariableDefinition.getVariableDefinition(symbol, expression));
        }
    }

    @Override public void enterStatement(StatementContext ctx) {
        if(ctx.parent instanceof SelectionStatementContext) {
            Environment.enterScope(null);
        }
    }

    @Override public void exitStatement(StatementContext ctx) {
        if(ctx.parent instanceof SelectionStatementContext) {
            Environment.exitScope();
        }
        returnNode.put(ctx, returnNode.get(ctx.getChild(0)));
    }

    @Override public void exitExprStatement(ExprStatementContext ctx) {
        returnNode.put(ctx, ExprStatement.getExprStatement((Expr)returnNode.get(ctx.expr())));
    }

    @Override public void exitSelectionStatement(SelectionStatementContext ctx) {
        returnNode.put(ctx, SelectionStatement.getSelectionStatement((Expr)returnNode.get(ctx.expr()),
                (Statement) returnNode.get(ctx.statement(0)),
                (Statement) returnNode.get(ctx.statement(1))));
    }

    @Override public void enterWhileStatement(WhileStatementContext ctx) {
        WhileStatement whileStatement = WhileStatement.getWhileStatement();
        Environment.enterScope(whileStatement);
        returnNode.put(ctx, whileStatement);
    }

    @Override public void exitWhileStatement(WhileStatementContext ctx) {
        ((WhileStatement)returnNode.get(ctx)).addCondition((Expr)returnNode.get(ctx.expr()));
        ((WhileStatement)returnNode.get(ctx)).addStatement((Statement)returnNode.get(ctx.statement()));
        Environment.exitScope();
    }

    @Override public void enterForStatement(ForStatementContext ctx) {
        ForStatement forStatement = ForStatement.getForStatement();
        Environment.enterScope(forStatement);
        returnNode.put(ctx, forStatement);
    }

    @Override public void exitForStatement(ForStatementContext ctx) {
        ForStatement forStatement = (ForStatement)returnNode.get(ctx);
        int cnt = 0;
        for (ParseTree children : ctx.children) {
            if (children.getText().equals(";")) {
                cnt++;
            }
            if (children instanceof ExprContext) {
                //System.out.println(cnt);
                Expr expr = (Expr)returnNode.get(children);
                if (cnt == 0) {
                    forStatement.addInitialization(expr);
                }
                else if (cnt == 1) {
                    //System.out.print("####" + expr.toString(0));
                    forStatement.addCondition(expr);
                }
                else if (cnt == 2) {
                    forStatement.addStep(expr);
                }
                else {
                    throw new GrammarError("Error in exitForStatement - cnt of children expr > 2");
                }
            }
        }
        forStatement.addStatement((Statement)returnNode.get(ctx.statement()));
        Environment.exitScope();
    }


    @Override public void exitContinueStatement(ContinueStatementContext ctx) {
        returnNode.put(ctx, ContinueStatement.getContinueStatement());
    }

    @Override public void exitBreakStatement(BreakStatementContext ctx) {
        returnNode.put(ctx, BreakStatement.getBreakStatement());
    }

    @Override public void exitReturnStatement(ReturnStatementContext ctx) {
        ReturnStatement returnStatement = ReturnStatement.getReturnStatement((Expr)returnNode.get(ctx.expr()));
        returnNode.put(ctx, returnStatement);
    }

    @Override public void enterBlockStatement(BlockStatementContext ctx) {
        BlockStatement blockStatement = BlockStatement.getBlockStatement();
        Environment.enterScope(blockStatement);
        //System.out.println("\n::enterBlockStatement :\n");
        if(ctx.parent instanceof FunctionDefinitionContext) {
            //System.out.println("\n  ::ctx.parent instanceof FunctionDefinitionContext\n");
            Function function = (Function) returnNode.get(ctx.parent);
            for (int i = 0; i < function.parameters.size(); i++) {
                Symbol symbol = function.parameters.get(i);
                function.parameters.set(i, Environment.symbolTable.addParameterVariable(symbol.type, symbol.name));
            }
        }
        returnNode.put(ctx, blockStatement);
    }

    @Override public void exitBlockStatement(BlockStatementContext ctx) {
        for(int i = 0; i < ctx.statement().size(); i++) {
            ((BlockStatement)returnNode.get(ctx)).addStatement((Statement)(returnNode.get(ctx.statement(i))));
        }
        Environment.exitScope();
    }

    @Override public void exitAssignmentExpr(AssignmentExprContext ctx) {
        returnNode.put(ctx, DualityAssignmentExpr.getDualityAssignmentExpr((Expr)returnNode.get(ctx.expr(0)), (Expr)returnNode.get(ctx.expr(1))));
    }

    @Override public void exitPostfixExpr(MxParser.PostfixExprContext ctx) {
        Expr expr = (Expr)returnNode.get(ctx.expr());
        if(ctx.operator.getText().equals("++")) {
            returnNode.put(ctx, PostfixPlusExpr.getPostfixPlusExpr(expr));
        }
        else if(ctx.operator.getText().equals("--")) {
            returnNode.put(ctx, PostfixMinusExpr.getPostfixMinusExpr(expr));
        }
    }

    @Override public void exitFunctionCallExpr(FunctionCallExprContext ctx) {
        Expr function = (Expr)returnNode.get(ctx.expr(0));
        List<Expr> parameters = new ArrayList<>();
        for (int i = 1; i < ctx.expr().size(); ++i) {
            Expr parameter = (Expr)returnNode.get(ctx.expr(i));
            parameters.add(parameter);
        }
        returnNode.put(ctx, FunctionCallExpr.getFunctionCallExpr(function, (ArrayList<Expr>) parameters));
    }

    @Override public void exitAndExpr(AndExprContext ctx) {
        returnNode.put(ctx, DualityAndExpr.getDualityAndExpr((Expr)returnNode.get(ctx.expr(0)), (Expr)returnNode.get(ctx.expr(1))));
    }

    @Override public void exitLogicalAndExpr(LogicalAndExprContext ctx) {
        returnNode.put(ctx, DualityLogicalAndExpr.getDualityLogicalAndExpr((Expr)returnNode.get(ctx.expr(0)), (Expr)returnNode.get(ctx.expr(1))));

    }

    @Override public void exitPartialOrderExpr(PartialOrderExprContext ctx) {
        //|	expr oper = ('<=' | '>=' | '<' | '>') expr                      #partialOrderExpr
        if(ctx.oper.getText().equals("<=")) {
            returnNode.put(ctx, DualityNoMoreExpr.getDualityNoMoreExpr((Expr)returnNode.get(ctx.expr(0)), (Expr)returnNode.get(ctx.expr(1))));
        }
        else if(ctx.oper.getText().equals(">=")) {
            returnNode.put(ctx, DualityNoLessExpr.getDualityNoLessExpr((Expr)returnNode.get(ctx.expr(0)), (Expr)returnNode.get(ctx.expr(1))));
        }
        else if(ctx.oper.getText().equals("<")) {
            returnNode.put(ctx, DualityLessExpr.getDualityLessExpr((Expr)returnNode.get(ctx.expr(0)), (Expr)returnNode.get(ctx.expr(1))));
        }
        else if(ctx.oper.getText().equals(">")) {
            returnNode.put(ctx, DualityMoreExpr.getDualityMoreExpr((Expr)returnNode.get(ctx.expr(0)), (Expr)returnNode.get(ctx.expr(1))));
        }

    }

    @Override public void exitConstantExpr(ConstantExprContext ctx) {
        returnNode.put(ctx, returnNode.get(ctx.constant()));
    }

    @Override public void exitHighBinaryExpr(HighBinaryExprContext ctx) {
        //|	expr oper = ('*' | '/' | '%') expr                              #highBinaryExpr
        if(ctx.oper.getText().equals("*")) {
            returnNode.put(ctx, DualityMultiplyExpr.getDualityMultiplyExpr((Expr)returnNode.get(ctx.expr(0)), (Expr)returnNode.get(ctx.expr(1))));
        }
        else if(ctx.oper.getText().equals("/")) {
            returnNode.put(ctx, DualityDivideExpr.getDualityDivideExpr((Expr)returnNode.get(ctx.expr(0)), (Expr)returnNode.get(ctx.expr(1))));
        }
        else if(ctx.oper.getText().equals("%")) {
            returnNode.put(ctx, DualityModuleExpr.getDualityModuleExpr((Expr)returnNode.get(ctx.expr(0)), (Expr)returnNode.get(ctx.expr(1))));
        }
    }

    @Override public void exitShiftExpr(ShiftExprContext ctx) {
        //|	expr oper = ('<<' | '>>') expr                                  #shiftExpr
        if(ctx.oper.getText().equals("<<")) {
            returnNode.put(ctx, DualityMoveLeftExpr.getDualityMoveLeftExpr((Expr)returnNode.get(ctx.expr(0)), (Expr)returnNode.get(ctx.expr(1))));
        }
        else if(ctx.oper.getText().equals(">>")) {
            returnNode.put(ctx, DualityMoveRightExpr.getDualityMoveRightExpr((Expr)returnNode.get(ctx.expr(0)), (Expr)returnNode.get(ctx.expr(1))));
        }

    }

    @Override public void exitVariableExpr(VariableExprContext ctx) {
        returnNode.put(ctx, IdentifierExpr.getIdentifierExpr(ctx.getText()));
    }

    @Override public void exitArrayMemberExpr(ArrayMemberExprContext ctx) {
        for(ParseTree children : ctx.children) {
            if(children instanceof NewArrayExprContext) {
                throw new GrammarError("Error in exitArrayMemberExpr - contains New ArrayExpr");
            }
        }
        returnNode.put(ctx, ArrayMemberExpr.getArrayMemberExpr((Expr)returnNode.get(ctx.expr(0)), (Expr)returnNode.get(ctx.expr(1))));
    }

    @Override public void exitLogicalOrExpr(LogicalOrExprContext ctx) {
        returnNode.put(ctx, DualityLogicalOrExpr.getDualityLogicalOrExpr((Expr)returnNode.get(ctx.expr(0)), (Expr)returnNode.get(ctx.expr(1))));
    }

    @Override public void exitNewArrayExpr(NewArrayExprContext ctx) {
        List<Expr> exprs = new ArrayList<>();
        for (int i = 0; i < ctx.expr().size(); i++) {
            exprs.add((Expr)returnNode.get(ctx.expr(i)));
        }
        ParseTree lastChildren = null;
        for (ParseTree children : ctx.children) {
            if((lastChildren != null) && (children instanceof TerminalNode) && (lastChildren instanceof TerminalNode)) {
                Token token = ((TerminalNode)children).getSymbol();
                Token lastToken = ((TerminalNode)lastChildren).getSymbol();
                if (token.getText().equals("]") && lastToken.getText().equals("[")) {
                    exprs.add(null);
                }
            }
            lastChildren = children;
        }

        Type baseType = (Type)returnNode.get(ctx.type());
        //NewExpr.getNewExpr(baseType, (ArrayList<Expr>)exprs).toString();
        returnNode.put(ctx, NewExpr.getNewExpr(baseType, (ArrayList<Expr>)exprs));

    }

    @Override public void exitLowBinaryExpr(LowBinaryExprContext ctx) {
        //|	expr oper = ('+' | '-') expr                                    #lowBinaryExpr
        if(ctx.oper.getText().equals("+")) {
            returnNode.put(ctx, DualityAddExpr.getDualityAddExpr((Expr)returnNode.get(ctx.expr(0)), (Expr)returnNode.get(ctx.expr(1))));
        }
        else if(ctx.oper.getText().equals("-")) {
            returnNode.put(ctx, DualitySubtractExpr.getDualitySubtractExpr((Expr)returnNode.get(ctx.expr(0)), (Expr)returnNode.get(ctx.expr(1))));
        }
    }

    @Override public void exitOrExpr(OrExprContext ctx) {
        returnNode.put(ctx, DualityOrExpr.getDualityOrExpr((Expr)returnNode.get(ctx.expr(0)), (Expr)returnNode.get(ctx.expr(1))));
    }

    @Override public void exitXorExpr(XorExprContext ctx) {
        returnNode.put(ctx, DualityXorExpr.getDualityXorExpr((Expr)returnNode.get(ctx.expr(0)), (Expr)returnNode.get(ctx.expr(1))));
    }

    @Override public void exitSubExpr(SubExprContext ctx) {
        returnNode.put(ctx, returnNode.get(ctx.expr()));
    }

    @Override public void exitUnitaryExpr(UnitaryExprContext ctx) {
        Expr expr = (Expr)returnNode.get(ctx.expr());
        if (ctx.oper.getText().equals("+")) {
            returnNode.put(ctx, PlusExpr.getPlusExpr(expr));
        }
        else if (ctx.oper.getText().equals("-")) {
            returnNode.put(ctx, MinusExpr.getMinusExpr(expr));
        }
        if (ctx.oper.getText().equals("!")) {
            returnNode.put(ctx, LogicalNotExpr.getLogicalNotExpr(expr));
        }
        else if (ctx.oper.getText().equals("~")) {
            returnNode.put(ctx, NotExpr.getNotExpr(expr));
        }
        else if (ctx.oper.getText().equals("++")) {
            returnNode.put(ctx, PrefixPlusExpr.getPrefixPlusExpr(expr));
        }
        else if (ctx.oper.getText().equals("--")) {
            returnNode.put(ctx, PrefixMinusExpr.getPrefixMinusExpr(expr));
        }
    }

    @Override public void exitClassMemberExpr(ClassMemberExprContext ctx) {
        returnNode.put(ctx, ClassMemberExpr.getClassMemberExpr((Expr)returnNode.get(ctx.expr()), ctx.IDENTIFIER().getText()));
    }

    @Override public void exitEqualityExpr(EqualityExprContext ctx) {
        //|	expr oper = ('==' | '!=') expr                                  #equalityExpr         #lowBinaryExpr
        if(ctx.oper.getText().equals("==")) {
            returnNode.put(ctx, DualityEqualExpr.getDualityEqualExpr((Expr)returnNode.get(ctx.expr(0)), (Expr)returnNode.get(ctx.expr(1))));
        }
        else if(ctx.oper.getText().equals("!=")) {
            returnNode.put(ctx, DualityUnEqualExpr.getDualityUnEqualExpr((Expr)returnNode.get(ctx.expr(0)), (Expr)returnNode.get(ctx.expr(1))));
        }
    }

    @Override public void exitBoolConstant(BoolConstantContext ctx) {
        returnNode.put(ctx, new BoolConstant(Boolean.valueOf(ctx.getText())));
    }

    @Override public void exitIntegerConstant(IntegerConstantContext ctx) {
        returnNode.put(ctx, new IntegerConstant(Integer.valueOf(ctx.getText())));
    }

    @Override public void exitStringConstant(StringConstantContext ctx) {
      returnNode.put(ctx, new StringConstant(ctx.getText()).getConstant(ctx.getText().substring(1, ctx.getText().length() - 1)));
    }

    @Override public void exitNullConstant(NullConstantContext ctx) {
        returnNode.put(ctx, new NullConstant().getConstant());
    }

}