package Compiler.Front.Cst.Listener;

import Compiler.Enviroment.Environment;
import Compiler.Enviroment.SymbolTable.Symbol;
import Compiler.Front.Ast.Function;
import Compiler.Front.Ast.Statement.Statement;
import Compiler.Front.Ast.Type.ArrayType;
import Compiler.Front.Ast.Type.BasicType.BoolType;
import Compiler.Front.Ast.Type.BasicType.IntegerType;
import Compiler.Front.Ast.Type.BasicType.StringType;
import Compiler.Front.Ast.Type.BasicType.VoidType;
import Compiler.Front.Ast.Type.Class.ClassMember.Member;
import Compiler.Front.Ast.Type.Class.ClassMember.MemberFunction;
import Compiler.Front.Ast.Type.Type;
import Compiler.Front.Cst.Listener.BaseListener;
import Compiler.Front.Cst.Parser.MxParser;
import Compiler.Front.Ast.Type.Class.Class;
import Compiler.Utility.Error.GrammarError;

import java.util.ArrayList;
import java.util.List;

public class FunctionListener extends BaseListener {
    @Override
    public void enterClassDefinition(MxParser.ClassDefinitionContext ctx) {
        if(!Environment.scopeTable.empty()) {
            throw new GrammarError("Error in enterClassDefinition - !Environment.scopeTable.empty():" + Environment.scopeTable.getClassScope().name);
        }
        Environment.enterScope((Class)returnNode.get(ctx));
        //System.out.println("");
        //System.out.print(ctx);
        //System.out.print(" # ");
        //System.out.println(returnNode.get(ctx));
    }
    @Override
    public void exitClassDefinition(MxParser.ClassDefinitionContext ctx) {
        Class currentClass = (Class)returnNode.get(ctx);
        for (MxParser.VariableDefinitionContext variableDefinitionContext : ctx.variableDefinition()) {
            String name = variableDefinitionContext.IDENTIFIER().get(0).getText();
            Type type = (Type)returnNode.get(variableDefinitionContext.type());
            currentClass.addMember(type, name);
        }
        Environment.exitScope();
    }
    @Override
    public void exitFunctionDefinition(MxParser.FunctionDefinitionContext ctx) {

        Type type = VoidType.getType();
        Class currentClass = Environment.scopeTable.getClassScope();
        if(ctx.IDENTIFIER().size() == ctx.type().size()) {
            String name = ctx.IDENTIFIER(0).getText();
            if(name.equals("this")) {
                throw new GrammarError("Error in exitFunctionDefinition -FunctionName Can not be this");
            }
            type = (Type)returnNode.get(ctx.type(0));
            List<Symbol> parameters = new ArrayList<>();
            if(currentClass != null) {
                parameters.add(new Symbol(currentClass, "this"));
            }
            for (int i = 1; i < ctx.type().size(); i++) {
                String parameterName = ctx.IDENTIFIER(i).getText();
                Type parameterType = (Type)returnNode.get(ctx.type(i));
                parameters.add(new Symbol(parameterType, parameterName));
            }

            Function function = Function.getFunction(type, name, parameters);
            if(currentClass != null) {
                currentClass.addMember(name, new MemberFunction(name, function));
            }
            else {
                //Environment.program.addFunction(name, function);
                Environment.symbolTable.add(function, name);
            }
            Environment.program.addFunction(name, function);
            returnNode.put(ctx, function);
        } else{//Constructor
            String name = ctx.type(0).getText();
            if(currentClass == null) {
                throw new GrammarError("Error in exitFunctionDefinition - Constructor: currentClass == null" + name);
            }
            if(!currentClass.name.equals(name)) {
                throw new GrammarError("Error in exitFunctionDefinition - Constructor: currentClass.name != name" + name + " " + currentClass.name);
            }
            List<Symbol> parameters = new ArrayList<>();
            if(currentClass != null) {
                parameters.add(new Symbol(currentClass, "this"));
            }
            for (int i = 1; i < ctx.type().size(); i++) {
                String parameterName = ctx.IDENTIFIER(i - 1).getText();
                Type parameterType = (Type)returnNode.get(ctx.type(i));
                parameters.add(new Symbol(parameterType, parameterName));
            }

            Function function = Function.getFunction(type, name, parameters);
            currentClass.addConstructor(function);
            Environment.program.addFunction(name, function);
            returnNode.put(ctx, function);
        }
        return ;
    }
    @Override public void exitStringType(MxParser.StringTypeContext ctx) {
        returnNode.put(ctx, StringType.getType());
    }

    @Override public void exitBoolType(MxParser.BoolTypeContext ctx) {
        returnNode.put(ctx, BoolType.getType());
    }

    @Override public void exitArrayType(MxParser.ArrayTypeContext ctx) {
        Type baseType = (Type)returnNode.get(ctx.type());
        int dimension;
        if(baseType instanceof ArrayType) {
            dimension = ((ArrayType) baseType).dimension + 1;
        }
        else if (baseType instanceof VoidType) {
            throw new GrammarError("Error in -exitArrayType - baseType instanceof VoidType");
        }
        else dimension = 1;
        //System.out.print("\n## ArrayType dimension = ");
        //System.out.println(dimension);
        returnNode.put(ctx, ArrayType.getType(baseType, dimension));
    }

    @Override public void exitVoidType(MxParser.VoidTypeContext ctx) {
        returnNode.put(ctx, VoidType.getType());
    }

    @Override public void exitUserDefinedType(MxParser.UserDefinedTypeContext ctx) {
        Class currentClass = Environment.classTable.getClass(ctx.getText());
        if(currentClass != null) {
            returnNode.put(ctx, currentClass);
        }
        else {
            throw new GrammarError("Error in -exitUserDefinedType - Environment.classTable.getClass(ctx.getText()) == null :" + ctx.getText());
        }
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitIntType(MxParser.IntTypeContext ctx) {
        returnNode.put(ctx, IntegerType.getType());
    }
}