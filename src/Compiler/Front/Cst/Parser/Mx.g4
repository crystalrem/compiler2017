grammar Mx;

program	: (functionDefinition | classDefinition | variableDefinition)+;

classDefinition	:	'class' IDENTIFIER '{' (functionDefinition | variableDefinition)* '}';

functionDefinition	:	type IDENTIFIER? '(' (type IDENTIFIER (',' type IDENTIFIER)* )? ')' blockStatement;

variableDefinition	:	type IDENTIFIER ( '=' expr)? (',' IDENTIFIER ('=' expr)?)* ';';

statement	:	variableDefinition
			|	exprStatement
			|	selectionStatement
			|	loopStatement
			|	jumpStatement
			|	blockStatement
			;
						
exprStatement	:	expr? ';';
			
selectionStatement	:	'if' '(' expr ')' statement ('else' statement)?;

loopStatement	:	'while' '(' expr ')' statement                          #whileStatement
				|	'for' '(' expr? ';' expr? ';' expr? ')' statement       #forStatement
				;
	
jumpStatement	:	'continue' ';'                                          #continueStatement
				|	'break' ';'                                             #breakStatement
				|	'return' (expr)? ';'                                    #returnStatement
				;

blockStatement	:	'{' statement* '}';                                     

expr	:	constant	                                                    #constantExpr
		|	IDENTIFIER                                                      #variableExpr
		|	'(' expr ')'                                                    #subExpr
		|	expr operator=('++'|'--')						                #postfixExpr
		|	expr '(' ( expr ( ',' expr )* )? ')'                            #functionCallExpr
		|	expr '.' IDENTIFIER                                             #classMemberExpr    
		|	expr '[' expr ']'                                               #arrayMemberExpr
		|	oper = ('+'|'-'|'!'|'~'|'++'|'--') expr                         #unitaryExpr
		|	'new' type ('[' expr ']')*('[' ']')*                            #newArrayExpr
		|	expr oper = ('*' | '/' | '%') expr                              #highBinaryExpr
		|	expr oper = ('+' | '-') expr                                    #lowBinaryExpr
		|	expr oper = ('<<' | '>>') expr                                  #shiftExpr
		|	expr oper = ('<=' | '>=' | '<' | '>') expr                      #partialOrderExpr
		|	expr oper = ('==' | '!=') expr                                  #equalityExpr
		|	expr op = '&' expr                                              #andExpr
		|	expr op = '^' expr                                              #xorExpr
		| 	expr op = '|' expr                                              #orExpr
		| 	expr op = '&&' expr                                             #logicalAndExpr
		|	expr op = '||' expr                                             #logicalOrExpr
		|	expr op = '=' expr                                              #assignmentExpr
		;

type	:	'bool'                                                          #boolType
		|	'int'                                                           #intType
		|	'void'                                                          #voidType
		|	'string'                                                        #stringType        
		|	IDENTIFIER                                                      #userDefinedType    
		|	type'[' ']'                                                     #arrayType
		;
				
constant	:	('true'|'false')                                            #boolConstant
			|	INTEGERCONSTANT                                             #integerConstant
			|	STRINGCONSTANT                                              #stringConstant
			|	'null'                                                      #nullConstant        
			;
			
IDENTIFIER	:	[a-zA-Z][a-zA-Z0-9_]*;
			
	
INTEGERCONSTANT	:	[0-9]+;

STRINGCONSTANT	:	'\"' CHARACTER* '\"';

fragment

CHARACTER	:	~["\\\r\n]
			|	'\\' ['"?abfnrtv\\]
			;

LINECOMMENT :   '//' ~[\r\n]*   ->  skip;

WHITESPACE	:	[ \n\r\t]+  ->  skip;
