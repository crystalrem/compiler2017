package Compiler;

import Compiler.Back.Allocator.GlobalRegisterAllocator.GlobalRegisterAllocator;
import Compiler.Back.MidCode.Intruction.Instruction;
import Compiler.Back.MidCode.MidCode;
import Compiler.Back.Translator.NasmTranslator.NaiveTranslator;
import Compiler.Back.Translator.NasmTranslator.NasmTranslator;
import Compiler.Back.Translator.NasmTranslator.UpdateTranslator;
import Compiler.Enviroment.Environment;
import Compiler.Front.Ast.Function;
import Compiler.Front.Ast.Type.BasicType.IntegerType;
import Compiler.Front.Cst.Listener.ClassListener;
import Compiler.Front.Cst.Listener.FunctionListener;
import Compiler.Front.Cst.Listener.SyntaxErrorListener;
import Compiler.Front.Cst.Listener.TreeBuildListener;
import Compiler.Front.Cst.Parser.MxLexer;
import Compiler.Front.Cst.Parser.MxParser;

import java.io.*;
import java.util.ArrayList;

import Compiler.Utility.Error.GrammarError;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

public class Main {
    public static void main(String[] args) throws Exception{
        try {
        InputStream is = new FileInputStream("program.txt");
        //    InputStream is = System.in;
            new Main().compile(is, System.out);
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        } catch(GrammarError e) {
            System.exit(1);
        }catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }

    }
    void load(InputStream file) throws Exception {
        ANTLRInputStream input = new ANTLRInputStream(file);
        MxLexer lexer = new MxLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        MxParser parser = new MxParser(tokens);
        parser.removeErrorListeners();
        parser.addErrorListener(new SyntaxErrorListener());
        ParseTree tree = parser.program();
        ParseTreeWalker walker = new ParseTreeWalker();
        Environment.initialize();
        walker.walk(new ClassListener(), tree);
        //System.out.print(Environment.program.toString(0));
        walker.walk(new FunctionListener(), tree);
        //System.out.print(Environment.program.toString(0));
        walker.walk(new TreeBuildListener(), tree);
        //System.out.print(Environment.program.toString(0));
        if(!Environment.program.functionMap.containsKey("main")) {
            throw new GrammarError("Error in no Main function");
        }
        if(!(Environment.program.functionMap.get("main").type instanceof IntegerType)) {
            throw new GrammarError("Error in Main function is not IntegerType");
        }
    }
    void compile(InputStream file, OutputStream output) throws Exception {
        load(file);
        for (Function function : Environment.program.functions) {

            function.midCode = new MidCode(function);

            //new
            function.allocator = new GlobalRegisterAllocator(function);
            //
            //System.out.print("\n\n\n\n" + "************************************MAIN" + function.name + "\n\n\n\n");
            /*for (int i = 0; i < function.midCode.instructions.size(); i++) {
                allInstructions.add(function.midCode.instructions.get(i));
            }*/
        }
        //new NaiveTranslator(new PrintStream(output)).translate();
        //new
        new UpdateTranslator(new PrintStream(output)).translate();
    }
}
